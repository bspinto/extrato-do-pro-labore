# README #

* Extrato do Pró-Labore – Ajuste e Disponibilização no Vida Web

### What is this repository for? ###

* Apresentar o Valor de IR e demais Encargos, e dados no Extrato de Pró-Labore disponibilizado na aplicação do SEGBR.

* Desenvolver funcionalidade de consulta do Extrato de Pró-Labore no sistema Vida Web, permitindo consulta online pelo usuário 


### How do I get set up? ###

* Vida Web – Sistema de Faturamento.
* SEGBR – Consulta de Pró-Labore (SEGP0270)
* SAP

### Contribution guidelines ###

* Especificação funcinal
* Entendimento Técnico
* Desenvolvimento
* Teste
* Implantação

### Who do I talk to? ###

* SEGBR
* Atualmente a consulta do Extrato do Pró-Labore é realizada através do sistema legado SEGBR, ao qual é disponibilizado pela aplicação SEGP0270, sendo possível realizar a consulta e impressão dos extratos de Pró-Labore. 
* No Extrato o Valor de IR é sempre apresentado com valor zerado, ao qual este dado é extraído em consulta diretamente no SAP para envio da informação ao segurado/estipulante.

* Vida Web
* Atualmente o Vida Web não possui a disponibilização de Extrato de Pró-Labore. 
