/*
	Bruno Schoralick Pinto (ntendencia) - 09/03/2020
	Demanda: extrato-do-pro-labore
	Nome do script: script_add_campos_rpt_ext_pgto_corretor_tb
	Descri��o: Script para incluir campos na tabela rpt_ext_pgto_corretor_tb referente a demanda extrato-do-pro-labore
	Objetivo: alter
	Banco: seguros_db
	Ambiente: AB e ABS
*/

ALTER TABLE dbo.rpt_ext_pgto_corretor_tb ADD val_bruno NUMERIC(14,2)
ALTER TABLE dbo.rpt_ext_pgto_corretor_tb ADD val_impostos_retido NUMERIC(14,2)
ALTER TABLE dbo.rpt_ext_pgto_corretor_tb ADD val_liquido NUMERIC(14,2)
ALTER TABLE dbo.rpt_ext_pgto_corretor_tb ADD dt_lancamento SMALLDATETIME
ALTER TABLE dbo.rpt_ext_pgto_corretor_tb ADD dt_pgto SMALLDATETIME
ALTER TABLE dbo.rpt_ext_pgto_corretor_tb ADD CNPJ_pgto VARCHAR(14)
ALTER TABLE dbo.rpt_ext_pgto_corretor_tb ADD razao_pgto VARCHAR(60)
ALTER TABLE dbo.rpt_ext_pgto_corretor_tb ADD CNPJ_beneficiario VARCHAR(14)
ALTER TABLE dbo.rpt_ext_pgto_corretor_tb ADD razao_beneficiario VARCHAR(60)

GO	     

