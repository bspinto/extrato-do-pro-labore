VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPagAssociados 
   Caption         =   "SEG07005 - Pagamentos Associados - "
   ClientHeight    =   6750
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9210
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6750
   ScaleWidth      =   9210
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   15
      Top             =   6495
      Width           =   9210
      _ExtentX        =   16245
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdAplicar 
      Caption         =   "&Aplicar"
      Height          =   435
      Left            =   5760
      TabIndex        =   14
      Top             =   5760
      Width           =   1335
   End
   Begin VB.TextBox txtValDeb 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4080
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   6000
      Width           =   1335
   End
   Begin VB.TextBox txtQtdDeb 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3000
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   6000
      Width           =   855
   End
   Begin VB.TextBox txtValCred 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4080
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   5520
      Width           =   1335
   End
   Begin VB.TextBox txtQtdCred 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3000
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   5520
      Width           =   855
   End
   Begin MSFlexGridLib.MSFlexGrid GridPagAssociadosDeb 
      Height          =   1860
      Left            =   120
      TabIndex        =   7
      Top             =   3480
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   3281
      _Version        =   393216
      Cols            =   0
      FixedCols       =   0
      HighLight       =   0
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   435
      Left            =   7560
      TabIndex        =   0
      Top             =   5760
      Width           =   1335
   End
   Begin MSFlexGridLib.MSFlexGrid GridPagAssociadosCred 
      Height          =   1860
      Left            =   120
      TabIndex        =   2
      Top             =   1320
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   3281
      _Version        =   393216
      Cols            =   0
      FixedCols       =   0
      HighLight       =   0
   End
   Begin VB.Frame fraFavorecido 
      Caption         =   " Favorecido"
      Height          =   1170
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   9015
      Begin VB.Frame Frame1 
         Caption         =   " Nome"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   810
         Left            =   2355
         TabIndex        =   5
         Top             =   240
         Width           =   6090
         Begin VB.TextBox txtNomeFavorecido 
            Height          =   315
            Left            =   165
            Locked          =   -1  'True
            TabIndex        =   6
            Top             =   315
            Width           =   5685
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   " Tipo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   810
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   2190
         Begin VB.TextBox txtTipoFavorecido 
            Height          =   315
            Left            =   150
            Locked          =   -1  'True
            TabIndex        =   4
            Top             =   315
            Width           =   1875
         End
      End
   End
   Begin VB.Label Label2 
      Caption         =   "Lan�amentos a D�bito .........."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   6000
      Width           =   2535
   End
   Begin VB.Label Label1 
      Caption         =   "Lan�amentos a Cr�dito ........."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   5520
      Width           =   2535
   End
End
Attribute VB_Name = "frmPagAssociados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Dt_Sistema As Date
Dim AprPag As Integer
Dim QtdCred   As Long
Dim QtdDeb    As Long
Dim ValCred   As Currency
Dim ValDeb    As Currency
Dim vColuna   As Long

Dim tp_movimentacao As String



Private Function Monta_Query_Associados() As String

Dim SQL As String, NumPagamento As Long

NumPagamento = Val(frmAprPagamentos.gridPagamentos.TextMatrix(frmAprPagamentos.gridPagamentos.Row, 2))

Select Case frmAprPagamentos.gridPagamentos.TextMatrix(frmAprPagamentos.gridPagamentos.Row, 3)
Case "Cliente"
    SQL = SQL & " SELECT a.dt_movimentacao, b.movimentacao_id, b.tp_operacao, b.retido_banco, b.val_movimentacao, c.nome, '1' tp_movimentacao"
    SQL = SQL & " FROM ps_movimentacao_tb a  WITH (NOLOCK)  , ps_mov_cliente_tb b  WITH (NOLOCK)  , cliente_tb c  WITH (NOLOCK)  "
    SQL = SQL & " WHERE a.movimentacao_id = b.movimentacao_id AND b.cliente_id = c.cliente_id"
    SQL = SQL & " AND b.acerto_id =" & NumPagamento
    
Case "Prestadora de Assist�ncia"
    
    SQL = SQL & " SELECT a.dt_movimentacao, b.movimentacao_id, b.tp_operacao, b.retido_banco, b.val_movimentacao, nome= '', '13' tp_movimentacao"
    SQL = SQL & " FROM ps_movimentacao_tb a  WITH (NOLOCK)  , ps_mov_assistencia_tb b  WITH (NOLOCK) "
    SQL = SQL & " Where a.movimentacao_id = b.movimentacao_id"
    SQL = SQL & " AND b.acerto_id =" & NumPagamento
    
    
Case "Corretor"
'   RMAURI 17/10/2006

    If UCase(TpAprovacao) = "COMISS�O RETIDA" Or UCase(TpAprovacao) = "PR�-LABORE RETIDO" Then
        SQL = SQL & " select  a.dt_movimentacao,"
        SQL = SQL & "     b.movimentacao_id,"
        SQL = SQL & "     b.tp_operacao,"
        SQL = SQL & "     b.retido_banco,"
        SQL = SQL & "     b.val_movimentacao,"
        SQL = SQL & "     c.nome,"
        SQL = SQL & "     '4' as tp_movimentacao,"
        SQL = SQL & "     e.produto_id,"
        SQL = SQL & "     e.nome as produto_nome,"
        SQL = SQL & "     f.ramo_id,"
        SQL = SQL & "     f.nome as ramo_nome"
        SQL = SQL & " from    ps_movimentacao_tb a  WITH (NOLOCK)  "
        SQL = SQL & " join    ps_mov_corretor_pf_tb b  WITH (NOLOCK)  "
        SQL = SQL & "     on  a.movimentacao_id = b.movimentacao_id"
        SQL = SQL & " join    corretor_tb c  WITH (NOLOCK)  "
        SQL = SQL & "     on  b.pf_corretor_id = c.corretor_id"
        SQL = SQL & " join    ps_mov_arquivo_baixa_tb d  WITH (NOLOCK)  "
        SQL = SQL & "     on  b.movimentacao_id = d.movimentacao_id"
        SQL = SQL & " join    produto_tb e  WITH (NOLOCK)  "
        SQL = SQL & "     on  d.produto_id = e.produto_id"
        SQL = SQL & " join    ramo_tb f  WITH (NOLOCK)  "
        SQL = SQL & "     on  d.ramo_id = f.ramo_id"
        SQL = SQL & " "
        SQL = SQL & " where b.acerto_id = " & NumPagamento
        SQL = SQL & " "
        SQL = SQL & " Union"
        SQL = SQL & " "
        SQL = SQL & " select  a.dt_movimentacao,"
        SQL = SQL & "     b.movimentacao_id,"
        SQL = SQL & "     b.tp_operacao,"
        SQL = SQL & "     b.retido_banco,"
        SQL = SQL & "     b.val_movimentacao,"
        SQL = SQL & "     c.nome,"
        SQL = SQL & "     '5' as tp_movimentacao,"
        SQL = SQL & "     e.produto_id,"
        SQL = SQL & "     e.nome as produto_nome,"
        SQL = SQL & "     f.ramo_id,"
        SQL = SQL & "     f.nome as ramo_nome"
        SQL = SQL & " from    ps_movimentacao_tb a  WITH (NOLOCK)  "
        SQL = SQL & " join    ps_mov_corretor_pj_tb b  WITH (NOLOCK)  "
        SQL = SQL & "     on  a.movimentacao_id = b.movimentacao_id"
        SQL = SQL & " join    sucursal_corretor_tb c  WITH (NOLOCK)  "
        SQL = SQL & "     on  b.pj_corretor_id = c.pj_corretor_id"
        SQL = SQL & "     and     b.sucursal_corretor_id = c.sucursal_corretor_id"
        SQL = SQL & " join    ps_mov_arquivo_baixa_tb d  WITH (NOLOCK)  "
        SQL = SQL & "     on  b.movimentacao_id = d.movimentacao_id"
        SQL = SQL & " join    produto_tb e  WITH (NOLOCK)  "
        SQL = SQL & "     on  d.produto_id = e.produto_id"
        SQL = SQL & " join    ramo_tb f  WITH (NOLOCK)  "
        SQL = SQL & "     on  d.ramo_id = f.ramo_id"
        SQL = SQL & " "
        SQL = SQL & " where b.acerto_id = " & NumPagamento
    Else
        SQL = SQL & " select  a.dt_movimentacao,"
        SQL = SQL & "     b.movimentacao_id,"
        SQL = SQL & "     b.tp_operacao,"
        SQL = SQL & "     b.retido_banco,"
        SQL = SQL & "     b.val_movimentacao,"
        SQL = SQL & "     c.nome,"
        SQL = SQL & "     '4' tp_movimentacao"
        SQL = SQL & " from    ps_movimentacao_tb a  WITH (NOLOCK)  "
        SQL = SQL & " join    ps_mov_corretor_pf_tb b  WITH (NOLOCK)  "
        SQL = SQL & "     on  a.movimentacao_id = b.movimentacao_id"
        SQL = SQL & " join    corretor_tb c  WITH (NOLOCK)  "
        SQL = SQL & "     on  b.pf_corretor_id = c.corretor_id"
        SQL = SQL & " where b.acerto_id = " & NumPagamento
        SQL = SQL & " "
        SQL = SQL & " Union"
        SQL = SQL & " "
        SQL = SQL & " select  a.dt_movimentacao,"
        SQL = SQL & "     b.movimentacao_id,"
        SQL = SQL & "     b.tp_operacao,"
        SQL = SQL & "     b.retido_banco,"
        SQL = SQL & "     b.val_movimentacao,"
        SQL = SQL & "     c.nome,"
        SQL = SQL & "     '5'"
        SQL = SQL & "     tp_movimentacao"
        SQL = SQL & " from    ps_movimentacao_tb a  WITH (NOLOCK)  "
        SQL = SQL & " join    ps_mov_corretor_pj_tb b  WITH (NOLOCK)  "
        SQL = SQL & "     on  a.movimentacao_id = b.movimentacao_id"
        SQL = SQL & " join    sucursal_corretor_tb c  WITH (NOLOCK)  "
        SQL = SQL & "     on  b.pj_corretor_id = c.pj_corretor_id"
        SQL = SQL & "     and     b.sucursal_corretor_id = c.sucursal_corretor_id"
        SQL = SQL & " where b.acerto_id = " & NumPagamento
    End If
    
'    SQL ANTIGO
'    SQL = SQL & " SELECT a.dt_movimentacao, b.movimentacao_id, b.tp_operacao, b.retido_banco, b.val_movimentacao, c.nome, '4' tp_movimentacao"
'    SQL = SQL & " FROM ps_movimentacao_tb a, ps_mov_corretor_pf_tb b, corretor_tb c"
'    SQL = SQL & " WHERE a.movimentacao_id = b.movimentacao_id AND b.pf_corretor_id = c.corretor_id"
'    SQL = SQL & " AND b.acerto_id=" & NumPagamento
'    SQL = SQL & " UNION"
'    SQL = SQL & " SELECT a.dt_movimentacao, b.movimentacao_id, b.tp_operacao, b.retido_banco, b.val_movimentacao, c.nome, '5' tp_movimentacao"
'    SQL = SQL & " FROM ps_movimentacao_tb a, ps_mov_corretor_pj_tb b, sucursal_corretor_tb c"
'    SQL = SQL & " WHERE a.movimentacao_id = b.movimentacao_id "
'    SQL = SQL & " AND b.pj_corretor_id = c.pj_corretor_id "
'    SQL = SQL & " AND b.sucursal_corretor_id = c.sucursal_corretor_id "
'    SQL = SQL & " AND b.acerto_id =" & NumPagamento
'   RMAURI 17/10/2006
    
    
Case "Cosseguro"
    SQL = SQL & " SELECT a.dt_movimentacao, b.movimentacao_id, b.tp_operacao, b.retido_banco, b.val_movimentacao, c.nome, '6' tp_movimentacao"
    SQL = SQL & " FROM ps_movimentacao_tb a  WITH (NOLOCK)  , ps_mov_cosseguro_tb b  WITH (NOLOCK)  , seguradora_tb c  WITH (NOLOCK)  "
    SQL = SQL & " WHERE a.movimentacao_id = b.movimentacao_id AND b.seguradora_cod_susep = c.seguradora_cod_susep"
    SQL = SQL & " AND b.acerto_id =" & NumPagamento

Case "Pro Labore"
    If UCase(TpAprovacao) = "COMISS�O RETIDA" Or UCase(TpAprovacao) = "PR�-LABORE RETIDO" Then
        SQL = SQL & " select  a.dt_movimentacao,"
        SQL = SQL & "     b.movimentacao_id,"
        SQL = SQL & "     b.tp_operacao,"
        SQL = SQL & "     b.retido_banco,"
        SQL = SQL & "     b.val_movimentacao,"
        SQL = SQL & "     c.nome,"
        SQL = SQL & "     '2' tp_movimentacao,"
        SQL = SQL & "     e.produto_id,"
        SQL = SQL & "     e.nome as produto_nome,"
        SQL = SQL & "     f.ramo_id,"
        SQL = SQL & "     f.nome as ramo_nome"
        SQL = SQL & " from    ps_movimentacao_tb a  WITH (NOLOCK)  "
        SQL = SQL & " join    ps_mov_estipulante_tb b  WITH (NOLOCK)  "
        SQL = SQL & "     on  a.movimentacao_id = b.movimentacao_id"
        SQL = SQL & " join    cliente_tb c  WITH (NOLOCK)  "
        SQL = SQL & "     on  b.cliente_id = c.cliente_id"
        SQL = SQL & "     and b.acerto_id = " & NumPagamento
        SQL = SQL & " join    ps_mov_arquivo_baixa_tb d  WITH (NOLOCK)  "
        SQL = SQL & "     on  b.movimentacao_id = d.movimentacao_id"
        SQL = SQL & " join    produto_tb e  WITH (NOLOCK)  "
        SQL = SQL & "     on  d.produto_id = e.produto_id"
        SQL = SQL & " join    ramo_tb f  WITH (NOLOCK)  "
        SQL = SQL & "     on  d.ramo_id = f.ramo_id"
    Else
        SQL = SQL & " select  a.dt_movimentacao,"
        SQL = SQL & "     b.movimentacao_id,"
        SQL = SQL & "     b.tp_operacao,"
        SQL = SQL & "     b.retido_banco,"
        SQL = SQL & "     b.val_movimentacao,"
        SQL = SQL & "     c.nome,"
        SQL = SQL & "     '2' tp_movimentacao"
        SQL = SQL & " from    ps_movimentacao_tb a  WITH (NOLOCK)  "
        SQL = SQL & " join    ps_mov_estipulante_tb b  WITH (NOLOCK)  "
        SQL = SQL & "     on  a.movimentacao_id = b.movimentacao_id"
        SQL = SQL & " join    cliente_tb c  WITH (NOLOCK)  "
        SQL = SQL & "     on  b.cliente_id = c.cliente_id"
        SQL = SQL & "     and b.acerto_id = " & NumPagamento
    End If
'       SQL ANTIGO
'       SQL = SQL & " SELECT a.dt_movimentacao, b.movimentacao_id, b.tp_operacao, b.retido_banco, b.val_movimentacao, c.nome, '2' tp_movimentacao"
'       SQL = SQL & " FROM ps_movimentacao_tb a, ps_mov_estipulante_tb b, cliente_tb c"
'       SQL = SQL & " WHERE a.movimentacao_id = b.movimentacao_id AND b.cliente_id = c.cliente_id"
'       SQL = SQL & " AND b.acerto_id =" & NumPagamento
'       RMAURI 17/10/2006

Case "Item Financeiro"
    SQL = SQL & " SELECT a.dt_movimentacao, b.movimentacao_id, b.tp_operacao, b.retido_banco, (b.val_movimentacao + b.val_multa + b.val_juros) val_movimentacao, c.nome,"
    SQL = SQL & " tp_movimentacao = case  b.cod_item_financeiro when 'IOF' then   '8' else '9' end "
    SQL = SQL & " FROM ps_movimentacao_tb a  WITH (NOLOCK)  , ps_mov_item_financeiro_tb b  WITH (NOLOCK)  , item_financeiro_tb c  WITH (NOLOCK)  "
    SQL = SQL & " WHERE a.movimentacao_id = b.movimentacao_id AND b.cod_item_financeiro = c.cod_item_financeiro"
    SQL = SQL & " AND b.acerto_id =" & NumPagamento

Case "Agenciador"
    SQL = SQL & " SELECT a.dt_movimentacao, b.movimentacao_id, b.tp_operacao, b.retido_banco, b.val_movimentacao, c.nome, '7' tp_movimentacao "
    SQL = SQL & " FROM ps_movimentacao_tb a  WITH (NOLOCK)  , ps_mov_agenciador_tb b  WITH (NOLOCK)  , agenciador_tb c  WITH (NOLOCK)  "
    SQL = SQL & " WHERE a.movimentacao_id = b.movimentacao_id AND b.agenciador_id = c.agenciador_id"
    SQL = SQL & " AND b.acerto_id =" & NumPagamento
    
Case "Prestador"
    SQL = SQL & " SELECT a.dt_movimentacao, b.movimentacao_id, b.tp_operacao, b.retido_banco, b.val_movimentacao, c.nome, '10' tp_movimentacao "
    SQL = SQL & " FROM ps_movimentacao_tb a  WITH (NOLOCK)  , ps_mov_prestador_tb b  WITH (NOLOCK)  , prestador_tb c  WITH (NOLOCK)  "
    SQL = SQL & " WHERE a.movimentacao_id = b.movimentacao_id AND b.prestador_id = c.prestador_id"
    SQL = SQL & " AND b.acerto_id =" & NumPagamento
    
Case "Regulador"
    SQL = SQL & " SELECT a.dt_movimentacao, b.movimentacao_id, b.tp_operacao, b.retido_banco, b.val_movimentacao, c.nome, '11' tp_movimentacao "
    SQL = SQL & " FROM ps_movimentacao_tb a  WITH (NOLOCK)  , ps_mov_regulador_tb b  WITH (NOLOCK)  , regulador_tb c  WITH (NOLOCK)  "
    SQL = SQL & " WHERE a.movimentacao_id = b.movimentacao_id AND b.regulador_id = c.regulador_id"
    SQL = SQL & " AND b.acerto_id =" & NumPagamento
    
'mathayde 24/07/2009 demanda : 654224
Case "Banco"
    SQL = ""
    SQL = SQL & " SELECT a.dt_movimentacao," & vbNewLine
    SQL = SQL & "        b.movimentacao_id," & vbNewLine
    SQL = SQL & "        b.tp_operacao," & vbNewLine
    SQL = SQL & "        b.retido_banco," & vbNewLine
    SQL = SQL & "        b.val_movimentacao," & vbNewLine
    SQL = SQL & "        nome = 'Banco do Brasil'," & vbNewLine
    SQL = SQL & "        tp_movimentacao = '12'," & vbNewLine
    SQL = SQL & "        g.produto_id," & vbNewLine
    SQL = SQL & "        produto_nome = g.nome," & vbNewLine
    SQL = SQL & "        f.ramo_id," & vbNewLine
    SQL = SQL & "        ramo_nome = F.nome" & vbNewLine
    SQL = SQL & "   FROM ps_movimentacao_tb a  WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & "   JOIN ps_mov_repasse_bb_tb b  WITH (NOLOCK)  " & vbNewLine
    SQL = SQL & "     ON a.movimentacao_id = b.movimentacao_id" & vbNewLine
    SQL = SQL & "   JOIN ps_mov_repasse_cartao_credito_tb c" & vbNewLine
    SQL = SQL & "     ON c.movimentacao_id = b.movimentacao_id" & vbNewLine
    SQL = SQL & "   JOIN repasse_cartao_credito_tb d" & vbNewLine
    SQL = SQL & "     ON d.repasse_cartao_credito_id = c.repasse_cartao_credito_id" & vbNewLine
    SQL = SQL & "   JOIN seguros_db..proposta_tb e" & vbNewLine
    SQL = SQL & "     ON e.proposta_id = d.proposta_id" & vbNewLine
    SQL = SQL & "   JOIN seguros_db..ramo_tb f" & vbNewLine
    SQL = SQL & "     ON f.ramo_id = e.ramo_id" & vbNewLine
    SQL = SQL & "   JOIN seguros_db..produto_tb g" & vbNewLine
    SQL = SQL & "     ON g.produto_id = e.produto_id" & vbNewLine
    SQL = SQL & "  WHERE b.acerto_id =" & NumPagamento & vbNewLine



End Select

Monta_Query_Associados = SQL & " ORDER BY dt_movimentacao"
    
End Function

Private Sub cmdAplicar_Click()
Dim rc As rdoResultset
Dim I  As Long
Dim SQL As String
Dim Val_Pag As Currency

On Error GoTo erro

If MsgBox("Confirma as altera��es realizadas?", vbYesNo) = vbNo Then
   Exit Sub
End If

Val_Pag = ValCred - ValDeb

If ConfiguracaoBrasil Then
   frmAprPagamentos.gridPagamentos.TextMatrix(frmAprPagamentos.gridPagamentos.Row, 5) = Format(Val_Pag, "#,###,###,##0.00")
Else
   frmAprPagamentos.gridPagamentos.TextMatrix(frmAprPagamentos.gridPagamentos.Row, 5) = TrocaValorAmePorBras(Format(Val_Pag, "#,###,###,##0.00"))
End If

rdocn.BeginTrans

For I = 1 To GridPagAssociadosCred.Rows - 1
    If Trim(GridPagAssociadosCred.TextMatrix(I, 8)) <> 13 Then
        If Trim(GridPagAssociadosCred.TextMatrix(GridPagAssociadosCred.Row, 0)) = "" Then
           SQL = "Exec ps_mov_acerto_spu "
           SQL = SQL & GridPagAssociadosCred.TextMatrix(I, 7) & ","
           SQL = SQL & "null,"
           SQL = SQL & "'" & GridPagAssociadosCred.TextMatrix(I, 8) & "','"
           SQL = SQL & cUserName & "'"
           
           Set rc = rdocn.OpenResultset(SQL)
           
           rc.Close
        End If
    Else
        SQL = "Exec ps_mov_acerto_spu "
        SQL = SQL & GridPagAssociadosCred.TextMatrix(I, 7) & ","
        SQL = SQL & "null,"
        SQL = SQL & "'" & GridPagAssociadosCred.TextMatrix(I, 8) & "','"
        SQL = SQL & cUserName & "'"
           
        Set rc = rdocn.OpenResultset(SQL)
           
        rc.Close
    End If
Next I

For I = 1 To GridPagAssociadosDeb.Rows - 1
    If Trim(GridPagAssociadosDeb.TextMatrix(GridPagAssociadosDeb.Row, 0)) = "" Then
       SQL = "Exec ps_mov_acerto_spu "
       SQL = SQL & GridPagAssociadosDeb.TextMatrix(I, 7) & ","
       SQL = SQL & "null,"
       SQL = SQL & "'" & GridPagAssociadosDeb.TextMatrix(I, 8) & "','"
       SQL = SQL & cUserName & "'"
       
       Set rc = rdocn.OpenResultset(SQL)
       
       rc.Close
    End If
Next I

'SQL = "Exec ps_valor_acerto_spu "
'SQL = SQL & frmAprPagamentos.gridPagamentos.TextMatrix(frmAprPagamentos.gridPagamentos.Row, 2) & ","
'SQL = SQL & MudaVirgulaParaPonto(Val_Pag) & ",'"
'SQL = SQL & cUserName & "'"
'
'Set rc = rdocn.OpenResultset(SQL)
'
'rc.Close

rdocn.CommitTrans

MsgBox "Opera��o Efetuada.", vbInformation

Exit Sub

erro:
    TrataErroGeral "Aplicar"
    MsgBox "Erro na rotina Aplicar. Programa ser� cancelado."
    End
       
End Sub

Private Sub cmdSair_Click()
    Unload Me
    
End Sub

Private Sub Form_Load()

'cmdAplicar.Enabled = False

Dim SQL As String
Dim rc As rdoResultset
Dim linha As String
Dim rc1 As rdoResultset

On Error GoTo erro

'jose.souza - Confitec - Em 30/08/2013
bUsarConexaoABS = False

QtdCred = 0
QtdDeb = 0
ValCred = 0
ValDeb = 0

txtQtdCred = ""
txtQtdDeb = ""
txtValDeb = ""
txtValCred = ""

If UCase(TpAprovacao) = "COMISS�O RETIDA" Or UCase(TpAprovacao) = "PR�-LABORE RETIDO" Then
    GridPagAssociadosCred.FormatString = "Aprova��o|Dt Movimenta��o | Tipo Opera��o   | Valor               |Produto                                          |Ramo                                             | Retido          |Acerto   | Tp | val_dt_Movimenta��o"
    GridPagAssociadosDeb.FormatString = "Aprova��o|Dt Movimenta��o | Tipo Opera��o   | Valor               |Produto                                          |Ramo                                             | Retido          |Acerto   | Tp | val_dt_Movimenta��o"
Else
    GridPagAssociadosCred.FormatString = "Aprova��o|Dt Movimenta��o | Tipo Opera��o   | Valor               |Proposta    | Origem                        | Retido          |Acerto      | Tp | val_dt_Movimenta��o"
    GridPagAssociadosDeb.FormatString = "Aprova��o|Dt Movimenta��o | Tipo Opera��o   | Valor               |Proposta  | Origem                        | Retido          |Acerto      | Tp | val_dt_Movimenta��o"
End If

GridPagAssociadosCred.ColWidth(9) = 0           'Data Movimenta��o para ordenar o grid
GridPagAssociadosDeb.ColWidth(9) = 0            'Data Movimenta��o para ordenar o grid

CentraFrm Me

  Me.Caption = "SEG10270 - Pagamentos Associados - " & Ambiente

'mathayde
If bUsarConexaoABS Then
    If frmAprPagamentos.gridPagamentos.TextMatrix(frmAprPagamentos.gridPagamentos.Row, iColunaEmpresa) = glAmbiente_id_seg2 Then
        Set rdocn = rdocn_Seg2
    Else
        Set rdocn = rdocn_Seg
    End If
End If

SQL = Monta_Query_Associados()
Set rc = rdocn.OpenResultset(SQL)
If rc.EOF Then
    Screen.MousePointer = vbDefault
    MsgBox "Nenhum registro foi encontrado !"
    GridPagAssociadosCred.Rows = 1
    GridPagAssociadosDeb.Rows = 1
    Exit Sub
End If

txtTipoFavorecido = frmAprPagamentos.gridPagamentos.TextMatrix(frmAprPagamentos.gridPagamentos.Row, 3)
txtNomeFavorecido = frmAprPagamentos.gridPagamentos.TextMatrix(frmAprPagamentos.gridPagamentos.Row, 4)
'txtValor = frmAprPagamentos.gridPagamentos.TextMatrix(frmAprPagamentos.gridPagamentos.Row, 5)

GridPagAssociadosCred.Rows = 1
GridPagAssociadosCred.Redraw = False
GridPagAssociadosDeb.Rows = 1
GridPagAssociadosDeb.Redraw = False

While Not rc.EOF
    'Preenche_grid
    linha = "" & vbTab
    linha = linha & Format(rc!dt_movimentacao, "dd/mm/yyyy") & vbTab
    If UCase(rc!tp_operacao) = "C" Then
        linha = linha & "Cr�dito" & vbTab
        QtdCred = QtdCred + 1
        ValCred = ValCred + Val(rc!val_movimentacao)
    Else
        linha = linha & "D�bito" & vbTab
        QtdDeb = QtdDeb + 1
        ValDeb = ValDeb + Val(rc!val_movimentacao)
    End If
    
    If ConfiguracaoBrasil Then
        linha = linha & Format(Val(rc!val_movimentacao), "###,###,###,##0.00") & vbTab
    Else
        linha = linha & TrocaValorAmePorBras(Format(Val(rc!val_movimentacao), "###,###,###,##0.00")) & vbTab
    End If
    
    ' descobrir a origem com o lan�amento
    SQL = "SELECT origem = 'Pgto parcela ' + convert(char(3), num_cobranca), proposta_id "
    SQL = SQL & " FROM ps_mov_agenda_cobranca_tb   WITH (NOLOCK)  "
    SQL = SQL & " WHERE movimentacao_id = " & rc!movimentacao_id
    SQL = SQL & " UNION SELECT origem = 'Endosso ' + convert(char(5), endosso_id), proposta_id "
    SQL = SQL & " FROM ps_mov_endosso_financeiro_tb   WITH (NOLOCK)  "
    SQL = SQL & " WHERE movimentacao_id = " & rc!movimentacao_id
    If rc!tp_movimentacao = "6" Then
       SQL = SQL & " UNION SELECT origem = 'Sinistro ' + convert(char(11), sinistro_id), b.proposta_id "
       SQL = SQL & " FROM ps_mov_sinistro_tb a  WITH (NOLOCK)  , apolice_tb b   WITH (NOLOCK)  "
       SQL = SQL & " WHERE movimentacao_id = " & rc!movimentacao_id & " and "
       SQL = SQL & "       a.apolice_id = b.apolice_id and "
       SQL = SQL & "       a.sucursal_seguradora_id = b.sucursal_seguradora_id and "
       SQL = SQL & "       a.seguradora_cod_susep = b.seguradora_cod_susep and "
       SQL = SQL & "       a.ramo_id = b.ramo_id "
    End If

'mathayde 24/07/2009 demanda : 654224
    If rc!tp_movimentacao = "12" Then
        SQL = SQL & " UNION " & vbNewLine
        SQL = SQL & " SELECT origem = 'Pgto parcela ' + convert(char(3), repasse_cartao_credito_tb.num_cobranca), repasse_cartao_credito_tb.proposta_id" & vbNewLine
        SQL = SQL & " From ps_mov_repasse_cartao_credito_tb  WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & " Join repasse_cartao_credito_tb   WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "   ON repasse_cartao_credito_tb.repasse_cartao_credito_ID = ps_mov_repasse_cartao_credito_tb.repasse_cartao_credito_ID" & vbNewLine
        SQL = SQL & " Where ps_mov_repasse_cartao_credito_tb.movimentacao_id =" & rc!movimentacao_id & vbNewLine


    End If
    Set rc1 = rdoCn1.OpenResultset(SQL)
    If rc1.EOF Then
        If UCase(rc!retido_banco) = "S" Then 'UCase(TpAprovacao) = "COMISS�O RETIDA" Or UCase(TpAprovacao) = "PR�-LABORE RETIDO" Then
            linha = linha & Format(rc("produto_id"), "0000") & " - " & rc("produto_nome") & vbTab
            linha = linha & Format(rc("ramo_id"), "0000") & " - " & rc("ramo_nome") & vbTab
            
            GridPagAssociadosCred.ColAlignment(4) = 1
            GridPagAssociadosCred.ColAlignment(5) = 1
        
            GridPagAssociadosDeb.ColAlignment(4) = 1
            GridPagAssociadosDeb.ColAlignment(5) = 1
        
        Else
            linha = linha & " " & vbTab
            linha = linha & " " & vbTab
        End If
    Else
        linha = linha & rc1!proposta_id & vbTab
        linha = linha & rc1!Origem & vbTab
    End If
    rc1.Close
    
    If UCase(rc!retido_banco) = "S" Then
        linha = linha & "S" & vbTab
    Else
        linha = linha & "N" & vbTab
    End If
    
    linha = linha & rc!movimentacao_id & vbTab
    linha = linha & rc!tp_movimentacao & vbTab
    linha = linha & Format(rc!dt_movimentacao, "yyyymmdd")
    
    If UCase(rc!tp_operacao) = "C" Then
       GridPagAssociadosCred.AddItem linha
       GridPagAssociadosCred.Col = 0
       GridPagAssociadosCred.Row = GridPagAssociadosCred.Rows - 1
       GridPagAssociadosCred.CellFontName = "Monotype Sorts"
       GridPagAssociadosCred.Text = "4"
    Else
       GridPagAssociadosDeb.AddItem linha
       GridPagAssociadosDeb.Col = 0
       GridPagAssociadosDeb.Row = GridPagAssociadosDeb.Rows - 1
       GridPagAssociadosDeb.CellFontName = "Monotype Sorts"
       GridPagAssociadosDeb.Text = "4"
    End If
    
    rc.MoveNext
Wend
    rc.Close
    txtQtdCred = Format(QtdCred, "000000")
    txtQtdDeb = Format(QtdDeb, "000000")
    If ConfiguracaoBrasil Then
        txtValCred = Format(ValCred, "###,###,###,##0.00")
        txtValDeb = Format(ValDeb, "###,###,###,##0.00")
    Else
        txtValCred = TrocaValorAmePorBras(Format(Val(ValCred), "###,###,###,##0.00"))
        txtValDeb = TrocaValorAmePorBras(Format(Val(ValDeb), "###,###,###,##0.00"))
    End If
    
    GridPagAssociadosCred.Col = 0
    GridPagAssociadosCred.ColSel = 0
    GridPagAssociadosCred.Redraw = True
    GridPagAssociadosDeb.Col = 0
    GridPagAssociadosDeb.ColSel = 0
    GridPagAssociadosDeb.Redraw = True
    
    Screen.MousePointer = vbDefault
    
    'gridPagAssociadosCred.ColAlignment(0) = 4
    'gridPagAssociadosCred.Sort = 1

Exit Sub

erro:
    TrataErroGeral "Form Load"
    MsgBox "Erro na rotina Form Load. Programa ser� cancelado."
    End

End Sub

Private Sub Form_Unload(Cancel As Integer)

frmAprPagamentos.StatusBar1.SimpleText = "Selecione o(s) pagamento(s) a ser(em) aprovado(s)."

End Sub

Private Sub gridPagAssociadosCred_Click()
Dim valor_mov    As Currency

On Error GoTo erro

If GridPagAssociadosCred.Row = 0 Then
    Exit Sub
End If
    
If GridPagAssociadosCred.Row = 1 And GridPagAssociadosCred.RowSel = GridPagAssociadosCred.Rows - 1 Then
'Ordena a coluna conforme click do mouse sobre a mesma
    vColuna = GridPagAssociadosCred.MouseCol
    If vColuna = 1 Then
        vColuna = 9
    End If
    GridPagAssociadosCred.Col = vColuna
    GridPagAssociadosCred.Sort = 1
    Exit Sub
End If


If ConfiguracaoBrasil Then
   valor_mov = Val(Str(GridPagAssociadosCred.TextMatrix(GridPagAssociadosCred.Row, 3)))
Else
   valor_mov = Val(TrocaValorBrasPorAme(GridPagAssociadosCred.TextMatrix(GridPagAssociadosCred.Row, 3)))
End If

If (GridPagAssociadosCred.Col = 0 And GridPagAssociadosCred.Row <> 0) Then
    If GridPagAssociadosCred.Text = "4" Then
        If (ValCred - valor_mov) - ValDeb <= 0# Then
           MsgBox "Esta altera��o tornar� o valor da OP menor ou igual a zero. Opera��o n�o realizada.", vbCritical
           Exit Sub
        End If
        GridPagAssociadosCred.Text = ""
        QtdCred = QtdCred - 1
        ValCred = ValCred - valor_mov
    Else
        GridPagAssociadosCred.CellFontName = "Monotype Sorts"
        GridPagAssociadosCred.Text = "4"
        QtdCred = QtdCred + 1
        ValCred = ValCred + valor_mov
    End If
    
    
    
    txtQtdCred = Format(QtdCred, "000000")
    If ConfiguracaoBrasil Then
        txtValCred = Format(ValCred, "###,###,###,##0.00")
    Else
        txtValCred = TrocaValorAmePorBras(Format(Val(ValCred), "###,###,###,##0.00"))
    End If
End If

Exit Sub

erro:
    TrataErroGeral "gridPagAssociadosCred_Click", Me.name
    TerminaSEGBR

End Sub

Private Sub gridPagAssociadosDeb_Click()
Dim valor_mov    As Currency

On Error GoTo erro

If GridPagAssociadosDeb.Row = 0 Then
    Exit Sub
End If

If GridPagAssociadosDeb.Row = 1 And GridPagAssociadosDeb.RowSel = GridPagAssociadosDeb.Rows - 1 Then
'Ordena a coluna conforme click do mouse sobre a mesma
    vColuna = GridPagAssociadosDeb.MouseCol
    GridPagAssociadosDeb.Col = vColuna
    If vColuna = 1 Then
        vColuna = 9
    End If
    GridPagAssociadosDeb.Sort = 1
    Exit Sub
End If

If ConfiguracaoBrasil Then
   valor_mov = Val(Str(GridPagAssociadosDeb.TextMatrix(GridPagAssociadosDeb.Row, 3)))
Else
   valor_mov = Val(TrocaValorBrasPorAme(GridPagAssociadosDeb.TextMatrix(GridPagAssociadosDeb.Row, 3)))
End If

If (GridPagAssociadosDeb.Col = 0 And GridPagAssociadosDeb.Row <> 0) Then
    If GridPagAssociadosDeb.Text = "4" Then
        GridPagAssociadosDeb.Text = ""
        QtdDeb = QtdDeb - 1
        ValDeb = ValDeb - valor_mov
    Else
        GridPagAssociadosDeb.CellFontName = "Monotype Sorts"
        GridPagAssociadosDeb.Text = "4"
        QtdDeb = QtdDeb + 1
        ValDeb = ValDeb + valor_mov
    End If
    
    txtQtdDeb = Format(QtdDeb, "000000")
    If ConfiguracaoBrasil Then
        txtValDeb = Format(ValDeb, "###,###,###,##0.00")
    Else
        txtValDeb = TrocaValorAmePorBras(Format(Val(ValDeb), "###,###,###,##0.00"))
    End If
End If

Exit Sub

erro:
    TrataErroGeral "gridPagAssociadosDeb_Click", Me.name
    TerminaSEGBR

End Sub


