VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSelPagamentos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "SEG07007 - Sele��o de Pagamentos - "
   ClientHeight    =   4650
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   7980
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4650
   ScaleWidth      =   7980
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdLimpar 
      Caption         =   "Limpar"
      Height          =   375
      Left            =   240
      TabIndex        =   20
      Top             =   3840
      Width           =   1215
   End
   Begin VB.Frame fraProposta 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   3960
      TabIndex        =   11
      Top             =   1680
      Width           =   3735
      Begin VB.TextBox txtApolice 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1320
         MaxLength       =   18
         TabIndex        =   15
         Top             =   840
         Width           =   2100
      End
      Begin VB.TextBox txtProposta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1320
         MaxLength       =   18
         TabIndex        =   13
         Top             =   360
         Width           =   2100
      End
      Begin VB.TextBox txtRamo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1320
         MaxLength       =   60
         TabIndex        =   17
         Top             =   1320
         Width           =   2100
      End
      Begin VB.Label lblApolice 
         Alignment       =   1  'Right Justify
         Caption         =   "Ap�lice:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   14
         Top             =   840
         Width           =   975
      End
      Begin VB.Label lblProposta 
         Alignment       =   1  'Right Justify
         Caption         =   "Proposta:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   12
         Top             =   360
         Width           =   975
      End
      Begin VB.Label lblRamo 
         Alignment       =   1  'Right Justify
         Caption         =   "Ramo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   16
         Top             =   1320
         Width           =   975
      End
   End
   Begin VB.Frame fraPeriodo 
      Caption         =   "Per�odo dos Pagamentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   240
      TabIndex        =   6
      Top             =   1680
      Width           =   3615
      Begin MSMask.MaskEdBox mkeInicio 
         Height          =   375
         Left            =   1320
         TabIndex        =   8
         Top             =   360
         Width           =   2100
         _ExtentX        =   3704
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "dd/mm/yyyy"
         Mask            =   "99/99/9999"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mkeTermino 
         Height          =   375
         Left            =   1320
         TabIndex        =   10
         Top             =   840
         Width           =   2100
         _ExtentX        =   3704
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "dd/mm/yyyy"
         Mask            =   "99/99/9999"
         PromptChar      =   "_"
      End
      Begin VB.Label lblTermino 
         Alignment       =   1  'Right Justify
         Caption         =   "T�rmino:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label lblInicio 
         Alignment       =   1  'Right Justify
         Caption         =   "In�cio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame fraFavorecido 
      Caption         =   "Favorecido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   7455
      Begin VB.TextBox txtNome 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1320
         MaxLength       =   60
         TabIndex        =   3
         Top             =   360
         Width           =   5895
      End
      Begin MSMask.MaskEdBox mkeDocumento 
         Height          =   375
         Left            =   1320
         TabIndex        =   5
         Top             =   840
         Width           =   2100
         _ExtentX        =   3704
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "_"
      End
      Begin VB.Label lblDocumento 
         Alignment       =   1  'Right Justify
         Caption         =   "CPF/CNPJ:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label lblNome 
         Alignment       =   1  'Right Justify
         Caption         =   "Nome:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   375
      Left            =   6480
      TabIndex        =   18
      Top             =   3840
      Width           =   1215
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4920
      TabIndex        =   0
      Top             =   3840
      Width           =   1215
   End
   Begin MSComctlLib.StatusBar stbSelecao 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   19
      Top             =   4395
      Width           =   7980
      _ExtentX        =   14076
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSelPagamentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'--[ Demanda 18721099 ]---- Cl�udia Eduarda - Confitec 25/05/2015 -------------------------------------
Dim rdoFiltro As rdoResultset
Dim rdoValida As rdoResultset

Dim sqlFiltro As String
Dim sqlValida As String

Dim buscaNome As String
Dim buscaDocumento As String
Dim buscaRamo As String
Dim buscaApolice As String

Public resFiltro As String

Option Explicit

Private Sub cmdLimpar_Click()
txtNome = ""
mkeDocumento = ""

txtProposta = ""
txtApolice = ""
txtRamo = ""

txtNome.Enabled = True
txtApolice.Enabled = True
txtRamo.Enabled = True

txtNome.BackColor = vbWindowBackground
txtApolice.BackColor = vbWindowBackground
txtRamo.BackColor = vbWindowBackground

mkeInicio = DateSerial(Year(Now - 30), Month(Now - 30), Day(Now - 30))
mkeTermino = DateSerial(Year(Now), Month(Now), Day(Now))

sqlFiltro = ""
resFiltro = ""

stbSelecao.SimpleText = ""

If txtNome.Visible And txtNome.Enabled Then txtNome.SetFocus
End Sub

Private Sub Form_Activate()
Screen.MousePointer = vbDefault
stbSelecao.SimpleText = ""
If txtNome.Enabled Then
   txtNome.SetFocus
   SelectAllText txtNome
Else
   mkeDocumento.SetFocus
   SelectAllText mkeDocumento
End If
End Sub

Private Sub Form_GotFocus()
If Me.Visible Then Me.Refresh
End Sub

Private Sub Form_Load()
CentraFrm Me
Me.Caption = "SEG07007 - Sele��o de " & TpAprovacao & " - " & Ambiente
fraFavorecido.Caption = "Favorecido (Pagamentos" & IIf(flagAprovados, "", " N�o") & " Aprovados" & ")"
cmdLimpar_Click
End Sub

Private Sub cmdOk_Click()

buscaNome = ""
buscaDocumento = ""
buscaRamo = ""
buscaApolice = ""

If Trim(txtProposta) = "" Then
   buscaRamo = Trim(txtRamo)
   buscaApolice = Trim(txtApolice)
End If

If Trim(mkeInicio) = "" Or Not IsDate(mkeInicio) Then mkeInicio = DateSerial(Year(Now - 30), Month(Now - 30), Day(Now - 30))
If Trim(mkeTermino) = "" Or Not IsDate(mkeTermino) Then mkeTermino = DateSerial(Year(Now), Month(Now), Day(Now))
If CDate(mkeTermino) < CDate(mkeInicio) Then mkeTermino = mkeInicio

If Trim(mkeDocumento) <> "" Then
   buscaDocumento = Trim(mkeDocumento)
   While InStr(buscaDocumento, " ") > 0
      buscaDocumento = Replace(buscaDocumento, " ", "")
   Wend
   buscaDocumento = Replace(buscaDocumento, ".", "")
   buscaDocumento = Replace(buscaDocumento, "-", "")
   buscaDocumento = Replace(buscaDocumento, "/", "")
   buscaDocumento = Replace(buscaDocumento, "\", "")
End If

If Len(buscaDocumento) <> 11 And Len(buscaDocumento) <> 14 Then
   If Trim(txtNome) <> "" Then
      buscaNome = UCase(Trim(txtNome))
      While InStr(buscaNome, "  ") > 0
         buscaNome = Replace(buscaNome, "  ", " ")
      Wend
      buscaNome = "%" + Replace(buscaNome, " ", "%") + "%"
   End If
   If Trim(buscaDocumento) <> "" Then buscaDocumento = "%" + Trim(buscaDocumento) + "%"
End If

stbSelecao.SimpleText = "Aguarde... Pesquisando por pagamentos no per�odo solicitado."
Screen.MousePointer = vbHourglass

resFiltro = ""
sqlFiltro = ""

If UCase(TpAprovacao) = "CORRETAGEM N�O-RETIDA" Then
   SQL_Pesquisa_Corretagem
Else
   SQL_Pesquisa_Pro_Labore
End If

Set rdoFiltro = rdocn.OpenResultset(sqlFiltro)

stbSelecao.SimpleText = ""
     
If rdoFiltro.EOF Then
   rdoFiltro.Close
   Screen.MousePointer = vbDefault
   MsgBox "N�o existe(m) pagamento(s) com estes crit�rios!", vbExclamation
   Form_Activate
Else
   If buscaNome + buscaDocumento + buscaRamo + buscaApolice + Trim(txtProposta) <> "" Then
      While Not rdoFiltro.EOF
         resFiltro = resFiltro + IIf(resFiltro = "", "", ",") + CStr(rdoFiltro(0))
         rdoFiltro.MoveNext
      Wend
      resFiltro = "   AND d.acerto_id IN (" + resFiltro + ")"
   End If
   resFiltro = "   AND d.dt_geracao BETWEEN '" & Format(mkeInicio, "yyyy-mm-dd") & "' AND '" & Format(mkeTermino, "yyyy-mm-dd") & "'" & vbNewLine & IIf(resFiltro <> "", resFiltro & vbNewLine, "")
   rdoFiltro.Close
   frmAprPagamentos.Show vbModal, Me
End If

End Sub

Private Sub cmdSair_Click()
   Unload Me
End Sub

Sub SQL_Pesquisa_Corretagem()

sqlFiltro = "SELECT TOP 25000 a.acerto_id" & vbNewLine
sqlFiltro = sqlFiltro & "  FROM seguros_db.dbo.ps_acerto_tb AS a WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "       INNER JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.ps_acerto_pagamento_tb AS p WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON a.acerto_id = p.acerto_id" & vbNewLine
sqlFiltro = sqlFiltro & "       LEFT JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.ps_acerto_corretor_pf_tb AS f WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON f.acerto_id = a.acerto_id" & vbNewLine
sqlFiltro = sqlFiltro & "       LEFT JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.ps_acerto_corretor_pj_tb AS j WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON j.acerto_id = a.acerto_id" & vbNewLine
If buscaNome + buscaDocumento <> "" Then
sqlFiltro = sqlFiltro & "       INNER JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.corretor_tb AS c WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON c.corretor_id IN (f.pf_corretor_id,j.pj_corretor_id)" & vbNewLine
sqlFiltro = sqlFiltro & "       LEFT JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.sucursal_corretor_tb AS s WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON s.pj_corretor_id       = j.pj_corretor_id" & vbNewLine
sqlFiltro = sqlFiltro & "   AND s.sucursal_corretor_id = j.sucursal_corretor_id" & vbNewLine
sqlFiltro = sqlFiltro & "       LEFT JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.corretor_pf_tb AS cf WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON cf.pf_corretor_id = f.pf_corretor_id" & vbNewLine
End If
sqlFiltro = sqlFiltro & " WHERE a.dt_geracao BETWEEN '" & Format(mkeInicio, "yyyy-mm-dd") & "' AND '" & Format(mkeTermino, "yyyy-mm-dd") & "'" & vbNewLine
sqlFiltro = sqlFiltro & "   AND p.situacao " & IIf(flagAprovados, "IN ('a','e')", " = 'p' AND a.retido_banco = 'n'") & vbNewLine
sqlFiltro = sqlFiltro & "   AND COALESCE(f.acerto_id,j.acerto_id) > 0" & vbNewLine

If buscaNome <> "" Then
sqlFiltro = sqlFiltro & "   AND ( c.nome LIKE '" & buscaNome & "'" & vbNewLine
sqlFiltro = sqlFiltro & "    OR   s.nome LIKE '" & buscaNome & "' )" & vbNewLine
End If

If buscaDocumento <> "" Then
   If InStr(buscaDocumento, "%") > 0 Then
      sqlFiltro = sqlFiltro & "   AND ( cf.cpf LIKE '" & buscaDocumento & "'" & vbNewLine
      sqlFiltro = sqlFiltro & "    OR    s.cgc LIKE '" & buscaDocumento & "' )" & vbNewLine
   ElseIf Len(buscaDocumento) = 11 Then
      sqlFiltro = sqlFiltro & "   AND cf.cpf = '" & buscaDocumento & "'" & vbNewLine
   ElseIf Len(buscaDocumento) = 14 Then
      sqlFiltro = sqlFiltro & "   AND s.cgc = '" & buscaDocumento & "'" & vbNewLine
   End If
End If

sqlFiltro = sqlFiltro & "   AND EXISTS ( SELECT 1" & vbNewLine
sqlFiltro = sqlFiltro & "                  FROM ( SELECT mpf.movimentacao_id" & vbNewLine
sqlFiltro = sqlFiltro & "                           FROM seguros_db.dbo.ps_mov_corretor_pf_tb AS mpf WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "                          WHERE mpf.acerto_id = a.acerto_id" & vbNewLine
sqlFiltro = sqlFiltro & "                          UNION" & vbNewLine
sqlFiltro = sqlFiltro & "                         SELECT mpj.movimentacao_id" & vbNewLine
sqlFiltro = sqlFiltro & "                           FROM seguros_db.dbo.ps_mov_corretor_pj_tb AS mpj WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "                          WHERE mpj.acerto_id = a.acerto_id ) AS mov" & vbNewLine
sqlFiltro = sqlFiltro & "                 WHERE EXISTS ( SELECT 1" & vbNewLine
sqlFiltro = sqlFiltro & "                                  FROM seguros_db.dbo.ps_movimentacao_tb AS ms WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "                                 WHERE ms.movimentacao_id = mov.movimentacao_id " & vbNewLine
sqlFiltro = sqlFiltro & "                                   AND COALESCE(ms.situacao,'*') <> 't' )"

If Trim(txtProposta + buscaRamo + buscaApolice) <> "" Then
   sqlFiltro = sqlFiltro & vbNewLine
   sqlFiltro = sqlFiltro & "                   AND mov.movimentacao_id IN ( SELECT ef.movimentacao_id" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                  FROM seguros_db.dbo.ps_mov_endosso_financeiro_tb AS ef WITH (NOLOCK)" & vbNewLine
   If Trim(txtProposta) <> "" Then
   sqlFiltro = sqlFiltro & "                                                 WHERE ef.proposta_id = " & Trim(txtProposta)
   Else
   sqlFiltro = sqlFiltro & "                                                       INNER JOIN" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                       seguros_db.dbo.proposta_tb AS pr WITH (NOLOCK)" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                    ON pr.proposta_id = ef.proposta_id" & vbNewLine
   If buscaApolice <> "" Then
   sqlFiltro = sqlFiltro & "                                                       INNER JOIN" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                       seguros_db.dbo.apolice_tb AS ap WITH (NOLOCK)" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                    ON ap.proposta_id = pr.proposta_id" & vbNewLine
   End If
   sqlFiltro = sqlFiltro & "                                                 WHERE ef.movimentacao_id = mov.movimentacao_id" & vbNewLine
   If buscaRamo <> "" Then
   sqlFiltro = sqlFiltro & "                                                   AND pr.ramo_id = " & buscaRamo & vbNewLine
   End If
   If buscaApolice <> "" Then
   sqlFiltro = sqlFiltro & "                                                   AND ap.apolice_id = " & buscaApolice & vbNewLine
   End If
   End If
   sqlFiltro = sqlFiltro & "                                                 UNION" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                SELECT ac.movimentacao_id" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                  FROM seguros_db.dbo.ps_mov_agenda_cobranca_tb AS ac WITH (NOLOCK)" & vbNewLine
   If Trim(txtProposta) <> "" Then
   sqlFiltro = sqlFiltro & "                                                 WHERE ac.proposta_id = " & Trim(txtProposta)
   Else
   sqlFiltro = sqlFiltro & "                                                       INNER JOIN" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                       seguros_db.dbo.proposta_tb AS pr WITH (NOLOCK)" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                    ON pr.proposta_id = ac.proposta_id" & vbNewLine
   If buscaApolice <> "" Then
   sqlFiltro = sqlFiltro & "                                                       INNER JOIN" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                       seguros_db.dbo.apolice_tb AS ap WITH (NOLOCK)" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                    ON ap.proposta_id = pr.proposta_id" & vbNewLine
   End If
   sqlFiltro = sqlFiltro & "                                                 WHERE ac.movimentacao_id = mov.movimentacao_id" & vbNewLine
   If buscaRamo <> "" Then
   sqlFiltro = sqlFiltro & "                                                   AND pr.ramo_id = " & buscaRamo & IIf(buscaApolice = "", "", Chr(13))
   End If
   If buscaApolice <> "" Then
   sqlFiltro = sqlFiltro & "                                                   AND ap.apolice_id = " & buscaApolice
   End If
   End If
   sqlFiltro = sqlFiltro & ") "
End If
sqlFiltro = sqlFiltro & " )" & vbNewLine

End Sub

Sub SQL_Pesquisa_Pro_Labore()

sqlFiltro = "SELECT TOP 25000 a.acerto_id" & vbNewLine
sqlFiltro = sqlFiltro & "  FROM seguros_db.dbo.ps_acerto_tb AS a WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "       INNER JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.ps_acerto_pagamento_tb AS p WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON a.acerto_id = p.acerto_id" & vbNewLine
sqlFiltro = sqlFiltro & "       INNER JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.ps_acerto_estip_tb AS e WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON e.acerto_id = a.acerto_id" & vbNewLine
If buscaNome + buscaDocumento <> "" Then
sqlFiltro = sqlFiltro & "       INNER JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.cliente_tb AS c WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON c.cliente_id = e.cliente_id" & vbNewLine
sqlFiltro = sqlFiltro & "       LEFT JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.pessoa_fisica_tb AS f WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON f.pf_cliente_id = c.cliente_id" & vbNewLine
sqlFiltro = sqlFiltro & "       LEFT JOIN" & vbNewLine
sqlFiltro = sqlFiltro & "       seguros_db.dbo.pessoa_juridica_tb AS j WITH (NOLOCK)" & vbNewLine
sqlFiltro = sqlFiltro & "    ON j.pj_cliente_id = c.cliente_id" & vbNewLine
End If
sqlFiltro = sqlFiltro & " WHERE a.dt_geracao BETWEEN '" & Format(mkeInicio, "yyyy-mm-dd") & "' AND '" & Format(mkeTermino, "yyyy-mm-dd") & "'" & vbNewLine
sqlFiltro = sqlFiltro & "   AND p.situacao " & IIf(flagAprovados, "IN ('a','e')", " = 'p' AND a.retido_banco = 'n' ") & vbNewLine

If buscaNome <> "" Then
sqlFiltro = sqlFiltro & "   AND ( c.nome LIKE '" & buscaNome & "'" & vbNewLine
sqlFiltro = sqlFiltro & "    OR   j.nome_fantasia LIKE '" & buscaNome & "' )" & vbNewLine
End If

If buscaDocumento <> "" Then
sqlFiltro = sqlFiltro & "   AND ( c.cpf_cnpj " & IIf(InStr(buscaDocumento, "%") > 0, "LIKE", "=") & " '" & buscaDocumento & "'" & vbNewLine
sqlFiltro = sqlFiltro & "    OR f.cpf " & IIf(InStr(buscaDocumento, "%") > 0, "LIKE", "=") & " '" & buscaDocumento & "'" & vbNewLine
sqlFiltro = sqlFiltro & "    OR j.cgc " & IIf(InStr(buscaDocumento, "%") > 0, "LIKE", "=") & " '" & buscaDocumento & "' )" & vbNewLine
End If
   
If Trim(txtProposta + buscaRamo + buscaApolice) <> "" Then
   sqlFiltro = sqlFiltro & "   AND EXISTS ( SELECT mov.movimentacao_id" & vbNewLine
   sqlFiltro = sqlFiltro & "                  FROM seguros_db.dbo.ps_mov_estipulante_tb AS mov WITH (NOLOCK)" & vbNewLine
   sqlFiltro = sqlFiltro & "                 WHERE mov.acerto_id = a.acerto_id" & vbNewLine
   sqlFiltro = sqlFiltro & "                   AND mov.movimentacao_id IN ( SELECT ef.movimentacao_id" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                  FROM seguros_db.dbo.ps_mov_endosso_financeiro_tb AS ef WITH (NOLOCK)" & vbNewLine
   If Trim(txtProposta) <> "" Then
   sqlFiltro = sqlFiltro & "                                                 WHERE ef.proposta_id = " & Trim(txtProposta)
   Else
   sqlFiltro = sqlFiltro & "                                                       INNER JOIN" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                       seguros_db.dbo.proposta_tb AS pr WITH (NOLOCK)" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                    ON pr.proposta_id = ef.proposta_id" & vbNewLine
   If buscaApolice <> "" Then
   sqlFiltro = sqlFiltro & "                                                       INNER JOIN" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                       seguros_db.dbo.apolice_tb AS ap WITH (NOLOCK)" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                    ON ap.proposta_id = pr.proposta_id" & vbNewLine
   End If
   sqlFiltro = sqlFiltro & "                                                 WHERE ef.movimentacao_id = mov.movimentacao_id" & vbNewLine
   If buscaRamo <> "" Then
   sqlFiltro = sqlFiltro & "                                                   AND pr.ramo_id = " & buscaRamo & vbNewLine
   End If
   If buscaApolice <> "" Then
   sqlFiltro = sqlFiltro & "                                                   AND ap.apolice_id = " & buscaApolice & vbNewLine
   End If
   End If
   sqlFiltro = sqlFiltro & "                                                 UNION" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                SELECT ac.movimentacao_id" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                  FROM seguros_db.dbo.ps_mov_agenda_cobranca_tb AS ac WITH (NOLOCK)" & vbNewLine
   If Trim(txtProposta) <> "" Then
   sqlFiltro = sqlFiltro & "                                                 WHERE ac.proposta_id = " & Trim(txtProposta)
   Else
   sqlFiltro = sqlFiltro & "                                                       INNER JOIN" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                       seguros_db.dbo.proposta_tb AS pr WITH (NOLOCK)" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                    ON pr.proposta_id = ac.proposta_id" & vbNewLine
   If buscaApolice <> "" Then
   sqlFiltro = sqlFiltro & "                                                       INNER JOIN" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                       seguros_db.dbo.apolice_tb AS ap WITH (NOLOCK)" & vbNewLine
   sqlFiltro = sqlFiltro & "                                                    ON ap.proposta_id = pr.proposta_id" & vbNewLine
   End If
   sqlFiltro = sqlFiltro & "                                                 WHERE ac.movimentacao_id = mov.movimentacao_id" & vbNewLine
   If buscaRamo <> "" Then
   sqlFiltro = sqlFiltro & "                                                   AND pr.ramo_id = " & buscaRamo & IIf(buscaApolice = "", Chr(13), "")
   End If
   If buscaApolice <> "" Then
   sqlFiltro = sqlFiltro & "                                                   AND ap.apolice_id = " & buscaApolice
   End If
   End If
   sqlFiltro = sqlFiltro & ") )" & vbNewLine
End If

End Sub

Sub SelectAllText(box As Object)
box.SelStart = 0
box.SelLength = Len(box.Text)
End Sub

Private Sub mkeDocumento_GotFocus()
SelectAllText mkeDocumento
txtNome.Enabled = True
txtNome.BackColor = vbWindowBackground
End Sub

Private Sub mkeDocumento_LostFocus()
   If Len(Trim(mkeDocumento)) = 11 Then
      mkeDocumento = Format(mkeDocumento, "000\.000\.000\-00")
   ElseIf Len(Trim(mkeDocumento)) = 14 Then
      mkeDocumento = Format(mkeDocumento, "00\.000\.000\/0000\-00")
   End If
End Sub

Private Sub mkeDocumento_Validate(Cancel As Boolean)
stbSelecao.SimpleText = ""
buscaDocumento = ""
txtNome.Enabled = True
txtNome.BackColor = vbWindowBackground
If Trim(mkeDocumento) <> "" Then
   buscaDocumento = Trim(mkeDocumento)
      
   While InStr(buscaDocumento, " ") > 0
      buscaDocumento = Replace(buscaDocumento, " ", "")
   Wend
   
   buscaDocumento = Replace(buscaDocumento, ".", "")
   buscaDocumento = Replace(buscaDocumento, "-", "")
   buscaDocumento = Replace(buscaDocumento, "/", "")
   buscaDocumento = Replace(buscaDocumento, "\", "")
   
   If Not IsNumeric(buscaDocumento) Then Cancel = True
End If
If Not Cancel Then
   If buscaDocumento <> "" Then
      
      sqlValida = ""
        
      If UCase(TpAprovacao) = "CORRETAGEM N�O-RETIDA" Then
         If Len(buscaDocumento) = 11 Then
            sqlValida = sqlValida & "SELECT nome = c.nome, sucursal = ''" & vbNewLine
            sqlValida = sqlValida & "  FROM seguros_db.dbo.corretor_pf_tb AS f WITH (NOLOCK) INNER JOIN" & vbNewLine
            sqlValida = sqlValida & "       seguros_db.dbo.corretor_tb    AS c WITH (NOLOCK) ON c.corretor_id = f.pf_corretor_id" & vbNewLine
            sqlValida = sqlValida & " WHERE f.cpf = '" & buscaDocumento & "'" & vbNewLine
         ElseIf Len(buscaDocumento) = 14 Then
            sqlValida = sqlValida & "SELECT nome = rtrim(c.nome), sucursal = CASE WHEN s.nome <> c.nome THEN ltrim(s.nome) ELSE '' END" & vbNewLine
            sqlValida = sqlValida & "  FROM seguros_db.dbo.sucursal_corretor_tb AS s WITH (NOLOCK) INNER JOIN" & vbNewLine
            sqlValida = sqlValida & "       seguros_db.dbo.corretor_tb          AS c WITH (NOLOCK) ON c.corretor_id = s.pj_corretor_id" & vbNewLine
            sqlValida = sqlValida & " WHERE s.cgc = '" & buscaDocumento & "'" & vbNewLine
         End If
      ElseIf Len(buscaDocumento) = 11 Or Len(buscaDocumento) = 14 Then
         sqlValida = sqlValida & "SELECT DISTINCT nome, sucursal" & vbNewLine
         sqlValida = sqlValida & "  FROM ( SELECT c.nome, '' sucursal" & vbNewLine
         sqlValida = sqlValida & "          FROM seguros_db.dbo.cliente_tb AS c WITH (NOLOCK)" & vbNewLine
         sqlValida = sqlValida & "         WHERE c.cpf_cnpj = '" & buscaDocumento & "'" & vbNewLine
         sqlValida = sqlValida & "         UNION" & vbNewLine
         sqlValida = sqlValida & "        SELECT c.nome, ''" & vbNewLine
         sqlValida = sqlValida & "          FROM seguros_db.dbo.pessoa_fisica_tb AS f WITH (NOLOCK)" & vbNewLine
         sqlValida = sqlValida & "               INNER JOIN" & vbNewLine
         sqlValida = sqlValida & "               seguros_db.dbo.cliente_tb AS c WITH (NOLOCK)" & vbNewLine
         sqlValida = sqlValida & "            ON f.pf_cliente_id = c.cliente_id" & vbNewLine
         sqlValida = sqlValida & "         WHERE f.cpf = '" & buscaDocumento & "'" & vbNewLine
         sqlValida = sqlValida & "         UNION" & vbNewLine
         sqlValida = sqlValida & "        SELECT c.nome, CASE WHEN c.nome <> j.nome_fantasia THEN j.nome_fantasia ELSE '' END" & vbNewLine
         sqlValida = sqlValida & "          FROM seguros_db.dbo.pessoa_juridica_tb AS j WITH (NOLOCK) " & vbNewLine
         sqlValida = sqlValida & "               INNER JOIN" & vbNewLine
         sqlValida = sqlValida & "               seguros_db.dbo.cliente_tb AS c WITH (NOLOCK)" & vbNewLine
         sqlValida = sqlValida & "            ON j.pj_cliente_id = c.cliente_id" & vbNewLine
         sqlValida = sqlValida & "         WHERE j.cgc = '" & buscaDocumento & "' ) AS cliente" & vbNewLine
      End If
      
      If sqlValida = "" Then
         mkeDocumento = buscaDocumento
      Else
         
         Set rdoValida = rdocn.OpenResultset(sqlValida)
                  
         If rdoValida.EOF Then
            Cancel = True
         Else
            stbSelecao.SimpleText = rdoValida!sucursal
            txtNome = rdoValida!nome
            txtNome.Enabled = False
            txtNome.BackColor = vbInfoBackground
         End If
         rdoValida.Close
      End If
   End If
End If
If Cancel Then
   If mkeDocumento.SelLength = Len(mkeDocumento) Then
      mkeDocumento = ""
   Else
      SelectAllText mkeDocumento
   End If
   stbSelecao.SimpleText = "Documento de " & IIf(UCase(TpAprovacao) = "CORRETAGEM N�O-RETIDA", "Corretor", "Cliente") & " inv�lido ou n�o cadastrado!"
   If Not txtNome.Enabled Then
      txtNome.Enabled = True
      txtNome.BackColor = vbWindowBackground
   End If
End If
End Sub

Private Sub mkeInicio_GotFocus()
SelectAllText mkeInicio
End Sub

Private Sub mkeInicio_Validate(Cancel As Boolean)
If Trim(mkeInicio) = "" Or Not IsDate(mkeInicio) Then
   stbSelecao.SimpleText = IIf(Trim(mkeInicio) = "", "Defina a data de in�cio do per�odo!", "Data de in�cio inv�lida!")
   mkeInicio = DateSerial(Year(Now), Month(Now), 1)
   Cancel = True
Else
   stbSelecao.SimpleText = ""
End If
If Cancel Then SelectAllText mkeInicio
End Sub

Private Sub mkeTermino_GotFocus()
SelectAllText mkeTermino
End Sub

Private Sub mkeTermino_Validate(Cancel As Boolean)
If Trim(mkeTermino) = "" Or Not IsDate(mkeTermino) Then
   stbSelecao.SimpleText = IIf(Trim(mkeTermino) = "", "Defina a data de t�rmino do per�odo!", "Data de t�rmino inv�lida!")
   mkeTermino = DateSerial(Year(Now), Month(Now), Day(Now))
   Cancel = True
ElseIf CDate(mkeTermino) < CDate(mkeInicio) Then
   stbSelecao.SimpleText = "Data de t�rmino n�o pode ser menor que a de in�cio!"
   mkeTermino = mkeInicio
   Cancel = True
Else
   stbSelecao.SimpleText = ""
End If
If Cancel Then SelectAllText mkeTermino
End Sub

Private Sub txtApolice_GotFocus()
SelectAllText txtApolice
txtRamo.Enabled = True
txtRamo.BackColor = vbWindowBackground
End Sub

Private Sub txtApolice_Validate(Cancel As Boolean)
stbSelecao.SimpleText = ""
txtRamo.Enabled = True
txtRamo.BackColor = vbWindowBackground
If Trim(txtApolice) <> "" And Not IsNumeric(txtApolice) Then Cancel = True
If Not Cancel Then
   If Trim(txtApolice) <> "" Then
      sqlValida = "            SELECT ramo = MIN(a.ramo_id), apolice = rtrim(MIN(r.nome)) + ' ( ' + ltrim(MIN(t.nome))+' )', controle = COUNT(DISTINCT a.ramo_id)" & vbNewLine
      sqlValida = sqlValida & "  FROM seguros_db.dbo.apolice_tb AS a WITH (NOLOCK)                                LEFT JOIN" & vbNewLine
      sqlValida = sqlValida & "       seguros_db.dbo.ramo_tb    AS r WITH (NOLOCK) ON r.ramo_id    = a.ramo_id    LEFT JOIN" & vbNewLine
      sqlValida = sqlValida & "       seguros_db.dbo.tp_ramo_tb AS t WITH (NOLOCK) ON r.tp_ramo_id = t.tp_ramo_id" & vbNewLine
      sqlValida = sqlValida & " WHERE a.apolice_id = " & Trim(txtApolice) & vbNewLine
   
      Set rdoValida = rdocn.OpenResultset(sqlValida)
     
     If rdoValida.EOF Or rdoValida!Controle <= 0 Then
         Cancel = True
      Else
         If rdoValida!Controle = 1 Then
            stbSelecao.SimpleText = rdoValida!apolice
            txtRamo = rdoValida!ramo
            txtRamo.Enabled = False
            txtRamo.BackColor = vbInfoBackground
            cmdOk.SetFocus
         End If
      End If
      rdoValida.Close
   End If
End If
If Cancel Then
   If txtApolice.SelLength = Len(txtApolice) Then
      txtApolice = ""
   Else
      SelectAllText txtApolice
   End If
   stbSelecao.SimpleText = "Ap�lice inv�lida ou n�o cadastrada!"
End If
End Sub

Private Sub txtProposta_GotFocus()
SelectAllText txtProposta
txtRamo.Enabled = True
txtRamo.BackColor = vbWindowBackground
txtApolice.Enabled = True
txtApolice.BackColor = vbWindowBackground
End Sub

Private Sub txtProposta_Validate(Cancel As Boolean)
stbSelecao.SimpleText = ""
txtRamo.Enabled = True
txtRamo.BackColor = vbWindowBackground
txtApolice.Enabled = True
txtApolice.BackColor = vbWindowBackground
If Trim(txtProposta) <> "" And Not IsNumeric(txtProposta) Then Cancel = True
If Not Cancel Then
   If Trim(txtProposta) <> "" Then
      sqlValida = "            SELECT ramo = p.ramo_id, apolice = COALESCE(CAST(a.apolice_id AS VARCHAR),''), Cliente = rtrim(c.nome), Produto = ' ( '+ltrim(d.nome)+' )' " & vbNewLine
      sqlValida = sqlValida & "  FROM proposta_tb AS p WITH (NOLOCK)                                     INNER JOIN" & vbNewLine
      sqlValida = sqlValida & "       cliente_tb  AS c WITH (NOLOCK) ON p.prop_cliente_id = c.cliente_id INNER JOIN" & vbNewLine
      sqlValida = sqlValida & "       produto_tb  AS d WITH (NOLOCK) ON p.produto_id      = d.produto_id LEFT  JOIN" & vbNewLine
      sqlValida = sqlValida & "       apolice_tb  AS a WITH (NOLOCK) ON a.proposta_id     = p.proposta_id" & vbNewLine
      sqlValida = sqlValida & " WHERE p.proposta_id = " & Trim(txtProposta) & vbNewLine
   
      Set rdoValida = rdocn.OpenResultset(sqlValida)
     
      If rdoValida.EOF Then
         Cancel = True
      Else
         stbSelecao.SimpleText = rdoValida!Cliente & rdoValida!Produto
         If Trim(rdoValida!ramo) <> "" Then txtRamo = rdoValida!ramo
         If Trim(rdoValida!apolice) <> "" Then txtApolice = rdoValida!apolice
         txtRamo.Enabled = False
         txtRamo.BackColor = vbInfoBackground
         txtApolice.Enabled = False
         txtApolice.BackColor = vbInfoBackground
         cmdOk.SetFocus
      End If
      rdoValida.Close
   End If
End If
If Cancel Then
   If txtProposta.SelLength = Len(txtProposta) Then
      txtProposta = ""
   Else
      SelectAllText txtProposta
   End If
   stbSelecao.SimpleText = "Proposta inv�lida ou n�o cadastrada!"
End If
End Sub

Private Sub txtRamo_GotFocus()
SelectAllText txtRamo
End Sub

Private Sub txtRamo_Validate(Cancel As Boolean)
stbSelecao.SimpleText = ""
If Trim(txtRamo) <> "" And Not IsNumeric(txtRamo) Then Cancel = True
If Not Cancel Then
   If Trim(txtRamo) <> "" Then
      If Trim(txtApolice) <> "" Then
         sqlValida = "            SELECT ramo = rtrim(MIN(r.nome)) + ' ( ' + ltrim(MIN(t.nome))+' )', controle = COALESCE(COUNT(DISTINCT a.ramo_id),0)" & vbNewLine
         sqlValida = sqlValida & "  FROM seguros_db.dbo.apolice_tb AS a WITH (NOLOCK)                                LEFT JOIN" & vbNewLine
         sqlValida = sqlValida & "       seguros_db.dbo.ramo_tb    AS r WITH (NOLOCK) ON r.ramo_id    = a.ramo_id    LEFT JOIN" & vbNewLine
         sqlValida = sqlValida & "       seguros_db.dbo.tp_ramo_tb AS t WITH (NOLOCK) ON r.tp_ramo_id = t.tp_ramo_id" & vbNewLine
         sqlValida = sqlValida & " WHERE a.apolice_id = " & Trim(txtApolice) & vbNewLine
         sqlValida = sqlValida & "   AND a.ramo_id = " & Trim(txtRamo) & vbNewLine
      Else
         sqlValida = "            SELECT ramo = rtrim(r.nome) + ' ( ' + ltrim(t.nome)+' )', controle = 1" & vbNewLine
         sqlValida = sqlValida & "  FROM seguros_db.dbo.ramo_tb    AS r WITH (NOLOCK) INNER JOIN" & vbNewLine
         sqlValida = sqlValida & "       seguros_db.dbo.tp_ramo_tb AS t WITH (NOLOCK) ON r.tp_ramo_id = t.tp_ramo_id" & vbNewLine
         sqlValida = sqlValida & " WHERE r.ramo_id = " & Trim(txtRamo) & vbNewLine
      End If
   
      Set rdoValida = rdocn.OpenResultset(sqlValida)
     
      If rdoValida.EOF Or rdoValida!Controle = 0 Then
         Cancel = True
      ElseIf rdoValida!Controle = 1 Then
         stbSelecao.SimpleText = rdoValida!ramo
      End If
      rdoValida.Close
   End If
End If
If Cancel Then
   If txtRamo.SelLength = Len(txtRamo) Then
      txtRamo = ""
   Else
      SelectAllText txtRamo
   End If
   stbSelecao.SimpleText = "Ramo inv�lido ou n�o cadastrado" & IIf(Trim(txtApolice) <> "", " nesta Ap�lice", "") & "!"
Else
   cmdOk.SetFocus
End If
End Sub

