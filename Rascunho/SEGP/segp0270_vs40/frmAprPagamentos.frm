VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form frmAprPagamentos 
   Caption         =   "SEG07006 - Aprova��o de Pagamentos - "
   ClientHeight    =   6405
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9195
   LinkTopic       =   "Form1"
   ScaleHeight     =   6405
   ScaleWidth      =   9195
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraopt 
      Caption         =   "Exibir"
      Height          =   615
      Left            =   5880
      TabIndex        =   7
      Top             =   5040
      Width           =   3135
      Begin VB.OptionButton optRetornados 
         Caption         =   "Com retorno SAP"
         Height          =   255
         Left            =   1200
         TabIndex        =   9
         Top             =   270
         Width           =   1575
      End
      Begin VB.OptionButton optTodos 
         Caption         =   "Todos"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   270
         Value           =   -1  'True
         Width           =   975
      End
   End
   Begin Crystal.CrystalReport Report1 
      Left            =   3240
      Top             =   5400
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin Crystal.CrystalReport AP 
      Left            =   2520
      Top             =   5520
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.CheckBox chkSintetico 
      Caption         =   "Sint�tico"
      Height          =   195
      Left            =   4080
      TabIndex        =   6
      Top             =   5320
      Width           =   1695
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   5
      Top             =   6150
      Width           =   9195
      _ExtentX        =   16219
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdImpressao 
      Caption         =   "Imprime AP"
      Height          =   440
      Left            =   4020
      TabIndex        =   4
      Top             =   5700
      Width           =   1530
   End
   Begin VB.CommandButton cmdMarcarTodos 
      Caption         =   "&Marcar Todos"
      Height          =   440
      Left            =   495
      TabIndex        =   3
      Top             =   5685
      Width           =   1530
   End
   Begin VB.CommandButton cmdAprovar 
      Caption         =   "&Aprovar"
      Height          =   440
      Left            =   5760
      TabIndex        =   0
      Top             =   5685
      Width           =   1530
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   440
      Left            =   7560
      TabIndex        =   1
      Top             =   5685
      Width           =   1530
   End
   Begin MSFlexGridLib.MSFlexGrid gridPagamentos 
      Height          =   4950
      Left            =   75
      TabIndex        =   2
      Top             =   105
      Width           =   9090
      _ExtentX        =   16034
      _ExtentY        =   8731
      _Version        =   393216
      Cols            =   12
      FixedCols       =   0
      HighLight       =   0
   End
End
Attribute VB_Name = "frmAprPagamentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Dt_Sistema As Date
Dim AprPag As Integer
Dim proposta_id As String
Dim sinistro_id As String
Dim detalhamento As String
Dim arqpath      As String
Dim PropostaBB As String
Dim vRamo As String

Dim tp_pessoa       As String
Dim nome            As String
Dim cpf_cgc         As String
Dim Endereco        As String
Dim bairro          As String
Dim Cep             As String
Dim banco           As String
Dim agencia         As String
Dim conta_corrente  As String
Dim ddd             As String
Dim telefone        As String
Dim ddd_fax_1       As String
Dim ddd_fax_2       As String
Dim telefone_fax_1  As String
Dim telefone_fax_2  As String
Dim e_mail          As String
Dim HostName        As String
Dim corretor_id     As String
Dim sucursal        As String
Dim vColuna         As Long

Private Sub Obtem_PropostaBB()
On Error GoTo erro

Dim rc    As rdoResultset
Dim rc1    As rdoResultset
Dim SQL   As String

SQL = " SELECT proposta_bb "
SQL = SQL & " FROM Proposta_Fechada_tb  WITH (NOLOCK)  "
SQL = SQL & " WHERE Proposta_id = " & proposta_id

Set rc = rdocn.OpenResultset(SQL)

If rc.EOF Then
    rc.Close
    SQL = " SELECT a.proposta_BB, a.ramo_id, b.nome "
    SQL = SQL & " FROM Proposta_Adesao_tb a  WITH (NOLOCK)  "
    SQL = SQL & "      inner    join ramo_tb b  WITH (NOLOCK)  "
    SQL = SQL & "               on  b.ramo_id = a.ramo_id"
    SQL = SQL & " WHERE a.Proposta_id = " & proposta_id
    Set rc = rdocn.OpenResultset(SQL)
    vRamo = rc("ramo_id") & " - " & rc("nome")
Else
    'implementado por fpimenta - 26/02/2003
    SQL = ""
    SQL = SQL & " select  a.ramo_id,"
    SQL = SQL & "     b.nome"
    SQL = SQL & " from    apolice_tb a  WITH (NOLOCK)  "
    SQL = SQL & "     inner   join ramo_tb b  WITH (NOLOCK)  "
    SQL = SQL & "         on  b.ramo_id = a.ramo_id"
    SQL = SQL & " Where a.proposta_id = " & proposta_id
    Set rc1 = rdocn.OpenResultset(SQL)
    vRamo = rc1("ramo_id") & " - " & rc1("nome")
End If


PropostaBB = IIf(IsNull(rc!proposta_bb), "", rc!proposta_bb)
rc.Close

Exit Sub

erro:
    TrataErroGeral "Obtem Proposta BB"
    MsgBox "Rotina: Obtem Proposta BB. O programa ser� abortado.", vbCritical, "Erro"
    End

End Sub

'PAULO PELEGRINI - 18958788 - AJUSTE NA GERA��O DO EXTRATO DE PAGAMENTO A CORRETORES - adi��o de novo parametro
Sub Monta_Grid_Pagamentos(pri_vez As Boolean, Optional bRetornoSAP As Boolean)

Dim SQL As String, rc As rdoResultset
Dim rc1 As rdoResultset
Dim linha As String, FAVORECIDO As String

On Error GoTo erro

'jose.souza - Confitec - Em 30/08/2013
bUsarConexaoABS = False

AprPag = 0

'mathayde
If bUsarConexaoABS And rdocn.name <> rdocn_Seg.name Then
    Set rdocn = rdocn_Seg
End If

'--[ Demanda 18721099 ]---- Cl�udia Eduarda - Confitec 28/05/2015 -------------------------------------
StatusBar1.SimpleText = "Aguarde! Carregando o(s) pagamento(s) selecinado(s)."
Screen.MousePointer = vbHourglass

SQL = Monta_Query_Pagamento(bRetornoSAP)
   
Set rc = rdocn.OpenResultset(SQL)

If rc.EOF Then
    If pri_vez Then MsgBox "Nenhum registro foi encontrado !"
    gridPagamentos.Rows = 1
    rc.Close
    If UCase(TpAprovacao) <> "CORRETAGEM N�O-RETIDA" Then 'PAULO PELEGRINI - 18958788 - AJUSTE NA GERA��O DO EXTRATO DE PAGAMENTO A CORRETORES
        Unload Me
    Else
        Screen.MousePointer = vbDefault
    End If
    Exit Sub
End If
gridPagamentos.Rows = 1
gridPagamentos.Redraw = False

While Not rc.EOF
    'Preenche_grid
    Select Case rc!tp_favorecido
    Case "agen": FAVORECIDO = "Agenciador"
    Case "clie": FAVORECIDO = "Cliente"
    Case "corr": FAVORECIDO = "Corretor"
    Case "coss": FAVORECIDO = "Cosseguro"
    Case "esti": FAVORECIDO = "Pro Labore"
    Case "item": FAVORECIDO = "Item Financeiro"
    Case "ress": FAVORECIDO = "Resseguro"
    Case "cart": FAVORECIDO = "Banco"   'mathayde 24/07/2009 demanda : 654224 / Procedimento para aprova��o da comiss�o retida do cart�o de cr�dito
    Case "assi": FAVORECIDO = "Prestadora de Assist�ncia" 'Gabriel C�mara - 17082374 - IPrem - Custo Assist�ncia
    Case "sini":
       If rc!item_val_estimativa = 2 Then
          FAVORECIDO = "Sinistro Honor�rios "
       ElseIf rc!item_val_estimativa = 1 Then
          FAVORECIDO = "Sinistro Indeniza��o"
       Else
          FAVORECIDO = "Sinistro Despesas"
       End If
    End Select
       
    If rc!tp_favorecido = "cart" Then 'mathayde 24/07/2009 demanda : 654224
        linha = "" & vbTab & Format(rc!dt_repasse, "dd/mm/yyyy") & vbTab
    Else
        linha = "" & vbTab & Format(rc!dt_geracao, "dd/mm/yyyy") & vbTab
    End If
    
    linha = linha & rc!acerto_id & vbTab
        
    linha = linha & FAVORECIDO & vbTab
    linha = linha & IIf(IsNull(rc!nome) = True, " ", rc!nome) & vbTab
    
    If ConfiguracaoBrasil Then
        linha = linha & Format(Val(rc!val_total_acerto), "###,###,###,##0.00") & vbTab
    Else
        linha = linha & TrocaValorAmePorBras(Format(Val(rc!val_total_acerto), "###,###,###,##0.00")) & vbTab
    End If
               
    linha = linha & UCase(rc!retido_banco) & vbTab
    linha = linha & rc!situacao & vbTab
    linha = linha & Format(rc!dt_acerto, "dd/mm/yyyy") & vbTab
    linha = linha & rc!voucher_id & vbTab
    linha = linha & Format(rc!dt_geracao, "yyyymmdd") & vbTab
    linha = linha & Format(rc!dt_acerto, "yyyymmdd") & vbTab

    If UCase(TpAprovacao) = "COMISS�O RETIDA" Or UCase(TpAprovacao) = "PR�-LABORE RETIDO" Then
        linha = linha & rc!ramo_nome & vbTab
        linha = linha & rc!produto_nome & vbTab
    End If
    
    ''mathayde 05/08/2009 demanda : 654224
    'Informa o arquivo do acerto
    If UCase(TpAprovacao) = "COMISS�O RETIDA CART�O DE CR�DITO" Then
        linha = linha & rc!arquivo_envio_bb & vbTab
    End If
    
    'MATHAYDE
'    If bUsarConexaoABS Then
'        linha = linha & rc!empresa & vbTab
'    Else
        linha = linha & glAmbiente_id & vbTab
'    End If
    
    gridPagamentos.AddItem linha
    gridPagamentos.Col = 0
    rc.MoveNext
Wend
    
gridPagamentos.ColSel = 2
gridPagamentos.Redraw = True
gridPagamentos.ColAlignment(0) = 4

'mathayde
If gridPagamentos.TextMatrix(0, iColunaEmpresa) = "empresa" Then
    gridPagamentos.ColWidth(iColunaEmpresa) = 0
End If
    
'gridPagamentos.Sort = 1
StatusBar1.SimpleText = "Selecione o(s) pagamento(s) a ser(em) aprovado(s)."
rc.Close

'--[ Demanda 18721099 ]---- Cl�udia Eduarda - Confitec 28/05/2015 -------------------------------------
Screen.MousePointer = vbDefault
Me.SetFocus

Exit Sub
    
Resume
    
erro:
    TrataErroGeral "Aprova��o de Pagamentos"
    MsgBox "Rotina: Aprova��o de Pagamentos. O programa ser� abortado.", vbCritical, "Erro"
    End

End Sub

Private Sub cmdAprovar_Click()

Dim rc1 As rdoResultset, rs As rdoResultset, SQL As String, i As Long

'jose.souza - Confitec - Em 30/08/2013
bUsarConexaoABS = False

If AprPag <= 0 Then
    MsgBox "Nenhum pagamento foi selecionado.", vbCritical
    Exit Sub
End If

On Error GoTo erro

SQL = "SELECT status_sistema FROM parametro_geral_tb  WITH (NOLOCK)  WHERE status_sistema='L'"
Set rc1 = rdoCn1.OpenResultset(SQL)
If rc1.EOF Then
    MsgBox "Sistema n�o dispon�vel para esta opera��o.", vbInformation
    Exit Sub
End If
rc1.Close

For i = 1 To gridPagamentos.Rows - 1
    If gridPagamentos.TextMatrix(i, 0) <> "" Then
    
        'mathayde
        If bUsarConexaoABS Then
            If gridPagamentos.TextMatrix(i, iColunaEmpresa) = glAmbiente_id_seg2 Then
                Set rdocn = rdocn_Seg2
            Else
                Set rdocn = rdocn_Seg
            End If
        End If
        
        rdocn.BeginTrans
        
        SQL = "exec ps_acerto_aprovacao_spu "
        SQL = SQL & Val(gridPagamentos.TextMatrix(i, 2))
        SQL = SQL & ", 'a', '" & Format(Date, "yyyymmdd") & "'"
        SQL = SQL & ", '" & cUserName & "'"
        

        
        Set rs = rdocn.OpenResultset(SQL)
        rs.Close
        
        SQL = "exec SEGS12289_SPS "
        SQL = SQL & Val(gridPagamentos.TextMatrix(i, 2))
        SQL = SQL & ", '" & App.Title & "'"
        SQL = SQL & ", '" & cUserName & "'"
        
        Set rs = rdocn.OpenResultset(SQL)
        rs.Close

        
        rdocn.CommitTrans
    End If
Next

MsgBox "Aprova��es Efetuadas"

'PAULO PELEGRINI - 18958788 - AJUSTE NA GERA��O DO EXTRATO DE PAGAMENTO A CORRETORES - adi��o de novo par�metro
Monta_Grid_Pagamentos False, False

Exit Sub

erro:
    TrataErroGeral "Aprova��o de Pagamentos"
    MsgBox "Rotina: Aprova��o de Pagamentos. O programa ser� abortado.", vbCritical, "Erro"
    End
End Sub

Private Sub Imprime_Aprovacao()

Dim i    As Integer

On Error GoTo erro

'jose.souza - Confitec - Em 30/08/2013
bUsarConexaoABS = False

If AprPag <= 0 Then
    MsgBox "Nenhum pagamento foi selecionado.", vbCritical
    Exit Sub
End If

'AP.ReportFileName = "c:\autorizacaosinistro.rpt"
AP.ReportFileName = arqpath & "aprovacaopgto.rpt"

For i = 1 To gridPagamentos.Rows - 1
    If gridPagamentos.TextMatrix(i, 0) <> "" Then
    
        AP.Formulas(0) = "acerto='" & gridPagamentos.TextMatrix(i, 2) & "'"
       
        Select Case UCase(gridPagamentos.TextMatrix(i, 3))
        Case "AGENCIADOR"
            AP.Formulas(1) = "Observacao='PAGAMENTO DE AGENCIADOR'"
            detalhamento = "CR�DITO DE COMISS�O DE AGENCIAMENTO"
         
        Case "CLIENTE"
            AP.Formulas(1) = "Observacao='RESTITUI��O DE PR�MIO'"
            
            Obtem_Proposta (gridPagamentos.TextMatrix(i, 2))
            detalhamento = "REF. A PROPOSTA AB " & proposta_id
            Obtem_PropostaBB
        Case "CORRETOR"
            AP.Formulas(1) = "Observacao='PAGAMENTO DE CORRETOR'"
            detalhamento = "CR�DITO DE COMISS�ES "
        
        Case "PRO LABORE"
            AP.Formulas(1) = "Observacao='PAGAMENTO DE PRO LABORE'"
            detalhamento = "CR�DITO DE COMISS�ES "
         
        Case "ITEM FINANCEIRO"
            AP.Formulas(1) = "Observacao='IOF A RECOLHER'"
            detalhamento = "IOF A RECOLHER N�O RETIDO "
         
        Case "SINISTROS HONOR�RIOS"
            AP.Formulas(1) = "Observacao='PAGAMENTO DE SINISTRO - HONOR�RIOS'"
            Obtem_Sinistro (gridPagamentos.TextMatrix(i, 2))
            detalhamento = "REF. A SINISTRO " & sinistro_id
         
        Case "SINISTROS DESPESAS"
            AP.Formulas(1) = "Observacao='PAGAMENTO DE SINISTRO - DESPESAS'"
            Obtem_Sinistro (gridPagamentos.TextMatrix(i, 2))
            detalhamento = "REF. A SINISTRO " & sinistro_id
        
        Case "SINISTRO INDENIZA��O"
            AP.Formulas(1) = "Observacao='PAGAMENTO DE SINISTRO - INDENIZA��O'"
            Obtem_Sinistro (gridPagamentos.TextMatrix(i, 2))
            detalhamento = "REF. A SINISTRO " & sinistro_id
            
        End Select
       
        'mathayde
        If bUsarConexaoABS Then
            If gridPagamentos.TextMatrix(i, iColunaEmpresa) = glAmbiente_id_seg2 Then
                Set rdocn = rdocn_Seg2
            Else
                Set rdocn = rdocn_Seg
            End If
        End If
        
        'carolina.marinho 01/11/2011 - Demanda 12649046 - Relat�rio IOF - Implementa��o do Nosso N�mero
        AP.Connect = rdocn.Connect
        AP.StoredProcParam(0) = gridPagamentos.TextMatrix(i, 2)
        
        AP.Formulas(2) = "Dt_pagamento='" & gridPagamentos.TextMatrix(i, 8) & "'"
        AP.Formulas(3) = "Beneficiario='" & gridPagamentos.TextMatrix(i, 4) & "'"
        AP.Formulas(4) = "Forma_pgto=''"
        AP.Formulas(5) = "moeda='R$'"
        AP.Formulas(6) = "cambio=''"
        AP.Formulas(7) = "Dt_emissao='" & gridPagamentos.TextMatrix(i, 1) & "'"
        AP.Formulas(8) = "Valor='" & gridPagamentos.TextMatrix(i, 5) & "'"
        AP.Formulas(9) = "Detalhamento='" & detalhamento & "'"
        AP.Formulas(10) = "Proposta_BB='" & PropostaBB & "'"
        AP.Formulas(11) = "ramo='" & vRamo & "'"
        AP.Destination = crptToPrinter
        AP.Action = 1
    End If
Next i

Exit Sub

erro:
    TrataErroGeral "Imprime Aprova��o"
    MsgBox "Rotina: Imprime Aprova��o. O programa ser� abortado.", vbCritical, "Erro"
    End


End Sub

Private Sub Imprime_Extrato()

Dim i    As Integer

On Error GoTo erro

'jose.souza - Confitec - Em 30/08/2013
bUsarConexaoABS = False

If AprPag <= 0 Then
    MsgBox "Nenhum pagamento foi selecionado.", vbCritical
    Exit Sub
End If

For i = 1 To gridPagamentos.Rows - 1
    If gridPagamentos.TextMatrix(i, 0) <> "" And UCase(gridPagamentos.TextMatrix(i, 6)) <> "S" Then
       
       If UCase(gridPagamentos.TextMatrix(i, 3)) = "CORRETOR" Then
          Obtem_Corretor gridPagamentos.TextMatrix(i, 2)
       Else
          Obtem_Estipulante gridPagamentos.TextMatrix(i, 2)
       End If
       
       'mathayde
       If bUsarConexaoABS Then
            If gridPagamentos.TextMatrix(i, iColunaEmpresa) = glAmbiente_id_seg2 Then
                Set rdocn = rdocn_Seg2
            Else
                Set rdocn = rdocn_Seg
            End If
       End If
        
       cod_origem = "CC"
       voucher_id = "1"
       dt_voucher = Format(gridPagamentos.TextMatrix(i, 8), "yyyymmdd")
       acerto = gridPagamentos.TextMatrix(i, 2)
       periodo_ini = "01/07/2000"
       periodo_fim = "31/07/2000"
       situacao = "p"
       
       If UCase(gridPagamentos.TextMatrix(i, 3)) = "CORRETOR" Then
          rdocn.Execute "exec rpt_ext_pgto_corretor_spi " & corretor_id & ", '" & sucursal & "', '" & tp_pessoa & "', '" & Format(periodo_ini, "yyyymmdd") & "', '" & Format(periodo_fim, "yyyymmdd") & "', '" & _
                        cod_origem & "'," & voucher_id & ",'" & dt_voucher & "'," & _
                        acerto & ",'" & cUserName & "','" & situacao & "'"
       Else
          rdocn.Execute "exec rpt_ext_pgto_pro_labore_spi " & corretor_id & ", '" & Format(periodo_ini, "yyyymmdd") & "', '" & Format(periodo_fim, "yyyymmdd") & "', '" & _
                        cod_origem & "'," & voucher_id & ",'" & dt_voucher & "'," & _
                        acerto & ",'" & cUserName & "','" & situacao & "'"
       End If

      'Alessandra Grig�rio - 11/08/2004 - Novo padr�o de nomenclatura
      'Report1.ReportFileName = arqpath & "ExtratoPgtoCorretor.rpt"
      If chkSintetico.Value = 0 Then
      Report1.ReportFileName = arqpath & "SEGR0270-02.rpt"
      Else
      Report1.ReportFileName = arqpath & "\SEGR0270-03.rpt"
      End If
      
      Report1.SelectionFormula = "{rpt_ext_pgto_corretor_tb.corretor_id} = " & corretor_id & " and {rpt_ext_pgto_corretor_tb.sucursal_corretor_id} = '" & sucursal & "' and {rpt_ext_pgto_corretor_tb.usuario}='" & cUserName & "'"
      Report1.Formulas(0) = "sucursal = '" & sucursal & "'"
      Report1.Formulas(1) = "nome = '" & nome & "'"
      Report1.Formulas(2) = "tp_pessoa = '" & tp_pessoa & "'"
      Report1.Formulas(3) = "cpf_cgc = '" & cpf_cgc & "'"
      Report1.Formulas(4) = "endereco = '" & Endereco & "'"
      Report1.Formulas(5) = "bairro = '" & bairro & "'"
      Report1.Formulas(6) = "cep = '" & Cep & "'"
      Report1.Formulas(7) = "banco = '" & banco & "'"
      Report1.Formulas(8) = "agencia = '" & agencia & "'"
      Report1.Formulas(9) = "conta_corrente = '" & conta_corrente & "'"
      Report1.Formulas(10) = "ddd = '" & ddd & "'"
      Report1.Formulas(11) = "telefone = '" & telefone & "'"
      Report1.Formulas(12) = "ddd_fax_1 = '" & ddd_fax_1 & "'"
      Report1.Formulas(13) = "ddd_fax_2 = '" & ddd_fax_2 & "'"
      Report1.Formulas(14) = "telefone_fax_1 = '" & telefone_fax_1 & "'"
      Report1.Formulas(15) = "telefone_fax_2 = '" & telefone_fax_2 & "'"
      Report1.Formulas(16) = "e_mail = '" & e_mail & "'"
      Report1.Formulas(17) = "periodo_de = '" & periodo_ini & "'"
      Report1.Formulas(18) = "periodo_ate = '" & periodo_fim & "'"
      'Demanda 18675295 - Edilson silva - 06/02/2015
      If glAmbiente_id = 3 Or glAmbiente_id = 2 Then
          Report1.Formulas(19) = "empresa = 'Companhia de Seguros Alian�a do Brasil'"
      Else
          Report1.Formulas(19) = "empresa = 'Alian�a do Brasil Seguros S/A'"
      End If
      'Demanda 18675295 - Edilson silva - 06/02/2015
      Report1.Destination = crptToWindow
      Report1.Connect = rdocn.Connect
      Report1.Action = 1
    End If
Next


MousePointer = 0
        
Exit Sub

erro:
    TrataErroGeral "Imprime Extrato"
    MsgBox "Rotina: Imprime Extrato. O programa ser� abortado.", vbCritical, "Erro"
    End

End Sub



Private Sub cmdImpressao_Click()
If UCase(TpAprovacao) = "CORRETAGEM N�O-RETIDA" Or UCase(TpAprovacao) = "PRO-LABORE N�O-RETIDO" Then
   Imprime_Extrato
Else
   Imprime_Aprovacao
End If

End Sub

Private Sub cmdMarcarTodos_Click()
Dim linha As Integer

gridPagamentos.Redraw = False
For linha = 1 To gridPagamentos.Rows - 1
    gridPagamentos.Col = 0
    gridPagamentos.Row = linha
    gridPagamentos.CellFontName = "Monotype Sorts"
    gridPagamentos.Text = "4"
Next
gridPagamentos.Redraw = True
AprPag = gridPagamentos.Rows - 1
End Sub


Private Function Monta_Query_Pagamento(Optional bRetornoSAP As Boolean) As String

    Dim SQL As String

    Select Case UCase(TpAprovacao)

    Case "CORRETAGEM N�O-RETIDA"

        'PAULO PELEGRINI - 18958788 - AJUSTE NA GERA��O DO EXTRATO DE PAGAMENTO A CORRETORES (INI)


        If bRetornoSAP Then
            Dim intFlagAprovados As Integer

            If flagAprovados = True Then
                intFlagAprovados = 1
            End If

            SQL = "SELECT TOP 25000 a.*, rtrim(e.nome) + ' ' + rtrim(c.nome) nome " & vbNewLine
            SQL = SQL & ", tp_favorecido = 'corr', d.dt_geracao, d.val_total_acerto " & vbNewLine
            SQL = SQL & ", d.retido_banco, dt_acerto " & vbNewLine
            SQL = SQL & ", empresa = 5 " & vbNewLine
            SQL = SQL & "From " & vbNewLine
            SQL = SQL & "   seguros_db.dbo.ps_acerto_pagamento_tb a  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "inner join seguros_db.dbo.ps_acerto_corretor_pf_tb b  WITH (NOLOCK)  on a.acerto_id = b.acerto_id " & vbNewLine
            SQL = SQL & "inner join seguros_db.dbo.corretor_tb c  WITH (NOLOCK) on b.pf_corretor_id = c.corretor_id " & vbNewLine
            SQL = SQL & "inner join seguros_db.dbo.ps_acerto_tb d  WITH (NOLOCK) on d.acerto_id = a.acerto_id " & vbNewLine
            SQL = SQL & "inner join seguros_db.dbo.corretor_tb e  WITH (NOLOCK) on b.pf_corretor_id = e.corretor_id " & vbNewLine
            SQL = SQL & "inner join Interface_Dados_Db.dbo.Importacao_ERP_Atual_Tb f with (nolock) on (a.voucher_id = f.voucher_id and f.Origem_Voucher = 'CC') " & vbNewLine
            SQL = SQL & "inner join (select distinct num_liq from interface_dados_db.dbo.Retorno_Tributacao_ERP_Atual_Tb with (nolock)) g on f.num_liq = g.num_liq " & vbNewLine
            SQL = SQL & "Where " & vbNewLine
            SQL = SQL & "(((a.situacao in ('a','e') and " & intFlagAprovados & " = 1)) or ((a.situacao in ('p') and " & intFlagAprovados & " = 0 and d.retido_banco = 'n'))) " & vbNewLine
            SQL = SQL & "AND 0 = (SELECT COUNT(1) " & vbNewLine
            SQL = SQL & "FROM seguros_db.dbo.ps_mov_corretor_pf_tb ps_mov_corretor_pf_tb  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "JOIN seguros_db.dbo.ps_movimentacao_tb  ps_movimentacao_tb WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "  ON ps_movimentacao_tb.movimentacao_id = ps_mov_corretor_pf_tb.movimentacao_id " & vbNewLine
            SQL = SQL & "Where ps_mov_corretor_pf_tb.acerto_id = d.acerto_id " & vbNewLine
            SQL = SQL & "  AND ps_movimentacao_tb.situacao = 't') " & vbNewLine
            SQL = SQL & frmSelPagamentos.resFiltro
            SQL = SQL & "Union " & vbNewLine
            SQL = SQL & "SELECT TOP 25000 a.*, rtrim(e.nome) + ' ' + rtrim(c.nome) nome " & vbNewLine
            SQL = SQL & ", tp_favorecido = 'corr', d.dt_geracao,  b.val_acerto val_total_acerto " & vbNewLine
            SQL = SQL & ", d.retido_banco, dt_acerto " & vbNewLine
            SQL = SQL & ", empresa = 5 " & vbNewLine
            SQL = SQL & "From " & vbNewLine
            SQL = SQL & "seguros_db.dbo.ps_acerto_pagamento_tb a  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "INNER JOIN seguros_db.dbo.ps_acerto_corretor_pj_tb b  WITH (NOLOCK)  ON a.acerto_id = b.acerto_id " & vbNewLine
            SQL = SQL & "INNER JOIN seguros_db.dbo.sucursal_corretor_tb c  WITH (NOLOCK)  ON b.pj_corretor_id = c.pj_corretor_id AND b.sucursal_corretor_id = c.sucursal_corretor_id " & vbNewLine
            SQL = SQL & "INNER JOIN seguros_db.dbo.ps_acerto_tb d  WITH (NOLOCK)  ON d.acerto_id = a.acerto_id " & vbNewLine
            SQL = SQL & "INNER JOIN seguros_db.dbo.corretor_tb e  WITH (NOLOCK)  ON b.pj_corretor_id = e.corretor_id " & vbNewLine
            SQL = SQL & "inner join Interface_Dados_Db.dbo.Importacao_ERP_Atual_Tb f with (nolock) on (a.voucher_id = f.voucher_id and f.Origem_Voucher = 'CC') " & vbNewLine
            SQL = SQL & "inner join (select distinct num_liq from interface_dados_db.dbo.Retorno_Tributacao_ERP_Atual_Tb with (nolock)) g on f.num_liq = g.num_liq " & vbNewLine
            SQL = SQL & "Where " & vbNewLine
            SQL = SQL & "  ((a.situacao in ('a','e') and " & intFlagAprovados & " = 1) or (a.situacao in ('p') and " & intFlagAprovados & " = 0 and d.retido_banco = 'n')) " & vbNewLine
            SQL = SQL & "AND 0 = (SELECT COUNT(1) " & vbNewLine
            SQL = SQL & "           FROM seguros_db.dbo.ps_mov_corretor_pj_tb ps_mov_corretor_pj_tb  WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "           JOIN seguros_db.dbo.ps_movimentacao_tb  ps_movimentacao_tb WITH (NOLOCK) " & vbNewLine
            SQL = SQL & "             ON ps_movimentacao_tb.movimentacao_id = ps_mov_corretor_pj_tb.movimentacao_id " & vbNewLine
            SQL = SQL & "          Where ps_mov_corretor_pj_tb.acerto_id = d.acerto_id " & vbNewLine
            SQL = SQL & "            AND ps_movimentacao_tb.situacao = 't') " & vbNewLine
            SQL = SQL & frmSelPagamentos.resFiltro
            'PAULO PELEGRINI - 18958788 - AJUSTE NA GERA��O DO EXTRATO DE PAGAMENTO A CORRETORES (FIM)
        Else

            '* CORRETOR PF
            ' In�cio Altera��o - Tales de S� - INC000004585314
            ' Tratamento para limitar os registros no grid at� que a demanda evolutiva seja realizada para incluir filtros
            'SQL = " SELECT a.*, rtrim(e.nome) + ' ' + rtrim(c.nome) nome"
            SQL = " SELECT TOP 25000 a.*, rtrim(e.nome) + ' ' + rtrim(c.nome) nome" & vbNewLine
            ' Fim Altera��o - INC000004585314
            SQL = SQL & ", tp_favorecido = 'corr', d.dt_geracao, d.val_total_acerto" & vbNewLine
            SQL = SQL & ", d.retido_banco, dt_acerto" & vbNewLine
            SQL = SQL & ", empresa = " & glAmbiente_id & vbNewLine
            SQL = SQL & " FROM ps_acerto_pagamento_tb a  WITH (NOLOCK)  , ps_acerto_corretor_pf_tb b  WITH (NOLOCK)  , corretor_tb c  WITH (NOLOCK)  , " & vbNewLine
            SQL = SQL & "      ps_acerto_tb d  WITH (NOLOCK)  , corretor_tb e  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & " WHERE a.acerto_id = b.acerto_id " & vbNewLine
            SQL = SQL & "   AND b.pf_corretor_id = c.corretor_id" & vbNewLine
            SQL = SQL & "   AND b.pf_corretor_id = e.corretor_id" & vbNewLine
            SQL = SQL & "   AND d.retido_banco = 'n' " & vbNewLine 'altera��o glauber.chaves@confitec.com.br 04/12/2018 - IM00574288
            If flagAprovados Then
                SQL = SQL & "   AND a.situacao in ('a','e')" & vbNewLine
            Else
                SQL = SQL & "   AND a.situacao='p'" & vbNewLine
                'SQL = SQL & "   AND d.retido_banco = 'n'" & vbNewLine
                                'altera��o glauber.chaves@confitec.com.br 04/12/2018 - IM00574288
            End If

            SQL = SQL & "   AND d.acerto_id = a.acerto_id" & vbNewLine

            '--[ Demanda 18721099 ]---- Cl�udia Eduarda - Confitec 28/05/2015 -------------------------------------
            SQL = SQL & frmSelPagamentos.resFiltro

            'FLOW 5392271 - Projeto AB - ABS - Marcelo Ferreira - Confitec Sistemas - 2010-09-15 - Ref. FLOW 5383834
            'Isolando os acertos transferidos para o outro banco de dados
            SQL = SQL & "   AND 0 = (SELECT COUNT(1) " & vbNewLine
            SQL = SQL & "              FROM ps_mov_corretor_pf_tb   WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "              JOIN ps_movimentacao_tb   WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "                ON ps_movimentacao_tb.movimentacao_id = ps_mov_corretor_pf_tb.movimentacao_id " & vbNewLine
            SQL = SQL & "             WHERE ps_mov_corretor_pf_tb.acerto_id = d.acerto_id " & vbNewLine
            SQL = SQL & "               AND ps_movimentacao_tb.situacao = 't') " & vbNewLine

            '    '* Inclu�do por Junior em 08/10/2003 - Para n�o exibir valores de restitui��o conforme solicitado pela �rea
            '    SQL = SQL & "   AND   NOT EXISTS (SELECT *"
            '    SQL = SQL & "                     FROM ps_mov_corretor_pf_tb PM"
            '    SQL = SQL & "                        JOIN ps_mov_endosso_financeiro_tb PEF"
            '    SQL = SQL & "                           ON PEF.movimentacao_id = PM.movimentacao_id"
            '    SQL = SQL & "                        JOIN endosso_financeiro_tb EF"
            '    SQL = SQL & "                           ON EF.proposta_id = PEF.proposta_id"
            '    SQL = SQL & "                          AND EF.endosso_id  = PEF.endosso_id"
            '    SQL = SQL & "                     WHERE PM.acerto_id = b.acerto_id"
            '    SQL = SQL & "                       AND val_financeiro < 0 )"
            '    '* FIM

            '* CORRETOR PJ
            SQL = SQL & " UNION" & vbNewLine

            ' In�cio Altera��o - Tales de S� - INC000004585314
            ' Tratamento para limitar os registros no grid at� que a demanda evolutiva seja realizada para incluir filtros
            'SQL = SQL & " SELECT a.*, rtrim(e.nome) + ' ' + rtrim(c.nome) nome"
            SQL = SQL & " SELECT TOP 25000 a.*, rtrim(e.nome) + ' ' + rtrim(c.nome) nome" & vbNewLine
            ' Fim Altera��o - INC000004585314
            SQL = SQL & ", tp_favorecido = 'corr', d.dt_geracao,  b.val_acerto val_total_acerto" & vbNewLine
            SQL = SQL & ", d.retido_banco, dt_acerto " & vbNewLine            'val_pagamento
            SQL = SQL & ", empresa = " & glAmbiente_id & vbNewLine
            SQL = SQL & " FROM ps_acerto_pagamento_tb a  WITH (NOLOCK)  , ps_acerto_corretor_pj_tb b  WITH (NOLOCK)  , sucursal_corretor_tb c  WITH (NOLOCK)  , " & vbNewLine
            SQL = SQL & "      ps_acerto_tb d  WITH (NOLOCK)  , corretor_tb e  WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & " WHERE a.acerto_id = b.acerto_id " & vbNewLine
            SQL = SQL & "   AND b.pj_corretor_id = c.pj_corretor_id " & vbNewLine
            SQL = SQL & "   AND b.pj_corretor_id = e.corretor_id " & vbNewLine
            SQL = SQL & "   AND b.sucursal_corretor_id = c.sucursal_corretor_id" & vbNewLine
            SQL = SQL & "   AND d.retido_banco = 'n' " & vbNewLine 'altera��o glauber.chaves@confitec.com.br 04/12/2018 - IM00574288
            If flagAprovados Then
                SQL = SQL & "   AND a.situacao in ('a','e')" & vbNewLine
            Else
                SQL = SQL & "   AND a.situacao='p'" & vbNewLine
                'SQL = SQL & "   AND d.retido_banco = 'n' " & vbNewLine
                'Retirando a condi��o de dentro o IF. Essa condi��o deve ser fixa - glauber.chaves@confitec.com.br 04/12/2018 - IM00574288
            End If

            SQL = SQL & "   AND d.acerto_id = a.acerto_id" & vbNewLine

            '--[ Demanda 18721099 ]---- Cl�udia Eduarda - Confitec 28/05/2015 -------------------------------------
            SQL = SQL & frmSelPagamentos.resFiltro

            'FLOW 5392271 - Projeto AB - ABS - Marcelo Ferreira - Confitec Sistemas - 2010-09-15 - Ref. FLOW 5383834
            'Isolando os acertos transferidos para o outro banco de dados
            SQL = SQL & "   AND 0 = (SELECT COUNT(1) " & vbNewLine
            SQL = SQL & "              FROM ps_mov_corretor_pj_tb   WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "              JOIN ps_movimentacao_tb   WITH (NOLOCK)  " & vbNewLine
            SQL = SQL & "                ON ps_movimentacao_tb.movimentacao_id = ps_mov_corretor_pj_tb.movimentacao_id " & vbNewLine
            SQL = SQL & "             WHERE ps_mov_corretor_pj_tb.acerto_id = d.acerto_id " & vbNewLine
            SQL = SQL & "               AND ps_movimentacao_tb.situacao = 't') " & vbNewLine

        End If



    Case "PRO-LABORE N�O-RETIDO"

        ' In�cio Altera��o - Tales de S� - INC000004585314
        ' Tratamento para limitar os registros no grid at� que a demanda evolutiva seja realizada para incluir filtros
        'SQL = "SELECT a.*, c.nome, tp_favorecido = 'esti', d.dt_geracao,  d.val_total_acerto, d.retido_banco, dt_acerto "
        SQL = "SELECT TOP 25000 a.*, c.nome, tp_favorecido = 'esti', d.dt_geracao,  d.val_total_acerto, d.retido_banco, dt_acerto " & vbNewLine
        ' Fim Altera��o - INC000004585314
        SQL = SQL & ", empresa = " & glAmbiente_id & vbNewLine
        SQL = SQL & " FROM ps_acerto_pagamento_tb a  WITH (NOLOCK)  , ps_acerto_estip_tb b  WITH (NOLOCK)  , cliente_tb c  WITH (NOLOCK)  , " & vbNewLine
        SQL = SQL & "      ps_acerto_tb d  WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & " WHERE a.acerto_id = b.acerto_id " & vbNewLine
        SQL = SQL & "   AND b.cliente_id = c.cliente_id" & vbNewLine
        SQL = SQL & "   AND d.retido_banco = 'n'" & vbNewLine 'altera��o glauber.chaves@confitec.com.br 04/12/2018 - IM00574288
        If flagAprovados Then
            SQL = SQL & "   AND a.situacao in ('a','e')" & vbNewLine
        Else
            SQL = SQL & "   AND a.situacao='p'" & vbNewLine
            'SQL = SQL & "   AND d.retido_banco = 'n'" & vbNewLine
            'Retirando a condi��o de dentro o IF. Essa condi��o deve ser fixa - glauber.chaves@confitec.com.br 04/12/2018 - IM00574288
        End If
        SQL = SQL & "   AND d.acerto_id = a.acerto_id"

        '--[ Demanda 18721099 ]---- Cl�udia Eduarda - Confitec 28/05/2015 -------------------------------------
        SQL = SQL & frmSelPagamentos.resFiltro


        '-- (IN�CIO) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        'MATHAYDE
        '    If bUsarConexaoABS Then
        '        SQL = SQL & " UNION ALL" & vbNewLine
        '        SQL = SQL & " SELECT a.*, c.nome, tp_favorecido = 'esti', d.dt_geracao,  d.val_total_acerto, d.retido_banco, dt_acerto " & vbNewLine
        '        SQL = SQL & ", empresa = " & glAmbiente_id_seg2 & vbNewLine
        '        SQL = SQL & " FROM ABSS.seguros_db.dbo.ps_acerto_pagamento_tb a, ABSS.seguros_db.dbo.ps_acerto_estip_tb b, ABSS.seguros_db.dbo.cliente_tb c, " & vbNewLine
        '        SQL = SQL & "      ABSS.seguros_db.dbo.ps_acerto_tb d" & vbNewLine
        '        SQL = SQL & " WHERE a.acerto_id = b.acerto_id " & vbNewLine
        '        SQL = SQL & "   AND b.cliente_id = c.cliente_id" & vbNewLine
        '        If flagAprovados Then
        '           SQL = SQL & "   AND a.situacao in ('a','e')" & vbNewLine
        '        Else
        '           SQL = SQL & "   AND a.situacao='p'" & vbNewLine
        '           SQL = SQL & "   AND d.retido_banco = 'n'" & vbNewLine
        '        End If
        '        SQL = SQL & "   AND d.acerto_id = a.acerto_id" & vbNewLine
        '
        '    End If

        SQL = SQL & " ORDER BY nome " & vbNewLine
        '-- (FIM) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'


    Case "AGENCIAMENTO"

        ' In�cio Altera��o - Tales de S� - INC000004585314
        ' Tratamento para limitar os registros no grid at� que a demanda evolutiva seja realizada para incluir filtros
        'SQL = " SELECT a.*, c.nome, tp_favorecido = 'agen', d.dt_geracao"
        SQL = " SELECT TOP 25000 a.*, c.nome, tp_favorecido = 'agen', d.dt_geracao"
        ' Fim Altera��o - INC000004585314
        SQL = SQL & ", d.val_total_acerto, d.retido_banco, dt_acerto "
        SQL = SQL & ", empresa = " & glAmbiente_id & vbNewLine
        SQL = SQL & " FROM ps_acerto_pagamento_tb a  WITH (NOLOCK)  , ps_acerto_agenciador_tb b  WITH (NOLOCK)  "
        SQL = SQL & ", agenciador_tb c  WITH (NOLOCK)  , ps_acerto_tb d  WITH (NOLOCK)  "
        SQL = SQL & " WHERE a.acerto_id = b.acerto_id "
        SQL = SQL & " AND b.agenciador_id = c.agenciador_id"
        If flagAprovados Then
            SQL = SQL & "   AND a.situacao in ('a', 'e')"
        Else
            SQL = SQL & "   AND a.situacao = 'p'"
        End If
        SQL = SQL & " AND d.acerto_id = a.acerto_id"

        '-- (IN�CIO) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        'MATHAYDE
        '    If bUsarConexaoABS Then
        '        SQL = SQL & " UNION ALL" & vbNewLine
        '        SQL = SQL & " SELECT a.*, c.nome, tp_favorecido = 'agen', d.dt_geracao" & vbNewLine
        '        SQL = SQL & ", d.val_total_acerto, d.retido_banco, dt_acerto " & vbNewLine
        '        SQL = SQL & ", empresa = " & glAmbiente_id_seg2 & vbNewLine
        '        SQL = SQL & " FROM ABSS.seguros_db.dbo.ps_acerto_pagamento_tb a, ABSS.seguros_db.dbo.ps_acerto_agenciador_tb b" & vbNewLine
        '        SQL = SQL & ", ABSS.seguros_db.dbo.agenciador_tb c, ABSS.seguros_db.dbo.ps_acerto_tb d" & vbNewLine
        '        SQL = SQL & " WHERE a.acerto_id = b.acerto_id " & vbNewLine
        '        SQL = SQL & " AND b.agenciador_id = c.agenciador_id" & vbNewLine
        '        If flagAprovados Then
        '           SQL = SQL & "   AND a.situacao in ('a', 'e')" & vbNewLine
        '        Else
        '           SQL = SQL & "   AND a.situacao = 'p'" & vbNewLine
        '        End If
        '        SQL = SQL & " AND d.acerto_id = a.acerto_id" & vbNewLine
        '
        '    End If
        '-- (FIM) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'

        SQL = SQL & " ORDER BY nome" & vbNewLine

    Case "COSSEGURO"

        SQL = SQL & " FROM ps_acerto_pagamento_tb a  WITH (NOLOCK)  , ps_acerto_cos_tb b  WITH (NOLOCK)  , seguradora_tb c  WITH (NOLOCK)  , "
        SQL = SQL & "      ps_acerto_tb d  WITH (NOLOCK)  "
        SQL = SQL & " WHERE a.acerto_id = b.acerto_id "
        SQL = SQL & "   AND b.seguradora_cod_susep = c.seguradora_cod_susep"
        SQL = SQL & "   AND a.situacao='p'"
        SQL = SQL & "   AND d.acerto_id = a.acerto_id" & vbNewLine
        SQL = SQL & " ORDER BY dt_geracao" & vbNewLine

        'MATHAYDE ?????


    Case "RESTITUI��O"

        'FLOW 3240956 - Marcelo Ferreira - Confitec Sistemas - 2010-04-26
        'Altera��o da estrutura abaixo para ser utilizada em tempor�ria.
        '-------------------------------------------------------------------------------
        '    SQL = " SELECT top 25000 a.*, c.nome, tp_favorecido = 'clie', d.dt_geracao"
        '    SQL = SQL & ",  d.val_total_acerto, d.retido_banco, dt_acerto "
        '    SQL = SQL & ", empresa = " & glAmbiente_id & vbNewLine
        '    SQL = SQL & " FROM ps_acerto_pagamento_tb a, ps_acerto_cliente_tb b, cliente_tb c, "
        '    SQL = SQL & "      ps_acerto_tb d"
        '    SQL = SQL & " WHERE a.acerto_id = b.acerto_id "
        '    SQL = SQL & "   AND b.cliente_id = c.cliente_id"
        '    If flagAprovados Then
        '       SQL = SQL & "   AND a.situacao in ('a','e')"
        '    Else
        '       SQL = SQL & "   AND a.situacao='p'"
        '    End If
        '    SQL = SQL & "   AND d.acerto_id = a.acerto_id" & vbNewLine
        '-------------------------------------------------------------------------------
        SQL = ""
        SQL = SQL & " SET NOCOUNT ON " & vbNewLine
        ' In�cio Altera��o - Tales de S� - INC000004585314
        ' Tratamento para limitar os registros no grid at� que a demanda evolutiva seja realizada para incluir filtros
        'SQL = SQL & " SELECT a.*, c.nome, tp_favorecido = 'clie', d.dt_geracao " & vbNewLine
        SQL = SQL & " SELECT TOP 25000 a.*, c.nome, tp_favorecido = 'clie', d.dt_geracao " & vbNewLine
        ' Fim Altera��o - INC000004585314
        SQL = SQL & "      , d.val_total_acerto, d.retido_banco, dt_acerto     " & vbNewLine
        SQL = SQL & "      , empresa = " & glAmbiente_id & vbNewLine
        'SQL = SQL & "   INTO #acertos " & vbNewLine
        SQL = SQL & "   FROM ps_acerto_pagamento_tb a   WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "   JOIN ps_acerto_cliente_tb b   WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "     ON b.acerto_id = a.acerto_id " & vbNewLine
        SQL = SQL & "   JOIN cliente_tb c   WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "     ON c.cliente_id = b.cliente_id " & vbNewLine
        SQL = SQL & "   JOIN ps_acerto_tb d  WITH (NOLOCK)  " & vbNewLine
        SQL = SQL & "     ON d.acerto_id = a.acerto_id " & vbNewLine
        SQL = SQL & "  WHERE 1 = 1 " & vbNewLine
        If flagAprovados Then
            SQL = SQL & "    AND a.situacao in ('a','e')" & vbNewLine
        Else
            SQL = SQL & "    AND a.situacao='p' " & vbNewLine
        End If


        '-- (IN�CIO) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        'MATHAYDE
        '    If bUsarConexaoABS Then
        '
        '        'FLOW 3240956 - Marcelo Ferreira - Confitec Sistemas - 2010-04-26
        '        'Altera��o da estrutura abaixo para ser utilizada em tempor�ria.
        '        '-------------------------------------------------------------------------------
        '        '        SQL = SQL & " UNION ALL" & vbNewLine
        '        '        SQL = SQL & "SELECT top 25000 a.*, c.nome, tp_favorecido = 'clie', d.dt_geracao" & vbNewLine
        '        '        SQL = SQL & ",  d.val_total_acerto, d.retido_banco, dt_acerto " & vbNewLine
        '        '        SQL = SQL & ", empresa = " & glAmbiente_id_seg2 & vbNewLine
        '        '        SQL = SQL & " FROM ABSS.seguros_db.dbo.ps_acerto_pagamento_tb a, ABSS.seguros_db.dbo.ps_acerto_cliente_tb b, ABSS.seguros_db.dbo.cliente_tb c, " & vbNewLine
        '        '        SQL = SQL & "      ABSS.seguros_db.dbo.ps_acerto_tb d" & vbNewLine
        '        '        SQL = SQL & " WHERE a.acerto_id = b.acerto_id " & vbNewLine
        '        '        SQL = SQL & "   AND b.cliente_id = c.cliente_id" & vbNewLine
        '        '        If flagAprovados Then
        '        '           SQL = SQL & "   AND a.situacao in ('a','e')" & vbNewLine
        '        '        Else
        '        '           SQL = SQL & "   AND a.situacao='p'" & vbNewLine
        '        '        End If
        '        '        SQL = SQL & "   AND d.acerto_id = a.acerto_id" & vbNewLine
        '        '-------------------------------------------------------------------------------
        '
        '        SQL = SQL & "  UNION ALL " & vbNewLine
        '        SQL = SQL & " SELECT a.*, c.nome, tp_favorecido = 'clie', d.dt_geracao " & vbNewLine
        '        SQL = SQL & "      , d.val_total_acerto, d.retido_banco, dt_acerto     " & vbNewLine
        '        SQL = SQL & "      , empresa = " & glAmbiente_id_seg2 & vbNewLine
        '        SQL = SQL & "   FROM ABSS.seguros_db.dbo.ps_acerto_pagamento_tb a " & vbNewLine
        '        SQL = SQL & "   JOIN ABSS.seguros_db.dbo.ps_acerto_cliente_tb b " & vbNewLine
        '        SQL = SQL & "     ON b.acerto_id = a.acerto_id " & vbNewLine
        '        SQL = SQL & "   JOIN ABSS.seguros_db.dbo.cliente_tb c " & vbNewLine
        '        SQL = SQL & "     ON c.cliente_id = b.cliente_id " & vbNewLine
        '        SQL = SQL & "   JOIN ABSS.seguros_db.dbo.ps_acerto_tb d" & vbNewLine
        '        SQL = SQL & "     ON d.acerto_id = a.acerto_id " & vbNewLine
        '        SQL = SQL & "  WHERE 1 = 1 " & vbNewLine
        '        If flagAprovados Then
        '           SQL = SQL & "    AND a.situacao in ('a','e')" & vbNewLine
        '        Else
        '           SQL = SQL & "    AND a.situacao='p' " & vbNewLine
        '        End If
        '
        '    End If
        '-- (FIM) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        'SQL = SQL & " ORDER BY dt_acerto desc" 'FLOW 3240956 - Marcelo Ferreira - Confitec Sistemas - 2010-04-26


        'SQL = SQL & " " & vbNewLine
        'SQL = SQL & " SELECT TOP 25000 * " & vbNewLine
        'SQL = SQL & "   FROM #acertos " & vbNewLine
        SQL = SQL & "  ORDER BY dt_acerto DESC " & vbNewLine

    Case "IOF(AINDA N�O RETIDO)"

        ' In�cio Altera��o - Tales de S� - INC000004585314
        ' Tratamento para limitar os registros no grid at� que a demanda evolutiva seja realizada para incluir filtros
        'SQL = " SELECT a.*, c.nome, tp_favorecido = 'item', d.dt_geracao"
        SQL = " SELECT TOP 25000 a.*, c.nome, tp_favorecido = 'item', d.dt_geracao"
        ' Fim Altera��o - INC000004585314
        SQL = SQL & ", d.val_total_acerto, d.retido_banco, dt_acerto "
        SQL = SQL & ", empresa = " & glAmbiente_id & vbNewLine
        SQL = SQL & " FROM ps_acerto_pagamento_tb a  WITH (NOLOCK)  , ps_acerto_item_financ_tb b  WITH (NOLOCK)  "
        SQL = SQL & ", item_financeiro_tb c  WITH (NOLOCK)  , ps_acerto_tb d   WITH (NOLOCK)  "
        SQL = SQL & " WHERE a.acerto_id = b.acerto_id "
        SQL = SQL & "   AND b.cod_item_financeiro = c.cod_item_financeiro"
        SQL = SQL & "   AND b.cod_item_financeiro = 'IOF' "
        SQL = SQL & "   AND a.situacao = 'p'"
        'SQL = SQL & "   AND d.retido_banco ='n'"
        SQL = SQL & "   AND d.acerto_id = a.acerto_id"

        '-- (IN�CIO) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        'MATHAYDE
        '    If bUsarConexaoABS Then
        '        SQL = SQL & " UNION ALL" & vbNewLine
        '        SQL = SQL & " SELECT a.*, c.nome, tp_favorecido = 'item', d.dt_geracao" & vbNewLine
        '        SQL = SQL & ", d.val_total_acerto, d.retido_banco, dt_acerto " & vbNewLine
        '        SQL = SQL & ", empresa = " & glAmbiente_id_seg2 & vbNewLine
        '        SQL = SQL & " FROM ABSS.seguros_db.dbo.ps_acerto_pagamento_tb a, ABSS.seguros_db.dbo.ps_acerto_item_financ_tb b" & vbNewLine
        '        SQL = SQL & ", ABSS.seguros_db.dbo.item_financeiro_tb c, ABSS.seguros_db.dbo.ps_acerto_tb d " & vbNewLine
        '        SQL = SQL & " WHERE a.acerto_id = b.acerto_id " & vbNewLine
        '        SQL = SQL & "   AND b.cod_item_financeiro = c.cod_item_financeiro" & vbNewLine
        '        SQL = SQL & "   AND b.cod_item_financeiro = 'IOF' " & vbNewLine
        '        SQL = SQL & "   AND a.situacao = 'p'" & vbNewLine
        '        SQL = SQL & "   AND d.acerto_id = a.acerto_id" & vbNewLine
        '    End If
        '-- (FIM) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        SQL = SQL & " ORDER BY dt_geracao"

    Case "RESSEGURO"

        ' In�cio Altera��o - Tales de S� - INC000004585314
        ' Tratamento para limitar os registros no grid at� que a demanda evolutiva seja realizada para incluir filtros
        'SQL = " SELECT a.*, c.nome, tp_favorecido = 'ress', d.dt_geracao,  d.val_total_acerto, d.retido_banco, dt_acerto  "
        SQL = " SELECT TOP 25000 a.*, c.nome, tp_favorecido = 'ress', d.dt_geracao,  d.val_total_acerto, d.retido_banco, dt_acerto  "
        ' Fim Altera��o - INC000004585314
        SQL = SQL & ", empresa = " & glAmbiente_id & vbNewLine
        SQL = SQL & " FROM ps_acerto_pagamento_tb a  WITH (NOLOCK)  , ps_acerto_res_tb b  WITH (NOLOCK)  , resseguradora_tb c  WITH (NOLOCK)  , "
        SQL = SQL & "      ps_acerto_tb d  WITH (NOLOCK)  "
        SQL = SQL & " WHERE a.acerto_id = b.acerto_id "
        SQL = SQL & "   AND b.resseguradora_id = c.resseguradora_id"
        SQL = SQL & "   AND a.situacao='p'"
        SQL = SQL & "   AND d.acerto_id = a.acerto_id"


        '-- (IN�CIO) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        'MATHAYDE
        '    If bUsarConexaoABS Then
        '        SQL = SQL & " UNION ALL" & vbNewLine
        '        SQL = SQL & " SELECT a.*, c.nome, tp_favorecido = 'ress', d.dt_geracao,  d.val_total_acerto, d.retido_banco, dt_acerto  " & vbNewLine
        '        SQL = SQL & ", empresa = " & glAmbiente_id_seg2 & vbNewLine
        '        SQL = SQL & " FROM ABSS.seguros_db.dbo.ps_acerto_pagamento_tb a, ABSS.seguros_db.dbo.ps_acerto_res_tb b, ABSS.seguros_db.dbo.resseguradora_tb c, " & vbNewLine
        '        SQL = SQL & "      ABSS.seguros_db.dbo.ps_acerto_tb d" & vbNewLine
        '        SQL = SQL & " WHERE a.acerto_id = b.acerto_id " & vbNewLine
        '        SQL = SQL & "   AND b.resseguradora_id = c.resseguradora_id" & vbNewLine
        '        SQL = SQL & "   AND a.situacao='p'" & vbNewLine
        '        SQL = SQL & "   AND d.acerto_id = a.acerto_id" & vbNewLine
        '    End If
        '-- (FIM) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        SQL = SQL & " ORDER BY dt_geracao"

    Case "SINISTRO"

        ' In�cio Altera��o - Tales de S� - INC000004585314
        ' Tratamento para limitar os registros no grid at� que a demanda evolutiva seja realizada para incluir filtros
        'SQL = " SELECT a.*, d.nome, tp_favorecido = 'sini', c.dt_geracao"
        SQL = " SELECT TOP 25000 a.*, d.nome, tp_favorecido = 'sini', c.dt_geracao"
        ' Fim Altera��o - INC000004585314
        SQL = SQL & ", c.val_total_acerto, c.retido_banco, b.item_val_estimativa"
        SQL = SQL & ", dt_acerto "
        SQL = SQL & ", empresa = " & glAmbiente_id & vbNewLine
        SQL = SQL & " FROM ps_acerto_pagamento_tb a   WITH (NOLOCK)  "
        SQL = SQL & " INNER JOIN      ps_acerto_sinistro_tb b   WITH (NOLOCK)  on a.acerto_id = b.acerto_id "
        SQL = SQL & " INNER JOIN      ps_acerto_tb c   WITH (NOLOCK)  on a.acerto_id = c.acerto_id "
        SQL = SQL & " INNER JOIN      sinistro_benef_tb d   WITH (NOLOCK)  on b.sinistro_id = d.sinistro_id and "
        SQL = SQL & "                                   b.apolice_id = d.apolice_id and "
        SQL = SQL & "                                   b.beneficiario_id = d.beneficiario_id "

        '-- (IN�CIO) -- RSOUZA -- 27/09/2010 ------------------------------------'
        SQL = SQL & " INNER JOIN  sinistro_tb   WITH (NOLOCK)  "
        SQL = SQL & "         on b.sinistro_id = sinistro_tb.sinistro_id "
        SQL = SQL & "        AND sinistro_tb.situacao <> 7 "
        '-- (FIM) -- RSOUZA -- 27/09/2010 ---------------------------------------'

        SQL = SQL & " WHERE a.situacao='p' "

        '-- (IN�CIO) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        'MATHAYDE
        '    If bUsarConexaoABS Then
        '        SQL = SQL & " UNION ALL" & vbNewLine
        '        SQL = SQL & " SELECT a.*, d.nome, tp_favorecido = 'sini', c.dt_geracao" & vbNewLine
        '        SQL = SQL & ", c.val_total_acerto, c.retido_banco, b.item_val_estimativa" & vbNewLine
        '        SQL = SQL & ", dt_acerto " & vbNewLine
        '        SQL = SQL & ", empresa = " & glAmbiente_id_seg2 & vbNewLine
        '        SQL = SQL & " FROM ABSS.seguros_db.dbo.ps_acerto_pagamento_tb a " & vbNewLine
        '        SQL = SQL & " INNER JOIN      ABSS.seguros_db.dbo.ps_acerto_sinistro_tb b on a.acerto_id = b.acerto_id " & vbNewLine
        '        SQL = SQL & " INNER JOIN      ABSS.seguros_db.dbo.ps_acerto_tb c on a.acerto_id = c.acerto_id " & vbNewLine
        '        SQL = SQL & " INNER JOIN      ABSS.seguros_db.dbo.sinistro_benef_tb d on b.sinistro_id = d.sinistro_id and " & vbNewLine
        '        SQL = SQL & "                                   b.apolice_id = d.apolice_id and " & vbNewLine
        '        SQL = SQL & "                                   b.beneficiario_id = d.beneficiario_id " & vbNewLine
        '        SQL = SQL & " WHERE a.situacao='p' " & vbNewLine
        '    End If
        '-- (FIM) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        SQL = SQL & " ORDER BY nome"


    Case "COMISS�O RETIDA"    'RMAURI 03/01/2007 (Altera��o para Procedure) (Inclusao de Ramo e Produto)
        '-- (IN�CIO) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        'mathayde
        '    If bUsarConexaoABS Then
        '        SQL = ""
        '        SQL = "SET NOCOUNT ON " & vbNewLine
        '        SQL = SQL & " CREATE TABLE #movimentacao" & vbNewLine
        '        SQL = SQL & "      ( acerto_id        INT" & vbNewLine
        '        SQL = SQL & "      , voucher_id       INT" & vbNewLine
        '        SQL = SQL & "      , cod_origem       CHAR          (2)" & vbNewLine
        '        SQL = SQL & "      , dt_inclusao      SMALLDATETIME" & vbNewLine
        '        SQL = SQL & "      , situacao         CHAR          (1)" & vbNewLine
        '        SQL = SQL & "      , dt_alteracao     SMALLDATETIME" & vbNewLine
        '        SQL = SQL & "      , usuario          VARCHAR       (20)" & vbNewLine
        '        SQL = SQL & "      , lock             VARCHAR       (60)" & vbNewLine
        '        SQL = SQL & "      , dt_geracao       SMALLDATETIME" & vbNewLine
        '        SQL = SQL & "      , val_total_acerto NUMERIC       (15,2)" & vbNewLine
        '        SQL = SQL & "      , retido_banco     CHAR          (1)" & vbNewLine
        '        SQL = SQL & "      , dt_acerto        SMALLDATETIME" & vbNewLine
        '        SQL = SQL & "      , ramo_id          INT" & vbNewLine
        '        SQL = SQL & "      , produto_id       INT" & vbNewLine
        '        SQL = SQL & "      , tp_favorecido    VARCHAR       (4)" & vbNewLine
        '        SQL = SQL & "      , nome             VARCHAR       (60)" & vbNewLine
        '        SQL = SQL & "      , ramo_id2         INT" & vbNewLine
        '        SQL = SQL & "      , ramo_nome        VARCHAR       (67)" & vbNewLine
        '        SQL = SQL & "      , produto_id2      INT" & vbNewLine
        '        SQL = SQL & "      , produto_nome     VARCHAR       (67)" & vbNewLine
        '        SQL = SQL & "      , nome2            VARCHAR       (60)" & vbNewLine
        '        SQL = SQL & "      , empresa          VARCHAR       (2))" & vbNewLine
        '
        '        SQL = SQL & " INSERT INTO #movimentacao" & vbNewLine
        '        SQL = SQL & "      ( acerto_id" & vbNewLine
        '        SQL = SQL & "      , voucher_id" & vbNewLine
        '        SQL = SQL & "      , cod_origem" & vbNewLine
        '        SQL = SQL & "      , dt_inclusao" & vbNewLine
        '        SQL = SQL & "      , situacao" & vbNewLine
        '        SQL = SQL & "      , dt_alteracao" & vbNewLine
        '        SQL = SQL & "      , usuario" & vbNewLine
        '        SQL = SQL & "      , lock" & vbNewLine
        '        SQL = SQL & "      , dt_geracao" & vbNewLine
        '        SQL = SQL & "      , val_total_acerto" & vbNewLine
        '        SQL = SQL & "      , retido_banco" & vbNewLine
        '        SQL = SQL & "      , dt_acerto" & vbNewLine
        '        SQL = SQL & "      , ramo_id" & vbNewLine
        '        SQL = SQL & "      , produto_id" & vbNewLine
        '        SQL = SQL & "      , tp_favorecido" & vbNewLine
        '        SQL = SQL & "      , nome" & vbNewLine
        '        SQL = SQL & "      , ramo_id2" & vbNewLine
        '        SQL = SQL & "      , ramo_nome" & vbNewLine
        '        SQL = SQL & "      , produto_id2" & vbNewLine
        '        SQL = SQL & "      , produto_nome" & vbNewLine
        '        SQL = SQL & "      , nome2            )" & vbNewLine
        '
        '        SQL = SQL & "   exec SEGS5620_SPS @comissao='s', @pro_labore = 'n', @situacao = '" & IIf(flagAprovados, "a", "p") & "'" & vbNewLine
        '
        '        SQL = SQL & " UPDATE #movimentacao "
        '        SQL = SQL & "    SET  empresa = " & glAmbiente_id
        '
        '        SQL = SQL & " INSERT INTO #movimentacao" & vbNewLine
        '        SQL = SQL & "      ( acerto_id" & vbNewLine
        '        SQL = SQL & "      , voucher_id" & vbNewLine
        '        SQL = SQL & "      , cod_origem" & vbNewLine
        '        SQL = SQL & "      , dt_inclusao" & vbNewLine
        '        SQL = SQL & "      , situacao" & vbNewLine
        '        SQL = SQL & "      , dt_alteracao" & vbNewLine
        '        SQL = SQL & "      , usuario" & vbNewLine
        '        SQL = SQL & "      , lock" & vbNewLine
        '        SQL = SQL & "      , dt_geracao" & vbNewLine
        '        SQL = SQL & "      , val_total_acerto" & vbNewLine
        '        SQL = SQL & "      , retido_banco" & vbNewLine
        '        SQL = SQL & "      , dt_acerto" & vbNewLine
        '        SQL = SQL & "      , ramo_id" & vbNewLine
        '        SQL = SQL & "      , produto_id" & vbNewLine
        '        SQL = SQL & "      , tp_favorecido" & vbNewLine
        '        SQL = SQL & "      , nome" & vbNewLine
        '        SQL = SQL & "      , ramo_id2" & vbNewLine
        '        SQL = SQL & "      , ramo_nome" & vbNewLine
        '        SQL = SQL & "      , produto_id2" & vbNewLine
        '        SQL = SQL & "      , produto_nome" & vbNewLine
        '        SQL = SQL & "      , nome2            )" & vbNewLine
        '
        '        SQL = SQL & "   exec ABSS.seguros_db.dbo.SEGS5620_SPS @comissao='s', @pro_labore = 'n', @situacao = '" & IIf(flagAprovados, "a", "p") & "'" & vbNewLine
        '
        '        SQL = SQL & " UPDATE #movimentacao " & vbNewLine
        '        SQL = SQL & "    SET empresa = " & glAmbiente_id_seg2 & vbNewLine
        '        SQL = SQL & "  WHERE empresa IS NULL" & vbNewLine
        '
        '        SQL = SQL & " SELECT * FROM #movimentacao" & vbNewLine
        '
        '        SQL = SQL & " DROP TABLE #movimentacao" & vbNewLine
        '    Else
        '        SQL = "   exec SEGS5620_SPS @comissao='s', @pro_labore = 'n', @situacao = '" & IIf(flagAprovados, "a", "p") & "'" & vbNewLine
        '
        '    End If
        '-- (FIM) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'

        SQL = "   exec SEGS5620_SPS @comissao='s', @pro_labore = 'n', @situacao = '" & IIf(flagAprovados, "a", "p") & "'" & vbNewLine


    Case "PR�-LABORE RETIDO"    'RMAURI 03/01/2007 (Altera��o para Procedure) (Inclusao de Ramo e Produto)

        '-- (IN�CIO) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        'mathayde
        '    If bUsarConexaoABS Then
        '        SQL = ""
        '        SQL = "SET NOCOUNT ON " & vbNewLine
        '        SQL = SQL & " CREATE TABLE #movimentacao" & vbNewLine
        '        SQL = SQL & "      ( acerto_id        INT" & vbNewLine
        '        SQL = SQL & "      , voucher_id       INT" & vbNewLine
        '        SQL = SQL & "      , cod_origem       CHAR          (2)" & vbNewLine
        '        SQL = SQL & "      , dt_inclusao      SMALLDATETIME" & vbNewLine
        '        SQL = SQL & "      , situacao         CHAR          (1)" & vbNewLine
        '        SQL = SQL & "      , dt_alteracao     SMALLDATETIME" & vbNewLine
        '        SQL = SQL & "      , usuario          VARCHAR       (20)" & vbNewLine
        '        SQL = SQL & "      , lock             VARCHAR       (60)" & vbNewLine
        '        SQL = SQL & "      , dt_geracao       SMALLDATETIME" & vbNewLine
        '        SQL = SQL & "      , val_total_acerto NUMERIC       (15,2)" & vbNewLine
        '        SQL = SQL & "      , retido_banco     CHAR          (1)" & vbNewLine
        '        SQL = SQL & "      , dt_acerto        SMALLDATETIME" & vbNewLine
        '        SQL = SQL & "      , ramo_id          INT" & vbNewLine
        '        SQL = SQL & "      , produto_id       INT" & vbNewLine
        '        SQL = SQL & "      , tp_favorecido    VARCHAR       (4)" & vbNewLine
        '        SQL = SQL & "      , nome             VARCHAR       (60)" & vbNewLine
        '        SQL = SQL & "      , ramo_id2         INT" & vbNewLine
        '        SQL = SQL & "      , ramo_nome        VARCHAR       (67)" & vbNewLine
        '        SQL = SQL & "      , produto_id2      INT" & vbNewLine
        '        SQL = SQL & "      , produto_nome     VARCHAR       (67)" & vbNewLine
        '        SQL = SQL & "      , nome2            VARCHAR       (60)" & vbNewLine
        '        SQL = SQL & "      , empresa          VARCHAR       (2))" & vbNewLine
        '
        '        SQL = SQL & " INSERT INTO #movimentacao" & vbNewLine
        '        SQL = SQL & "      ( acerto_id" & vbNewLine
        '        SQL = SQL & "      , voucher_id" & vbNewLine
        '        SQL = SQL & "      , cod_origem" & vbNewLine
        '        SQL = SQL & "      , dt_inclusao" & vbNewLine
        '        SQL = SQL & "      , situacao" & vbNewLine
        '        SQL = SQL & "      , dt_alteracao" & vbNewLine
        '        SQL = SQL & "      , usuario" & vbNewLine
        '        SQL = SQL & "      , lock" & vbNewLine
        '        SQL = SQL & "      , dt_geracao" & vbNewLine
        '        SQL = SQL & "      , val_total_acerto" & vbNewLine
        '        SQL = SQL & "      , retido_banco" & vbNewLine
        '        SQL = SQL & "      , dt_acerto" & vbNewLine
        '        SQL = SQL & "      , ramo_id" & vbNewLine
        '        SQL = SQL & "      , produto_id" & vbNewLine
        '        SQL = SQL & "      , tp_favorecido" & vbNewLine
        '        SQL = SQL & "      , nome" & vbNewLine
        '        SQL = SQL & "      , ramo_id2" & vbNewLine
        '        SQL = SQL & "      , ramo_nome" & vbNewLine
        '        SQL = SQL & "      , produto_id2" & vbNewLine
        '        SQL = SQL & "      , produto_nome" & vbNewLine
        '        SQL = SQL & "      , nome2            )" & vbNewLine
        '
        '        SQL = SQL & "   exec SEGS5620_SPS @comissao = 'n', @pro_labore = 's', @situacao = '" & IIf(flagAprovados, "a", "p") & "'" & vbNewLine
        '
        '        SQL = SQL & " UPDATE #movimentacao " & vbNewLine
        '        SQL = SQL & "    SET empresa = " & glAmbiente_id & vbNewLine
        '        SQL = SQL & "  WHERE empresa IS NULL" & vbNewLine
        '
        '        SQL = SQL & " INSERT INTO #movimentacao" & vbNewLine
        '        SQL = SQL & "      ( acerto_id" & vbNewLine
        '        SQL = SQL & "      , voucher_id" & vbNewLine
        '        SQL = SQL & "      , cod_origem" & vbNewLine
        '        SQL = SQL & "      , dt_inclusao" & vbNewLine
        '        SQL = SQL & "      , situacao" & vbNewLine
        '        SQL = SQL & "      , dt_alteracao" & vbNewLine
        '        SQL = SQL & "      , usuario" & vbNewLine
        '        SQL = SQL & "      , lock" & vbNewLine
        '        SQL = SQL & "      , dt_geracao" & vbNewLine
        '        SQL = SQL & "      , val_total_acerto" & vbNewLine
        '        SQL = SQL & "      , retido_banco" & vbNewLine
        '        SQL = SQL & "      , dt_acerto" & vbNewLine
        '        SQL = SQL & "      , ramo_id" & vbNewLine
        '        SQL = SQL & "      , produto_id" & vbNewLine
        '        SQL = SQL & "      , tp_favorecido" & vbNewLine
        '        SQL = SQL & "      , nome" & vbNewLine
        '        SQL = SQL & "      , ramo_id2" & vbNewLine
        '        SQL = SQL & "      , ramo_nome" & vbNewLine
        '        SQL = SQL & "      , produto_id2" & vbNewLine
        '        SQL = SQL & "      , produto_nome" & vbNewLine
        '        SQL = SQL & "      , nome2            )" & vbNewLine
        '
        '        SQL = SQL & "   exec ABSS.seguros_db.dbo.SEGS5620_SPS @comissao = 'n', @pro_labore = 's', @situacao = '" & IIf(flagAprovados, "a", "p") & "'" & vbNewLine
        '
        '        SQL = SQL & " UPDATE #movimentacao " & vbNewLine
        '        SQL = SQL & "    SET empresa = " & glAmbiente_id_seg2 & vbNewLine
        '        SQL = SQL & "  WHERE empresa IS NULL" & vbNewLine
        '
        '        SQL = SQL & " SELECT * FROM #movimentacao" & vbNewLine
        '
        '        SQL = SQL & " DROP TABLE #movimentacao" & vbNewLine
        '    Else
        '        SQL = "exec SEGS5620_SPS @comissao = 'n', @pro_labore = 's', @situacao = '" & IIf(flagAprovados, "a", "p") & "'" & vbNewLine
        '    End If
        '-- (FIM) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        SQL = "exec SEGS5620_SPS @comissao = 'n', @pro_labore = 's', @situacao = '" & IIf(flagAprovados, "a", "p") & "'" & vbNewLine



        ''mathayde 24/07/2009 demanda : 654224
        'Query que retorna os acerto do cart�o de cr�dito
    Case "COMISS�O RETIDA CART�O DE CR�DITO"

        SQL = ""
        ' In�cio Altera��o - Tales de S� - INC000004585314
        ' Tratamento para limitar os registros no grid at� que a demanda evolutiva seja realizada para incluir filtros
        'SQL = SQL & "SELECT DISTINCT" & vbNewLine
        SQL = SQL & "SELECT DISTINCT TOP 25000 " & vbNewLine
        ' Fim Altera��o - INC000004585314
        SQL = SQL & "       a.*," & vbNewLine
        SQL = SQL & "       c.dt_geracao," & vbNewLine
        SQL = SQL & "       f.dt_repasse," & vbNewLine
        SQL = SQL & "       c.val_total_acerto," & vbNewLine
        SQL = SQL & "       c.retido_banco," & vbNewLine
        SQL = SQL & "       c.dt_acerto," & vbNewLine
        SQL = SQL & "       tp_favorecido = 'cart' ," & vbNewLine
        SQL = SQL & "       nome = 'Banco do Brasil'," & vbNewLine
        SQL = SQL & "       arquivo_envio_bb" & vbNewLine
        SQL = SQL & "     , empresa = " & glAmbiente_id & vbNewLine
        SQL = SQL & "  FROM ps_acerto_pagamento_tb a  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "  JOIN ps_acerto_cartao_credito_tb b  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON b.acerto_id = a.acerto_id" & vbNewLine
        SQL = SQL & "  JOIN ps_acerto_tb c  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON c.acerto_id = b.acerto_id" & vbNewLine
        SQL = SQL & "  JOIN ps_mov_repasse_bb_tb d  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON d.acerto_id = a.acerto_id" & vbNewLine
        SQL = SQL & "  JOIN ps_mov_repasse_cartao_credito_tb e  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON e.movimentacao_id = d.movimentacao_id" & vbNewLine
        SQL = SQL & "  JOIN repasse_cartao_credito_tb f  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "    ON f.repasse_cartao_credito_id = e.repasse_cartao_credito_id" & vbNewLine
        SQL = SQL & " WHERE a.cod_origem = 'CR'" & vbNewLine

        If flagAprovados Then
            SQL = SQL & "   AND a.situacao in ('a','e')"
        Else
            SQL = SQL & "   AND a.situacao='p'"
        End If

        'Gabriel C�mara - 17082374 - IPrem - Custo Assist�ncia
    Case "CUSTO ASSIST�NCIA"

        SQL = ""
        ' In�cio Altera��o - Tales de S� - INC000004585314
        ' Tratamento para limitar os registros no grid at� que a demanda evolutiva seja realizada para incluir filtros
        'SQL = SQL & "SELECT ps_acerto_assistencia_tb.acerto_id," & vbNewLine
        SQL = SQL & "SELECT TOP 25000 ps_acerto_assistencia_tb.acerto_id," & vbNewLine
        ' Fim Altera��o - INC000004585314
        SQL = SQL & "nome = 'Brasil Assist�ncia'," & vbNewLine
        SQL = SQL & "tp_favorecido = 'assi' ," & vbNewLine
        SQL = SQL & "ps_acerto_assistencia_tb.voucher_id," & vbNewLine
        SQL = SQL & "ps_acerto_tb.dt_geracao," & vbNewLine
        SQL = SQL & "ps_acerto_tb.val_total_acerto," & vbNewLine
        SQL = SQL & "ps_acerto_tb.retido_banco," & vbNewLine
        SQL = SQL & "ps_acerto_tb.dt_acerto," & vbNewLine
        SQL = SQL & "ps_acerto_pagamento_tb.situacao" & vbNewLine
        SQL = SQL & "FROM seguros_db.dbo.ps_acerto_assistencia_tb ps_acerto_assistencia_tb WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "JOIN seguros_db.dbo.ps_acerto_tb ps_acerto_tb WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "ON ps_acerto_tb.acerto_id = ps_acerto_assistencia_tb.acerto_id" & vbNewLine
        SQL = SQL & "JOIN seguros_db.dbo.ps_acerto_pagamento_tb ps_acerto_pagamento_tb  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "ON ps_acerto_pagamento_tb.acerto_id = ps_acerto_tb.acerto_id " & vbNewLine
        SQL = SQL & "WHERE 1 = 1 " & vbNewLine
        If flagAprovados Then
            SQL = SQL & "AND ps_acerto_pagamento_tb.situacao in ('a','e')" & vbNewLine
        Else
            SQL = SQL & "AND ps_acerto_pagamento_tb.situacao='p' " & vbNewLine
            SQL = SQL & "AND ps_acerto_tb.dt_aprovacao IS NULL" & vbNewLine
        End If


        '-- (IN�CIO) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
        'MATHAYDE
        '    If bUsarConexaoABS Then
        '        SQL = SQL & " UNION ALL" & vbNewLine
        '        SQL = SQL & "SELECT DISTINCT" & vbNewLine
        '        SQL = SQL & "       a.*," & vbNewLine
        '        SQL = SQL & "       c.dt_geracao," & vbNewLine
        '        SQL = SQL & "       f.dt_repasse," & vbNewLine
        '        SQL = SQL & "       c.val_total_acerto," & vbNewLine
        '        SQL = SQL & "       c.retido_banco," & vbNewLine
        '        SQL = SQL & "       c.dt_acerto," & vbNewLine
        '        SQL = SQL & "       tp_favorecido = 'cart' ," & vbNewLine
        '        SQL = SQL & "       nome = 'Banco do Brasil'," & vbNewLine
        '        SQL = SQL & "       arquivo_envio_bb" & vbNewLine
        '        SQL = SQL & "     , empresa = " & glAmbiente_id_seg2 & vbNewLine
        '        SQL = SQL & "  FROM ABSS.seguros_db.dbo.ps_acerto_pagamento_tb a  WITH (NOLOCK) " & vbNewLine
        '        SQL = SQL & "  JOIN ABSS.seguros_db.dbo.ps_acerto_cartao_credito_tb b  WITH (NOLOCK) " & vbNewLine
        '        SQL = SQL & "    ON b.acerto_id = a.acerto_id" & vbNewLine
        '        SQL = SQL & "  JOIN ABSS.seguros_db.dbo.ps_acerto_tb c  WITH (NOLOCK) " & vbNewLine
        '        SQL = SQL & "    ON c.acerto_id = b.acerto_id" & vbNewLine
        '        SQL = SQL & "  JOIN ABSS.seguros_db.dbo.ps_mov_repasse_bb_tb d  WITH (NOLOCK) " & vbNewLine
        '        SQL = SQL & "    ON d.acerto_id = a.acerto_id" & vbNewLine
        '        SQL = SQL & "  JOIN ABSS.seguros_db.dbo.ps_mov_repasse_cartao_credito_tb e  WITH (NOLOCK) " & vbNewLine
        '        SQL = SQL & "    ON e.movimentacao_id = d.movimentacao_id" & vbNewLine
        '        SQL = SQL & "  JOIN ABSS.seguros_db.dbo.repasse_cartao_credito_tb f  WITH (NOLOCK) " & vbNewLine
        '        SQL = SQL & "    ON f.repasse_cartao_credito_id = e.repasse_cartao_credito_id" & vbNewLine
        '        SQL = SQL & " WHERE a.cod_origem = 'CR'" & vbNewLine
        '
        '        If flagAprovados Then
        '            SQL = SQL & "   AND a.situacao in ('a','e')" & vbNewLine
        '        Else
        '            SQL = SQL & "   AND a.situacao='p'" & vbNewLine
        '        End If
        '    End If
        '-- (FIM) -- RSOUZA -- 27/09/2010 -------------------------------------------------------------------------------------------------------------------------------------------'
    End Select

    Monta_Query_Pagamento = SQL

End Function

Private Sub cmdSair_Click()

    Unload Me

End Sub

Private Sub Form_GotFocus()
If Me.Visible Then Me.Refresh
End Sub

Private Sub Form_Load()

CentraFrm Me

gridPagamentos.ColWidth(10) = 0         'Data Cria��o para ordenar
gridPagamentos.ColWidth(11) = 0         'Data Pagamento para ordenar

'claudia.araujo - travar aprova��o para os tipos de pagamentos COMISS�O N�O-RETIDA e PR�-LABORE N�O-RETIDO para GEMPE - 12/07/11
If flagPerfil Then
   Me.cmdAprovar.Enabled = True
Else
   Me.cmdAprovar.Enabled = False
End If


Me.Caption = "SEG00270 - Aprova��o de Pagamentos - " & Ambiente

sDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
If sDecimal = "." Then
    ConfiguracaoBrasil = False
Else
    ConfiguracaoBrasil = True
End If

Dt_Sistema = Data_Sistema
    
End Sub

Private Sub Form_Activate()
'MATHAYDE - ??? OS RPTs ESTAR�O HOSPEDADOS NA MESMA PASTA?
arqpath = LerArquivoIni("producao", "producao_path")

If UCase(TpAprovacao) = "CORRETAGEM" Or UCase(TpAprovacao) = "PRO-LABORE" Then
   cmdImpressao.Caption = "Extrato"
Else
   cmdImpressao.Caption = "Imprime AP"
End If

'PAULO PELEGRINI - 18958788 - AJUSTE NA GERA��O DO EXTRATO DE PAGAMENTO A CORRETORES (INI)
If UCase(TpAprovacao) = "CORRETAGEM N�O-RETIDA" Then
    fraopt.Visible = True
    cmdImpressao.Enabled = False
Else
    fraopt.Visible = False
    cmdImpressao.Enabled = True
End If
'PAULO PELEGRINI - 18958788 - AJUSTE NA GERA��O DO EXTRATO DE PAGAMENTO A CORRETORES (FIM)

If flagAprovados Then
   cmdAprovar.Enabled = False
Else
   cmdAprovar.Enabled = True
  'claudia.araujo - travar aprova��o para os tipos de pagamentos COMISS�O N�O-RETIDA e PR�-LABORE N�O-RETIDO para GEMPE - 12/07/11
  If flagPerfil Then
     Me.cmdAprovar.Enabled = True
  Else
     Me.cmdAprovar.Enabled = False
  End If
End If

Select Case UCase(TpAprovacao)
    Case "COMISS�O RETIDA", "PR�-LABORE RETIDO"
        cmdImpressao.Visible = False
        gridPagamentos.FormatString = "^Aprovar   | Data Cria��o | Pagamento | Tipo Favorecido          | Favorecido                                       | Valor                    |Retido |Status       |Data Pagamento | Voucher | Dt Cria��o | Dt Pagamento | Ramo                                            | Produto                                          |empresa" 'mathayde
        gridPagamentos.ColAlignment(12) = 2
        gridPagamentos.ColAlignment(13) = 2
        'mathayde
        iColunaEmpresa = 14
        
    ''mathayde 05/08/2009 demanda : 654224
    'Formatando grid
    Case "COMISS�O RETIDA CART�O DE CR�DITO"
        cmdImpressao.Visible = False
        gridPagamentos.FormatString = "^Aprovar   | Data de Repasse | Pagamento | Tipo Favorecido          | Favorecido                                       | Valor                    |Retido |Status       |Data Pagamento | Voucher | Dt Cria��o | Dt Pagamento  | Arquivo Repasse BB |empresa" 'mathayde
        gridPagamentos.ColAlignment(12) = 2
        'mathayde
        iColunaEmpresa = 13

    'Gabriel C�mara - 17082374 - IPrem - Custo Assist�ncia
    Case "CUSTO ASSIST�NCIA"
        cmdImpressao.Visible = False
        gridPagamentos.FormatString = "^Aprovar   | Data Cria��o | Pagamento | Tipo Favorecido          | Favorecido                                       | Valor                    |Retido |Status       |Data Pagamento | Voucher | Dt Cria��o | Dt Pagamento |empresa"
        iColunaEmpresa = 12
        
    Case Else
        cmdImpressao.Visible = True
        gridPagamentos.FormatString = "^Aprovar   | Data Cria��o | Pagamento | Tipo Favorecido          | Favorecido                                       | Valor                    |Retido |Status       |Data Pagamento | Voucher | Dt Cria��o | Dt Pagamento |empresa" 'mathayde
        'mathayde
        iColunaEmpresa = 12
End Select

gridPagamentos.Rows = 1

'PAULO PELEGRINI - 18958788 - AJUSTE NA GERA��O DO EXTRATO DE PAGAMENTO A CORRETORES - adi��o de novo parametro
Monta_Grid_Pagamentos True, False

End Sub

Private Sub Form_Unload(Cancel As Integer)
Screen.MousePointer = vbDefault
End Sub

Private Sub gridPagamentos_Click()

If gridPagamentos.Row = 0 Then
    Exit Sub
End If

If gridPagamentos.Row = 1 And gridPagamentos.RowSel = gridPagamentos.Rows - 1 Then
'Ordena a coluna conforme click do mouse sobre a mesma
    vColuna = gridPagamentos.MouseCol

    If vColuna = 1 Then
        vColuna = 10
    ElseIf vColuna = 8 Then
        vColuna = 11
    End If

    gridPagamentos.Col = vColuna
    gridPagamentos.Sort = 1
    'Exit Sub
End If

If (gridPagamentos.Col = 0 And gridPagamentos.Row <> 0) Then
    If gridPagamentos.Text = "4" Then
        gridPagamentos.Text = ""
        AprPag = AprPag - 1
    Else
        gridPagamentos.CellFontName = "Monotype Sorts"
        gridPagamentos.Text = "4"
        AprPag = AprPag + 1
    End If
End If
End Sub


Private Sub gridPagamentos_DblClick()

If gridPagamentos.Rows = 1 Or gridPagamentos.RowSel = 0 Then Exit Sub

gridPagamentos.Col = 6
gridPagamentos.Row = gridPagamentos.RowSel

'If gridPagamentos.Text = "S" Then
'   MsgBox "Pagamento j� retido no banco. N�o pode ser alterado", vbInformation
'   Exit Sub
'End If

StatusBar1.SimpleText = "Aguarde, pesquisando registros..."
Screen.MousePointer = vbHourglass
frmPagAssociados.Show vbModal, Me
Screen.MousePointer = vbDefault
'Me.Hide   -  jailson
End Sub


Private Sub gridPagamentos_KeyDown(KeyCode As Integer, Shift As Integer)

Select Case KeyCode
Case 32
    If (gridPagamentos.Col = 0 And gridPagamentos.Row <> 0) Then
        If gridPagamentos.Text = "4" Then
            gridPagamentos.Text = ""
            AprPag = AprPag - 1
        Else
            gridPagamentos.CellFontName = "Monotype Sorts"
            gridPagamentos.Text = "4"
            AprPag = AprPag + 1
        End If
    End If
Case Else
End Select

End Sub

Private Sub Obtem_Proposta(ByVal acerto As String)
Dim rc    As rdoResultset
Dim SQL   As String

SQL = " SELECT proposta_id "
SQL = SQL & " FROM ps_acerto_cliente_tb  WITH (NOLOCK)  "
SQL = SQL & " WHERE acerto_id = " & acerto

Set rc = rdocn.OpenResultset(SQL)

proposta_id = rc!proposta_id

Exit Sub

erro:
    TrataErroGeral "Obtem Proposta"
    MsgBox "Rotina: Obtem Proposta. O programa ser� abortado.", vbCritical, "Erro"
    End

End Sub


Private Sub Obtem_Sinistro(ByVal acerto As String)
Dim rc    As rdoResultset
Dim SQL   As String


SQL = " SELECT sinistro_id "
SQL = SQL & " FROM ps_acerto_sinistro_tb  WITH (NOLOCK)  "
SQL = SQL & " WHERE acerto_id = " & acerto

Set rc = rdocn.OpenResultset(SQL)

sinistro_id = rc!sinistro_id

Exit Sub

erro:
    TrataErroGeral "Obtem Sinistro"
    MsgBox "Rotina: Obtem Sinistro. O programa ser� abortado.", vbCritical, "Erro"
    End

End Sub


Private Sub Obtem_Corretor(ByVal acerto As String)
Dim rc    As rdoResultset
Dim SQL   As String

On Error GoTo erro

SQL = "SELECT b.pj_corretor_id corretor, b.sucursal_corretor_id sucursal, 'pj' tp_pessoa, a.nome, b.cgc cpf_cgc, a.endereco, a.bairro, a.cep, banco, agencia, conta_corrente, a.ddd, a.telefone, a.ddd_fax_1, a.ddd_fax_2, a.telefone_fax_1, a.telefone_fax_2, a.e_mail"
SQL = SQL & " FROM corretor_tb a  WITH (NOLOCK)  , "
SQL = SQL & "      sucursal_corretor_tb b  WITH (NOLOCK)  ,"
SQL = SQL & "      ps_acerto_corretor_pj_tb c  WITH (NOLOCK)  "
SQL = SQL & " WHERE c.acerto_id = " & acerto
SQL = SQL & "   and c.pj_corretor_id = b.pj_corretor_id "
SQL = SQL & "   and b.sucursal_corretor_id = c.sucursal_corretor_id "
SQL = SQL & "   and b.pj_corretor_id = a.corretor_id"

SQL = SQL & " UNION "

SQL = SQL & " SELECT b.pf_corretor_id corretor, '0000' sucursal,'pf' tp_pessoa, a.nome, b.cpf cpf_cgc, a.endereco, a.bairro, a.cep, banco, agencia, conta_corrente, a.ddd, a.telefone, a.ddd_fax_1, a.ddd_fax_2, a.telefone_fax_1, a.telefone_fax_2, a.e_mail"
SQL = SQL & " FROM corretor_tb a  WITH (NOLOCK)  , "
SQL = SQL & "      corretor_pf_tb b  WITH (NOLOCK)  ,"
SQL = SQL & "      ps_acerto_corretor_pf_tb c  WITH (NOLOCK)  "
SQL = SQL & " WHERE c.acerto_id = " & acerto
SQL = SQL & "   and c.pf_corretor_id = b.pf_corretor_id "
SQL = SQL & "   and b.pf_corretor_id = a.corretor_id"

Set rc = rdocn.OpenResultset(SQL)

If rc.EOF Then
    MousePointer = 0
    MsgBox "Corretor informado n�o encontrado", vbCritical
    Exit Sub
Else
    corretor_id = rc!corretor
    sucursal = rc!sucursal
    tp_pessoa = rc!tp_pessoa
    nome = IIf(Not IsNull(rc!nome), rc!nome, "")
    cpf_cgc = IIf(Not IsNull(rc!cpf_cgc), rc!cpf_cgc, "")
    Endereco = IIf(Not IsNull(rc!Endereco), rc!Endereco, "")
    bairro = IIf(Not IsNull(rc!bairro), rc!bairro, "")
    Cep = IIf(Not IsNull(rc!Cep), rc!Cep, "")
    banco = IIf(Not IsNull(rc!banco), rc!banco, "")
    agencia = IIf(Not IsNull(rc!agencia), rc!agencia, "")
    conta_corrente = IIf(Not IsNull(rc!conta_corrente), rc!conta_corrente, "")
    ddd = IIf(Not IsNull(rc!ddd), rc!ddd, "")
    telefone = IIf(Not IsNull(rc!telefone), rc!telefone, "")
    ddd_fax_1 = IIf(Not IsNull(rc!ddd_fax_1), rc!ddd_fax_1, "")
    ddd_fax_2 = IIf(Not IsNull(rc!ddd_fax_2), rc!ddd_fax_2, "")
    telefone_fax_1 = IIf(Not IsNull(rc!telefone_fax_1), rc!telefone_fax_1, "")
    telefone_fax_2 = IIf(Not IsNull(rc!telefone_fax_2), rc!telefone_fax_2, "")
    e_mail = IIf(Not IsNull(rc!e_mail), rc!e_mail, "")
    HostName = cUserName
End If

rc.Close

Exit Sub

erro:
    TrataErroGeral "Obtem Corretor"
    MsgBox "Rotina: Obtem Corretor. O programa ser� abortado.", vbCritical, "Erro"
    End

End Sub


Private Sub Obtem_Estipulante(ByVal acerto As String)
Dim rc    As rdoResultset
Dim SQL   As String

On Error GoTo erro
'INICIO Migra��o SQLSERVER 2012
'SQL = "SELECT b.pj_cliente_id corretor, '0000' sucursal, 'pj' tp_pessoa, a.nome, b.cgc cpf_cgc, d.endereco, d.bairro, d.cep, banco_id, agencia, conta_corrente, a.ddd_1, a.telefone_1, a.ddd_fax_1, a.ddd_fax_2, a.fax_1, a.fax_2, a.e_mail, host_name() auxhostname"
'SQL = SQL & " FROM cliente_tb a  WITH (NOLOCK)  , "
'SQL = SQL & "      pessoa_juridica_tb b  WITH (NOLOCK)  ,"
'SQL = SQL & "      ps_acerto_estip_tb c  WITH (NOLOCK)  ,"
'SQL = SQL & "      endereco_cliente_tb d  WITH (NOLOCK)  "
'SQL = SQL & " WHERE c.acerto_id = " & acerto
'SQL = SQL & "   and c.cliente_id = b.pj_cliente_id "
'SQL = SQL & "   and b.Pj_cliente_id = a.cliente_id"
'SQL = SQL & "   and b.pj_cliente_id *= d.cliente_id"
'
'SQL = SQL & " UNION "
'
'SQL = SQL & " SELECT b.pf_cliente_id corretor, '0000' sucursal,'pf' tp_pessoa, a.nome, b.cpf cpf_cgc, d.endereco, d.bairro, d.cep, banco_id, agencia, conta_corrente, a.ddd_1, a.telefone_1, a.ddd_fax_1, a.ddd_fax_2, a.fax_1, a.fax_2, a.e_mail, host_name() auxhostname"
'SQL = SQL & " FROM cliente_tb a  WITH (NOLOCK)  , "
'SQL = SQL & "      pessoa_fisica_tb b  WITH (NOLOCK)  ,"
'SQL = SQL & "      ps_acerto_estip_tb c  WITH (NOLOCK)  ,"
'SQL = SQL & "      endereco_cliente_tb d  WITH (NOLOCK)  "
'SQL = SQL & " WHERE c.acerto_id = " & acerto
'SQL = SQL & "   and c.cliente_id = b.pf_cliente_id "
'SQL = SQL & "   and b.pf_cliente_id = a.cliente_id"
'SQL = SQL & "   and b.pf_cliente_id *= d.cliente_id"

SQL = "SELECT b.pj_cliente_id corretor, '0000' sucursal, 'pj' tp_pessoa, a.nome, b.cgc cpf_cgc, " & vbCrLf _
    & "       d.endereco, d.bairro, d.cep, banco_id, agencia, conta_corrente, " & vbCrLf _
    & "       a.ddd_1, a.telefone_1, a.ddd_fax_1, a.ddd_fax_2, a.fax_1, a.fax_2, a.e_mail, " & vbCrLf _
    & "       host_name() auxhostname " & vbCrLf _
    & "FROM cliente_tb a  WITH (NOLOCK)  " & vbCrLf _
    & "INNER JOIN pessoa_juridica_tb  b  WITH (NOLOCK)  ON b.Pj_cliente_id = a.cliente_id " & vbCrLf _
    & "INNER JOIN ps_acerto_estip_tb  c  WITH (NOLOCK)  ON c.cliente_id = b.pj_cliente_id " & vbCrLf _
    & "LEFT  JOIN endereco_cliente_tb d  WITH (NOLOCK)  ON b.pj_cliente_id = d.cliente_id " & vbCrLf _
    & "WHERE c.acerto_id = " & acerto & vbCrLf

SQL = SQL & " UNION " & vbCrLf _
    & "SELECT b.pf_cliente_id corretor, '0000' sucursal,'pf' tp_pessoa, a.nome, b.cpf cpf_cgc, " & vbCrLf _
    & "       d.endereco, d.bairro, d.cep, banco_id, agencia, conta_corrente, " & vbCrLf _
    & "       a.ddd_1, a.telefone_1, a.ddd_fax_1, a.ddd_fax_2, a.fax_1, a.fax_2, a.e_mail, " & vbCrLf _
    & "       host_name() auxhostname " & vbCrLf _
    & "FROM cliente_tb a  WITH (NOLOCK)  " & vbCrLf _
    & "INNER JOIN pessoa_fisica_tb    b  WITH (NOLOCK)  ON b.Pf_cliente_id = a.cliente_id " & vbCrLf _
    & "INNER JOIN ps_acerto_estip_tb  c  WITH (NOLOCK)  ON c.cliente_id = b.pf_cliente_id " & vbCrLf _
    & "LEFT  JOIN endereco_cliente_tb d  WITH (NOLOCK)  ON b.pf_cliente_id = d.cliente_id " & vbCrLf _
    & "WHERE c.acerto_id = " & acerto & vbCrLf
'FIM    Migra��o SQLSERVER 2012


Set rc = rdocn.OpenResultset(SQL)

If rc.EOF Then
    MousePointer = 0
    MsgBox "Corretor informado n�o encontrado", vbCritical
    Exit Sub
Else
    corretor_id = rc!corretor
    sucursal = rc!sucursal
    tp_pessoa = rc!tp_pessoa
    nome = IIf(Not IsNull(rc!nome), rc!nome, "")
    cpf_cgc = IIf(Not IsNull(rc!cpf_cgc), rc!cpf_cgc, "")
    Endereco = IIf(Not IsNull(rc!Endereco), rc!Endereco, "")
    bairro = IIf(Not IsNull(rc!bairro), rc!bairro, "")
    Cep = IIf(Not IsNull(rc!Cep), rc!Cep, "")
    banco = IIf(Not IsNull(rc!banco_id), rc!banco_id, "")
    agencia = IIf(Not IsNull(rc!agencia), rc!agencia, "")
    conta_corrente = IIf(Not IsNull(rc!conta_corrente), rc!conta_corrente, "")
    ddd = IIf(Not IsNull(rc!ddd_1), rc!ddd_1, "")
    telefone = IIf(Not IsNull(rc!telefone_1), rc!telefone_1, "")
    ddd_fax_1 = IIf(Not IsNull(rc!ddd_fax_1), rc!ddd_fax_1, "")
    ddd_fax_2 = IIf(Not IsNull(rc!ddd_fax_2), rc!ddd_fax_2, "")
    telefone_fax_1 = IIf(Not IsNull(rc!fax_1), rc!fax_1, "")
    telefone_fax_2 = IIf(Not IsNull(rc!fax_2), rc!fax_2, "")
    e_mail = IIf(Not IsNull(rc!e_mail), rc!e_mail, "")
    HostName = rc!auxhostname
End If

rc.Close

Exit Sub

erro:
    TrataErroGeral "Obtem Estipulante"
    MsgBox "Rotina: Obtem Estipulante. O programa ser� abortado.", vbCritical, "Erro"
    End

End Sub

'PAULO PELEGRINI - 18958788 - AJUSTE NA GERA��O DO EXTRATO DE PAGAMENTO A CORRETORES (INI)
Private Sub optTodos_Click()
    cmdImpressao.Enabled = False
    Monta_Grid_Pagamentos True, False
End Sub

Private Sub optRetornados_Click()
    cmdImpressao.Enabled = True
    Monta_Grid_Pagamentos True, True
End Sub
'PAULO PELEGRINI - 18958788 - AJUSTE NA GERA��O DO EXTRATO DE PAGAMENTO A CORRETORES (FIM)
