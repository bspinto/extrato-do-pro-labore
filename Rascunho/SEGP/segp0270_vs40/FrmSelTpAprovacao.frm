VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmSelTpAprovacao 
   Caption         =   "SEG07004 - Seleciona Tipo de Aprova��o - "
   ClientHeight    =   3570
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6345
   LinkTopic       =   "Form1"
   ScaleHeight     =   3570
   ScaleWidth      =   6345
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   7
      Top             =   3240
      Width           =   6345
      _ExtentX        =   11192
      _ExtentY        =   582
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Frame frameApr 
      Caption         =   "Aprovados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   300
      TabIndex        =   4
      Top             =   2490
      Width           =   2295
      Begin VB.OptionButton optNao 
         Caption         =   "N�o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1320
         TabIndex        =   6
         Top             =   240
         Width           =   855
      End
      Begin VB.OptionButton OptSim 
         Caption         =   "Sim"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4740
      TabIndex        =   3
      Top             =   2730
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3180
      TabIndex        =   2
      Top             =   2730
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Height          =   2475
      Left            =   300
      TabIndex        =   0
      Top             =   0
      Width           =   5655
      Begin VB.ListBox lstTpAprovacao 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1980
         ItemData        =   "FrmSelTpAprovacao.frx":0000
         Left            =   360
         List            =   "FrmSelTpAprovacao.frx":001C
         TabIndex        =   1
         Top             =   240
         Width           =   4935
      End
   End
End
Attribute VB_Name = "FrmSelTpAprovacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const TP_AP_CORR = 0
Const TP_AP_ESTIP = 1
Const TP_AP_REST = 2
Const TP_AP_IOF = 3
Const TP_AP_SIN = 4
Const TP_AP_RET = 5
Const TP_AP_PRORET = 6
'mathayde 24/07/2009 demanda : 654224
'Vari�vel para sele��o de situa��o (aprovado ou n�o aprovado)
Const TP_AP_PRO_CARTAO = 7
Const TP_AP_PRO_ASSITENCIA = 8    'Gabriel C�mara - 17082374 - IPrem - Custo Assist�ncia

Private Sub cmdOk_Click()

    If lstTpAprovacao.ListIndex = -1 Then
        MsgBox "Seleciona o tipo de aprova��o"
        Exit Sub
    End If

    '-- (IN�CIO) -- RSOUZA -- 27/09/2010 ----------------------'
    'mathayde
    'If bUsarConexaoABS And rdocn.name <> rdocn_Seg.name Then
    '    Set rdocn = rdocn_Seg
    'End If
    '-- (FIM) -- RSOUZA -- 27/09/2010    ----------------------'

    TpAprovacao = lstTpAprovacao.Text

    If optNao.Value Then
        flagAprovados = False
    Else
        flagAprovados = True
    End If

    If glAmbiente_id = 2 Or glAmbiente_id = 3 Then
        SQL = "select * "
        SQL = SQL & "from "
        SQL = SQL & "segab_db.DBO.usuario_unidade_tb AS usuario_unidade_tb WITH (NOLOCK) "
        SQL = SQL & "INNER Join segab_db.DBO.usuario_tb AS usuario_tb WITH (NOLOCK) "
        SQL = SQL & "on usuario_unidade_tb.usuario_id = usuario_tb.usuario_id "
        SQL = SQL & "and usuario_unidade_tb.unidade_id = usuario_tb.unidade_id "
        SQL = SQL & "where "
        SQL = SQL & "usuario_unidade_tb.unidade_id IN (3911, 893, 6, 21) "    'GEFIN/SUFIN/DIFIN/GEMPE - GENESCO SILVA - CONFITEC SP - FLOW 14268220
        SQL = SQL & "and (usuario_tb.cpf = '" & cUserName & "' OR usuario_tb.login_rede = '" & cUserName & "') "
    ElseIf glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        SQL = "select * "
        SQL = SQL & "from ABSS.segab_db.DBO.usuario_unidade_tb AS usuario_unidade_tb WITH (NOLOCK) "
        SQL = SQL & "INNER Join ABSS.segab_db.DBO.usuario_tb AS usuario_tb WITH (NOLOCK) "
        SQL = SQL & "on usuario_unidade_tb.usuario_id = usuario_tb.usuario_id "
        SQL = SQL & "and usuario_unidade_tb.unidade_id = usuario_tb.unidade_id "
        SQL = SQL & "where "
        SQL = SQL & "usuario_unidade_tb.unidade_id IN (3911, 893, 6, 21) "    'GEFIN/SUFIN/DIFIN/GEMPE - GENESCO SILVA - CONFITEC SP - FLOW 14268220
        SQL = SQL & "and (usuario_tb.cpf = '" & cUserName & "' OR usuario_tb.login_rede = '" & cUserName & "') "
    Else
        SQL = "select * from segab_db..usuario_unidade_tb usuario_unidade_tb  WITH (NOLOCK)  "
        SQL = SQL & " Join segab_db..usuario_tb usuario_tb   WITH (NOLOCK)  "
        SQL = SQL & " on usuario_unidade_tb.usuario_id = usuario_tb.usuario_id"
        SQL = SQL & " and usuario_unidade_tb.unidade_id = usuario_tb.unidade_id"
        SQL = SQL & " where usuario_unidade_tb.unidade_id IN (3911,893,6,21) "    'GEFIN/SUFIN/DIFIN/GEMPE - GENESCO SILVA - CONFITEC SP - FLOW 14268220
        SQL = SQL & " and (usuario_tb.cpf = " & "'" & cUserName & "' OR usuario_tb.login_rede = " & "'" & cUserName & "')"
    End If
    
'    SQL = "select * from segab_db..usuario_unidade_tb usuario_unidade_tb  WITH (NOLOCK)  "
'    SQL = SQL & " Join segab_db..usuario_tb usuario_tb   WITH (NOLOCK)  "
'    SQL = SQL & " on usuario_unidade_tb.usuario_id = usuario_tb.usuario_id"
'    SQL = SQL & " and usuario_unidade_tb.unidade_id = usuario_tb.unidade_id"
'    SQL = SQL & " where usuario_unidade_tb.unidade_id IN (3911,893,6,21) "    'GEFIN/SUFIN/DIFIN/GEMPE - GENESCO SILVA - CONFITEC SP - FLOW 14268220
'    SQL = SQL & " and (usuario_tb.cpf = " & "'" & cUserName & "' OR usuario_tb.login_rede = " & "'" & cUserName & "')"
    
    Set rc1 = rdoCn1.OpenResultset(SQL)

    If rc1.EOF Then
        flagPerfil = False
    Else
        flagPerfil = True
    End If
    
    rc1.Close


    '''
    '''flagPerfil = True
    ''''claudia.araujo - cria��o de perfil para travar aprova��o para os tipos de pagamentos abaixo para GEMPE - 12/07/11
    '''If UCase(TpAprovacao) = "CORRETAGEM N�O-RETIDA" Or UCase(TpAprovacao) = "PRO-LABORE N�O-RETIDO" Then
    '''   SQL = "select * from segab_db..usuario_unidade_tb usuario_unidade_tb"
    '''   SQL = SQL & " Join segab_db..usuario_tb usuario_tb "
    '''   SQL = SQL & " on usuario_unidade_tb.usuario_id = usuario_tb.usuario_id"
    '''   SQL = SQL & " and usuario_unidade_tb.unidade_id = usuario_tb.unidade_id"
    '''   SQL = SQL & " where usuario_unidade_tb.unidade_id NOT IN (3911,893,6) " 'GEFIN/SUFIN/DIFIN
    '''   SQL = SQL & " and (usuario_tb.cpf = " & "'" & cUserName & "' OR usuario_tb.login_rede = " & "'" & cUserName & "')"
    '''   Set rc1 = rdoCn1.OpenResultset(SQL)
    '''   If Not rc1.EOF Then
    '''      flagPerfil = False
    '''   End If
    '''   rc1.Close
    '''End If

    '--[ Demanda 18721099 ]---- Cl�udia Eduarda - Confitec 25/05/2015 -------------------------------------
    If UCase(TpAprovacao) = "CORRETAGEM N�O-RETIDA" Or UCase(TpAprovacao) = "PRO-LABORE N�O-RETIDO" Then
        frmSelPagamentos.Show vbModal, Me
    Else
        frmAprPagamentos.Show vbModal, Me
    End If

End Sub

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub Form_GotFocus()
    If Me.Visible Then Me.Refresh
End Sub

Private Sub Form_Load()

    Me.Caption = "SEG20270 - Seleciona Tipo de Aprova��o - " & Ambiente

    optNao.Value = True
    frameApr.Visible = False

    If glAmbiente_id = 2 Or glAmbiente_id = 3 Then
        SQL = "select * "
        SQL = SQL & "from segab_db.DBO.usuario_unidade_tb AS usuario_unidade_tb WITH (NOLOCK) "
        SQL = SQL & "INNER Join segab_db.DBO.usuario_tb AS usuario_tb WITH (NOLOCK) "
        SQL = SQL & "on usuario_unidade_tb.usuario_id = usuario_tb.usuario_id "
        SQL = SQL & "and usuario_unidade_tb.unidade_id = usuario_tb.unidade_id "
        SQL = SQL & "where "
        SQL = SQL & "usuario_unidade_tb.unidade_id IN (3911, 893, 6, 21) "    'GEFIN/SUFIN/DIFIN/GEMPE - GENESCO SILVA - CONFITEC SP - FLOW 14268220
        SQL = SQL & "and (usuario_tb.cpf = '" & cUserName & "' OR usuario_tb.login_rede = '" & cUserName & "') "
    ElseIf glAmbiente_id = 6 Or glAmbiente_id = 7 Then
        SQL = "select * "
        SQL = SQL & "from ABSS.segab_db.DBO.usuario_unidade_tb AS usuario_unidade_tb WITH (NOLOCK) "
        SQL = SQL & "INNER Join ABSS.segab_db.DBO.usuario_tb AS usuario_tb WITH (NOLOCK) "
        SQL = SQL & "on usuario_unidade_tb.usuario_id = usuario_tb.usuario_id "
        SQL = SQL & "and usuario_unidade_tb.unidade_id = usuario_tb.unidade_id "
        SQL = SQL & "where "
        SQL = SQL & "usuario_unidade_tb.unidade_id IN (3911, 893, 6, 21) "    'GEFIN/SUFIN/DIFIN/GEMPE - GENESCO SILVA - CONFITEC SP - FLOW 14268220
        SQL = SQL & "and (usuario_tb.cpf = '" & cUserName & "' OR usuario_tb.login_rede = '" & cUserName & "') "
    Else
        SQL = "select * from segab_db..usuario_unidade_tb usuario_unidade_tb  WITH (NOLOCK) "
        SQL = SQL & " Join segab_db..usuario_tb usuario_tb   WITH (NOLOCK)  "
        SQL = SQL & " on usuario_unidade_tb.usuario_id = usuario_tb.usuario_id"
        SQL = SQL & " and usuario_unidade_tb.unidade_id = usuario_tb.unidade_id"
        SQL = SQL & " where usuario_unidade_tb.unidade_id IN (3911,893,6,21) "    'GEFIN/SUFIN/DIFIN/GEMPE - GENESCO SILVA - CONFITEC SP - FLOW 14268220
        SQL = SQL & " and (usuario_tb.cpf = " & "'" & cUserName & "' OR usuario_tb.login_rede = " & "'" & cUserName & "')"
    End If

'    SQL = "select * from segab_db..usuario_unidade_tb usuario_unidade_tb  WITH (NOLOCK)  "
'    SQL = SQL & " Join segab_db..usuario_tb usuario_tb   WITH (NOLOCK)  "
'    SQL = SQL & " on usuario_unidade_tb.usuario_id = usuario_tb.usuario_id"
'    SQL = SQL & " and usuario_unidade_tb.unidade_id = usuario_tb.unidade_id"
'    SQL = SQL & " where usuario_unidade_tb.unidade_id IN (3911,893,6,21) "    'GEFIN/SUFIN/DIFIN/GEMPE - GENESCO SILVA - CONFITEC SP - FLOW 14268220
'    SQL = SQL & " and (usuario_tb.cpf = " & "'" & cUserName & "' OR usuario_tb.login_rede = " & "'" & cUserName & "')"
    
    Set rc1 = rdoCn1.OpenResultset(SQL)

    If rc1.EOF And glAmbiente_id <> 3 Then
        lstTpAprovacao.Clear
        lstTpAprovacao.AddItem ("Corretagem N�o-Retida")
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        lstTpAprovacao.AddItem ("Pro-labore N�o-Retido")
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        
        flagPerfil = False
    Else
        lstTpAprovacao.Clear
        lstTpAprovacao.AddItem ("Corretagem N�o-Retida")
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        lstTpAprovacao.AddItem ("Pro-labore N�o-Retido")
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        lstTpAprovacao.AddItem ("Restitui��o")
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        lstTpAprovacao.AddItem ("IOF(ainda n�o retido)")
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        lstTpAprovacao.AddItem ("Sinistro")
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        lstTpAprovacao.AddItem ("Comiss�o Retida")
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        lstTpAprovacao.AddItem ("Pr�-labore Retido")
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        lstTpAprovacao.AddItem ("Comiss�o Retida Cart�o de Cr�dito")
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        lstTpAprovacao.AddItem ("Custo Assist�ncia")    'Gabriel C�mara - 17082374 - IPrem - Custo Assist�ncia
        lstTpAprovacao.ItemData(lstTpAprovacao.NewIndex) = 0
        
        flagPerfil = True
    End If
    
    rc1.Close

End Sub

Private Sub lstTpAprovacao_Click()

    If lstTpAprovacao.ListIndex = TP_AP_CORR _
       Or lstTpAprovacao.ListIndex = TP_AP_REST _
       Or lstTpAprovacao.ListIndex = TP_AP_ESTIP _
       Or lstTpAprovacao.ListIndex = TP_AP_AGE _
       Or lstTpAprovacao.ListIndex = TP_AP_RET _
       Or lstTpAprovacao.ListIndex = TP_AP_PRORET _
       Or lstTpAprovacao.ListIndex = TP_AP_PRO_CARTAO _
       Or lstTpAprovacao.ListIndex = TP_AP_PRO_ASSITENCIA Then  'Gabriel C�mara - 17082374 - IPrem - Custo Assist�ncia
        frameApr.Visible = True
    Else
        frameApr.Visible = False
    End If

End Sub
