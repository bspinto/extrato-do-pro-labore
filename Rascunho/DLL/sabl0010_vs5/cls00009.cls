VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls00009"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private ConexaoADO   As New ADODB.Connection

Private Sub Conecta()
On Local Error GoTo ErroConexao
  
  If Not InicializaVariaveisConexao Then
    Err.Raise 13998, "Ambiente.dll", "Erro ao tentar abrir o arquivo: config.ini"
  End If
  'Conecta no banco de Ambiente
  With ConexaoADO
    .ConnectionString = "driver={SQL Server};server=" & db_ServerName & ";uid=" & db_UserName & ";pwd=" & db_UserPass & ";database=" & db_Name
    .ConnectionString = "driver={SQL Server};server=" & "VBR008002-016\QUALID" & ";uid=" & "SISBR" & ";pwd=" & "SISBR_QLD" & ";database=" & db_Name
    .ConnectionTimeout = 30000
    .Open
  End With
  Exit Sub

ErroConexao:
    Err.Raise 13999, "Ambiente.dll", "Erro ao tentar conectar no banco: " & db_Name & " - " & ConexaoADO.ConnectionString & " - " & ArquivoINI & " - " & Err.Number & " - " & Err.Description

End Sub

Private Sub Class_Initialize()
  Conecta
End Sub

Private Sub Class_Terminate()
  Desconecta
End Sub
Private Sub Desconecta()
  'fecha conex�o com o Banco de Dados
  ConexaoADO.Close
End Sub

Public Function ExecutaAmbiente(Sql As String) As Object
Dim Cmd As New ADODB.Command

On Local Error GoTo ErroExecutaAmbiente
  'Cria o Comando ADO
  Cmd.CommandType = adCmdText
  Cmd.CommandText = Sql
  Cmd.ActiveConnection = ConexaoADO
  'devolve o recordset na forma de objeto
  Set ExecutaAmbiente = Cmd.Execute
  
  Exit Function
  
ErroExecutaAmbiente:

  ErroSQLAmbiente
  
End Function
Private Sub ErroSQLAmbiente()
Dim Descricao As String
Dim I         As Integer
  
'Varre cole��o de erros do ADO
While I < ConexaoADO.Errors.Count
  'Concatena erros
  Inc Descricao, " " & ConexaoADO.Errors(I).Number
  Inc Descricao, " - "
  Inc Descricao, ConexaoADO.Errors(I).Description & vbNewLine
  Inc I
  
Wend
  'devolve erros para a inst�ncia
  Err.Raise 13997, "Ambiente.dll", Descricao
  
End Sub

Public Function RetornaValorAmbiente(ByVal Sistema As String, ByVal Secao As String, ByVal Campo As String, ByVal Chave As String, Optional ByVal Ambiente_id As Long = 0) As String
  On Local Error GoTo TrataErro
  Dim Cmd As New ADODB.Command
  Dim Rec As New ADODB.Recordset
  Dim Sql As String
    
  'Prepara Query
  Sql = "SELECT valor, cifrado" & vbNewLine
  Inc Sql, "FROM parametro_tb" & vbNewLine
  Inc Sql, "WHERE sigla_sistema = " & FormatoSQL(Sistema) & vbNewLine
  Inc Sql, "  AND secao         = " & FormatoSQL(Secao) & vbNewLine
  Inc Sql, "  AND campo         = " & FormatoSQL(Campo)
  If Ambiente_id <> 0 Then
    If (Ambiente_id = 4 Or Ambiente_id = 8) And (Campo = "LOCAL_PATH" Or Campo = "PRODUCAO_PATH") Then
    Inc Sql, "  AND ambiente_id   = 3"
    Else
    Inc Sql, "  AND ambiente_id   = " & FormatoSQL(Ambiente_id)
    End If
  End If
  
  'Cria o comando ADO
  Cmd.CommandType = adCmdText
  Cmd.CommandText = Sql
  Cmd.ActiveConnection = ConexaoADO
  
  'Recebe o recordset
  Set Rec = Cmd.Execute
  
  If Rec.EOF Then
    RetornaValorAmbiente = ""
  Else
    'verifica se o campo est� cifrado
    If Logico(Rec("cifrado")) Then
      'decifra a informa��o
      RetornaValorAmbiente = Decifra(Rec("valor"), Chave)
    Else
      RetornaValorAmbiente = Rec("valor")
      If (Ambiente_id = 4 Or Ambiente_id = 8) And Campo = "LOCAL_PATH" Then
        RetornaValorAmbiente = Replace(RetornaValorAmbiente, "QLD", "DES")
      End If
      
    End If
  End If
   'Fabio moretti 16/10/2013 fiza o dominio que o usuario esta logado na maquina
    If UCase(Campo) = UCase("Dominio") Then
        RetornaValorAmbiente = UCase(Environ("USERDOMAIN"))
    End If
  Exit Function
TrataErro:
  
  ErroSQLAmbiente
  
End Function
 
Public Function GetArquivoIni() As String
  GetArquivoIni = ArquivoINI
End Function

Public Function RetornaConexao() As Object

  Set RetornaConexao = ConexaoADO

End Function
