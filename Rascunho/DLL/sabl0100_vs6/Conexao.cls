VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 1  'NoTransaction
END
Attribute VB_Name = "Conexao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private ErrCode As Variant
Private WithEvents Rs As ADODB.Recordset
Attribute Rs.VB_VarHelpID = -1
Private Declare Function GetWindowsDirectory& Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long)
Private ErrDesc As String
Private vProgress As Long
Private vMaxProgress As Long
Private vComplete As Boolean
Private vErros As New Erros
Dim eTpIni As String
Dim eTpFim As String
Private Function ValidaAmbiente(ByVal eAmbiente As String) As Boolean

On Error GoTo ErrValidaAmbiente:
  If UCase(eAmbiente) = "PRODUCAO" Or UCase(eAmbiente) = "PRODUCAO_ABS" Then
     ValidaAmbiente = True
  Else
     ValidaAmbiente = False
  End If
Exit Function

ErrValidaAmbiente:

End Function
Private Function ValidaVersao(ByVal Sigla_Recurso As String, _
                              ByVal eChave As Long, _
                              ByVal eDescricaoArquivo As String) As Boolean
'On Error GoTo ErrValidaVersao:
'Dim obj0016 As New cls000016
'Dim RetSenha As String
'Dim ReSSenha As String
'Dim eSQL As String
'Dim RsRte As New ADODB.Recordset

'eSQL = "Set NoCount on Exec segab_db.dbo.Retorna_Versao_recurso_sps '" & Sigla_Recurso & "'"
'Set RsRte = RecordSet_Auxiliar("SEGAB", "PRODUCAO", eSQL, eChave)
'ReSSenha = LTrim(RsRte.Fields("Senha"))
'
'If Err = 0 And RsRte.State = 1 Then
'   Dim ObjPass As New cls000016
'   RetSenha = ObjPass.AbrePassword(ReSSenha, eChave)
'   Set ObjPass = Nothing
'   If CBool(eDescricaoArquivo = RetSenha) = True Then
      ValidaVersao = True
'   Else
'      ValidaVersao = False
'   End If
'Else
'   GoTo ErrValidaVersao:
'End If
'Exit Function
'ErrValidaVersao:
End Function
Public Function AbrirTransacao(ByVal ICont As Long) As Boolean
AbrirTransacao = False
    On Error GoTo ErrBeginTrans

         If vArrayCox(ICont).State = 0 Then GoTo ErrBeginTrans
            vArrayCox(ICont).BeginTrans
         If vArrayCox(ICont).State = 0 Then GoTo ErrBeginTrans
            AbrirTransacao = True

    Exit Function
ErrBeginTrans:
      AbrirTransacao = False
      ErrCode = Err.Number
      ErrDesc = Err.Description
End Function
Private Function ObterProvider(ByVal eSistema As String, ByVal eAmbiente As String, ByRef eConexao As ADODB.Connection, Optional ByVal eTimeOut As Integer = 30000) As ADODB.Recordset
    Dim ObjRs As Object
    Dim lLogContinue As Boolean
    Dim StatusCode As Integer
    Dim StatusString As String
    Dim sql As String
    Const lcIntNoErrors = 0
    Set ObjRs = CreateObject("ADODB.Recordset")
    If Err = lcIntNoErrors Then
        lLogContinue = True
    Else
        StatusCode = Err
        StatusString = Err.Description
    End If
    On Error GoTo 0
    sql = "Set NoCount on Exec Controle_Sistema_db.dbo.obter_parametro_conexao2_sps " & _
          "'" & eSistema & "'," & _
          "'" & eAmbiente & "'"
    
    If lLogContinue = True Then
       On Error Resume Next
          
          ObjRs.CursorType = adOpenForwardOnly
          ObjRs.LockType = adLockReadOnly
          ObjRs.CursorLocation = adUseClient
          Set ObjRs.ActiveConnection = eConexao
          Set ObjRs = eConexao.Execute(sql)
       
       If ObjRs Is Nothing Then
            StatusCode = Err
            StatusString = Err.Description
       Else
            If Err = lcIntNoErrors And ObjRs.State = 1 Then
                 'Sucesso
               StatusCode = 0
               Set ObterProvider = CreateObject("ADODB.Recordset")
               Set ObterProvider = ObjRs
               Set ObjRs = Nothing
               Set eConexao = Nothing

            Else
               StatusCode = Err
               StatusString = Err.Description
              ' Set ObjCx_Cx = Nothing
              'ObterConexao = False

            End If
        End If
    End If
                     
End Function
Public Property Get AbrirConexao(ByVal eSistema As String, _
                                      ByVal eAmbiente As String, _
                                      ByVal eSigla_Recurso As String, _
                                      ByVal eChave As Long, _
                                      ByVal eDescricaoArquivo As String) As Long


On Error GoTo ErrRetornaID_Conexao
Dim ICont As Integer
Dim ObjRs_Parametros As Variant
Dim StatusString As String
Dim ObjRs_Auxiliar As New ADODB.Recordset
Dim ObjCx_Execucao As New ADODB.Connection


Select Case ValidaAmbiente(eAmbiente)
Case True 'ACESSANDO A PRODUCAO
     'If ValidaVersao(eSigla_Recurso, eChave, eDescricaoArquivo) = True Then
        ICont = 0
        Do While vArrayCox(ICont).State = 1
           ICont = ICont + 1
        Loop
        
        
        Set vArrayCox(ICont) = New ADODB.Connection
        
        If (eSistema <> "SEGBR" And eSistema <> "DIG" And eSistema <> "SCI" And eSistema <> "SMQ") Then
            Set ObjRs_Parametros = ObterConexao(eSistema, eAmbiente, eChave)
            Set vArrayCox(ICont) = ObterConexao(eSistema, eAmbiente, eChave, 1, , ObjRs_Parametros)
        Else
            Set vArrayCox(ICont) = ObterConexao(eSistema, eAmbiente, eChave, 1)
        End If
        
        If vArrayCox(ICont).State = 0 And Err <> 0 Then _
                                          GoTo ErrRetornaID_Conexao:
        AbrirConexao = ICont
     
     'End If
Case False ' ACESSANDO A QUALIDADE
        ICont = 0
        Do While vArrayCox(ICont).State = 1
           ICont = ICont + 1
        Loop
        
        Set ObjRs_Parametros = ObterConexao(eSistema, eAmbiente, eChave)
        Set vArrayCox(ICont) = New ADODB.Connection
        Set vArrayCox(ICont) = ObterConexao(eSistema, eAmbiente, eChave, 1, , ObjRs_Parametros)

        If vArrayCox(ICont).State = 0 And Err <> 0 Then _
                                          GoTo ErrRetornaID_Conexao:
        AbrirConexao = ICont
End Select
     
Exit Property
ErrRetornaID_Conexao:

AbrirConexao = -1

End Property
Private Property Get Retorna_ArrayCx(ByVal ICont As Long) As Variant


On Error GoTo ErrRetorna_ArrayCx
 If vArrayCox(ICont).State = 1 Then
        
        Set Retorna_ArrayCx = New ADODB.Connection
        Set Retorna_ArrayCx = vArrayCox(ICont)

        If Retorna_ArrayCx.State = 0 And Err <> 0 Then _
           GoTo ErrRetorna_ArrayCx:
Else
        Set Retorna_ArrayCx = Nothing
        
End If
      
Exit Property
ErrRetorna_ArrayCx:

End Property
Private Function ObterConexao(ByVal eSistema As String, _
                              ByVal eAmbiente As String, _
                              ByVal eChave As String, _
                              Optional ByVal eTipo As Integer = 0, _
                              Optional ByVal eCursorLocation As Integer = adUseClient, _
                              Optional ByVal eRecorSet As ADODB.Recordset, _
                              Optional ByVal eTimeOut As Integer = 30000) _
                              As Variant
                              
On Error GoTo AbrirConexaoErr

Const lcIntNoErrors = 0
Dim ObjCx_Cx As Object
Dim ObjRs_Cx As Object
Dim lLogContinue As Boolean
Dim sql As String
Dim Provider As String
Dim StatusString As String
Dim StatusCode As Long
Dim ObjPass As New cls000016
On Error GoTo AbrirConexaoErr

    Set ObjCx_Cx = CreateObject("ADODB.Connection")
    If Err = lcIntNoErrors Then
        lLogContinue = True
    Else
        StatusCode = Err
        StatusString = Err.Description
    End If
    On Error GoTo 0

    If lLogContinue = True Then
       On Error Resume Next
     If eTipo = 0 Then
        If UCase(eAmbiente) = "PRODUCAO" Or UCase(eAmbiente) = "PRODUCAO_ABS" Then
            ObjCx_Cx.ConnectionString = ConexaoTexto()
        Else
            'implementado dessa forma para n�o haver necessidade de troca ou inclus�o de arquivos nas maquinas dos usu�rios.
            ObjCx_Cx.ConnectionString = ObterConexaoFixo("SEGBR", "QUALIDADE", "SISBB")
        End If
     Else
     'eAmbiente = Ambiente(eAmbiente)
        If UCase(eAmbiente) = "PRODUCAO" Or UCase(eAmbiente) = "PRODUCAO_ABS" Then
        
            If Not eRecorSet Is Nothing Then
              ObjCx_Cx.ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;" & _
                                          "User ID=" & eRecorSet!usuario & _
                                          ";pwd=" & ObjPass.AbrePassword(eRecorSet!senha, eChave) & _
                                          ";Initial Catalog=" & eRecorSet!banco & _
                                          ";Data Source=" & eRecorSet!servidor
                     
             Set ObjPass = Nothing
            Else
            
                ObjCx_Cx.ConnectionString = ObterConexaoFixo(eSistema, eAmbiente, eChave)
            
            End If
        Else
            If Not eRecorSet Is Nothing Then
                            ObjCx_Cx.ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;" & _
                                      "User ID=" & eRecorSet!usuario & _
                                      ";pwd=" & eRecorSet!senha & _
                                      ";Initial Catalog=" & eRecorSet!banco & _
                                      ";Data Source=" & eRecorSet!servidor
            Else
            
                ObjCx_Cx.ConnectionString = ObterConexaoFixo(eSistema, eAmbiente, eChave)
            
            End If
        End If
     End If
       ObjCx_Cx.ConnectionTimeout = 30000 'eTimeOut
       ObjCx_Cx.CommandTimeout = 0
       ObjCx_Cx.CursorLocation = adUseClient
       ObjCx_Cx.Open
       If ObjCx_Cx Is Nothing Or ObjCx_Cx.State = 0 Then
            StatusCode = Err
            StatusString = Err.Description
      
       Else
            If Err = lcIntNoErrors And ObjCx_Cx.State = 1 Then
                If eTipo = 0 Then
                    Set ObterConexao = CreateObject("ADODB.Recordset")
                    ObterConexao.CursorLocation = adUseClient
                    Set ObterConexao = ObterProvider(eSistema, eAmbiente, ObjCx_Cx)
                Else
                    Set ObterConexao = Nothing
                    Set ObterConexao = CreateObject("ADODB.Connection")
                    ObterConexao.CursorLocation = adUseClient
                    ObterConexao.ConnectionTimeout = 30000
                    Set ObterConexao = ObjCx_Cx
                    Set ObjCx_Cx = Nothing
                End If
                If Err = lcIntNoErrors And ObterConexao.State = 1 Then

                Else
                  StatusCode = Err
                  StatusString = Err.Description
                  Set ObjCx_Cx = Nothing
                  ObterConexao = False

                
                End If
    On Error GoTo 0
            Else
               StatusCode = Err
               StatusString = Err.Description
               Set ObjCx_Cx = Nothing
               ObterConexao = False

            End If
        End If
    End If
    Set ObjCx_Cx = Nothing
Exit Function
AbrirConexaoErr:
      ErrCode = Err.Number
      ErrDesc = Err.Description
  '  Call RaiseError(MyUnhandledError, "TransacaoBanco:AbrirConexao Method")
End Function
Public Function _
ExecutarSQL(ByVal eSistema As String, _
            ByVal eAmbiente As String, _
            ByVal eSigla_Recurso As String, _
            ByVal eDescricaoArquivo As String, _
            ByVal eSQL As String, _
            ByVal NunCx As Long, _
            ByVal eChave As Long, _
   Optional ByVal eRetRecordSet As Boolean = True, _
   Optional ByVal eExecASincrona As Boolean = False, _
   Optional ByVal eTimeOut As Long = 30000, _
   Optional ByVal eLockType As LockTypeEnum = adLockReadOnly, _
   Optional ByVal eCursorLocation As CursorLocationEnum = adUseClient, _
   Optional ByVal eCursorType As CursorTypeEnum = adLockReadOnly) As Variant

On Error GoTo ErrConex:
      
      Const lcIntNoErrors = 0
      Dim ObjRs_Ret As ADODB.Recordset
      Dim ObjRs_Parametros As Variant
      Dim ObjCx_Execucao As Variant
      Dim StatusString As String

Select Case ValidaAmbiente(eAmbiente)
Case True
   If ValidaVersao(eSigla_Recurso, eChave, eDescricaoArquivo) = True Then
      Set ObjCx_Execucao = CreateObject("ADODB.Connection")
      Set ObjCx_Execucao = Retorna_ArrayCx(NunCx)
      If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
      
       'timeout
       ObjCx_Execucao.CommandTimeout = eTimeOut
      
       If eRetRecordSet = True Then
         Set ObjRs_Ret = Nothing
         Set ObjRs_Ret = CreateObject("ADODB.Recordset")
         Set ObjRs_Ret = New ADODB.Recordset
             ObjRs_Ret.CursorType = eCursorType
             ObjRs_Ret.LockType = eLockType
             ObjRs_Ret.CursorLocation = eCursorLocation
            
             If Not eExecASincrona Then
                 eTpIni = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  ObjRs_Ret.Open eSQL, ObjCx_Execucao, , adLockReadOnly, adCmdText
                 eTpFim = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                 'Trace eSQL, eSigla_Recurso, eSistema, eAmbiente, eTpIni, eTpFim
             Else
                 Set ObjRs_Ret = ObjCx_Execucao.Execute(eSQL, , adAsyncExecute)
             End If
             
             If Err = lcIntNoErrors And ObjRs_Ret.State = 1 Then
                Set ExecutarSQL = CreateObject("ADODB.Recordset")
                Set ExecutarSQL = New ADODB.Recordset
                Set ExecutarSQL = ObjRs_Ret
             Else
                  Set ObjRs_Ret = Nothing
                  Set ObjCx_Execucao = Nothing
             End If
       Else
             If eExecASincrona = True Then
                  ObjCx_Execucao.Execute eSQL, , adAsyncExecute
                '  If ObjCx_Execucao.State = 1 Then
                '     Set Rs = ObjCx_Execucao
                '     Set ObjCx_Execucao = Nothing
                '  End If
             Else
                 eTpIni = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  ObjCx_Execucao.Execute (eSQL)
                 eTpFim = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                 'Trace eSQL, eSigla_Recurso, eSistema, eAmbiente, eTpIni, eTpFim
             End If
             If Err = lcIntNoErrors Then 'And ObjCx_Execucao.State = 1 Then
                ExecutarSQL = True
             Else
                Set ObjCx_Execucao = Nothing
                ExecutarSQL = False
             End If
       End If
      End If
  Else
     'versao incorreta
  End If
Case False
      Set ObjCx_Execucao = CreateObject("ADODB.Connection")
      Set ObjCx_Execucao = Retorna_ArrayCx(NunCx)
      If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
            If eRetRecordSet = True Then
               Set ObjRs_Ret = Nothing
               Set ObjRs_Ret = CreateObject("ADODB.Recordset")
               Set ObjRs_Ret = New ADODB.Recordset
               'ObjRs_Ret.CursorLocation = adUseClient
               If eExecASincrona = True Then
                  ObjRs_Ret.Open eSQL, ObjCx_Execucao, adOpenKeyset, , adAsyncFetch
                  Set ExecutarSQL = CreateObject("ADODB.Recordset")
                  Set ExecutarSQL = New ADODB.Recordset
                  Set ExecutarSQL = ObjRs_Ret
                  Set ObjRs_Ret = Nothing
               Else
                  eTpIni = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  Set ObjRs_Ret = ObjCx_Execucao.Execute(eSQL)
                  eTpFim = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  'Trace eSQL, eSigla_Recurso, eSistema, eAmbiente, eTpIni, eTpFim
                  If Err = lcIntNoErrors And ObjRs_Ret.State = 1 Then
                     Set ExecutarSQL = CreateObject("ADODB.Recordset")
                     Set ExecutarSQL = New ADODB.Recordset
                     Set ExecutarSQL = ObjRs_Ret
                  Else
                     Set ObjRs_Ret = Nothing
                     Set ObjCx_Execucao = Nothing
                  End If
               End If
        Else
             If eExecASincrona = True Then
                  ObjCx_Execucao.Execute (eSQL), , adOpenKeyset, , adAsyncFetch
                  If ObjCx_Execucao.State = 1 Then
                     Set Rs = ObjCx_Execucao
                     Set ObjCx_Execucao = Nothing
                  End If
             Else
                  eTpIni = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  ObjCx_Execucao.Execute (eSQL), , adCmdText
                  eTpFim = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  'Trace eSQL, eSigla_Recurso, eSistema, eAmbiente, eTpIni, eTpFim
             End If
             If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
                ExecutarSQL = True
             Else
                Set ObjCx_Execucao = Nothing
                ExecutarSQL = False
             End If
        End If
       End If
End Select
Exit Function
ErrConex:
    ErrCode = Err.Number
    ErrDesc = Err.Description
    Call Err.Raise(ErrCode, , "SABL0100.Conexao.ExecutarSQL - " & ErrDesc)
End Function
Public Function ConfirmarTransacao(ByVal ICont As Long) As Boolean
    
    On Error GoTo ErrCommitTrans
      ConfirmarTransacao = False
         
         If vArrayCox(ICont).State = 0 Then GoTo ErrCommitTrans
            vArrayCox(ICont).CommitTrans
         If vArrayCox(ICont).State = 0 Then GoTo ErrCommitTrans
            ConfirmarTransacao = True

    Exit Function
ErrCommitTrans:
      ConfirmarTransacao = False
      ErrCode = Err.Number
      ErrDesc = Err.Description
End Function
Public Function RetornarTransacao(ByVal ICont As Long) As Boolean

    On Error GoTo ErrRollbackTrans
      
      RetornarTransacao = False
         
         If vArrayCox(ICont).State = 0 Then GoTo ErrRollbackTrans
            vArrayCox(ICont).RollbackTrans
         If vArrayCox(ICont).State = 0 Then GoTo ErrRollbackTrans
            RetornarTransacao = True

    Exit Function
ErrRollbackTrans:
     RetornarTransacao = False
      ErrCode = Err.Number
      ErrDesc = Err.Description
End Function
'Public Function EstadoConexao(ByVal ICont As Long) As Boolean
'
'    On Error GoTo ErrCommitTrans
'
'      If vArrayCox(ICont).State = 1 Then EstadoConexao = True Else EstadoConexao = False
'
'    Exit Function
'ErrCommitTrans:
'      ErrCode = Err.Number
'      ErrDesc = Err.Description
'End Function
Public Function EstadoConexao(ByVal ICont As Long) As ADODB.ObjectStateEnum


'adStateClosed       0
'adStateOpen         1
'adStateConnecting   2
'adStateExecuting    4
'adStateFetching     8

 

    On Error GoTo ErrCommitTrans

      EstadoConexao = vArrayCox(ICont).State
 
    Exit Function

ErrCommitTrans:
      ErrCode = Err.Number
      ErrDesc = Err.Description

End Function


Public Property Get Erros() As Erros

    On Error GoTo ErrCommitTrans
    
    Set Erros = vErros

    Exit Function
ErrCommitTrans:
      ErrCode = Err.Number
      ErrDesc = Err.Description
End Property
Private Function _
RecordSet_Auxiliar(ByVal eSistema As String, _
                   ByVal eAmbiente As String, _
                   ByVal eSQL As String, _
                   ByVal eChave As String, _
                   Optional ByVal eTimeOut As Integer = 30000) As ADODB.Recordset

Const lcIntNoErrors = 0
Dim ObjRs_Parametros As Variant
Dim StatusString As String
Dim ObjCx_Execucao As Object

Dim ObjRs_Auxiliar As New ADODB.Recordset
Set ObjCx_Execucao = CreateObject("ADODB.Connection")
Set ObjRs_Parametros = ObterConexao(eSistema, eAmbiente, eChave, 0)
 
If Err = lcIntNoErrors And ObjRs_Parametros.State = 1 Then
   Set ObjCx_Execucao = ObterConexao(eSistema, eAmbiente, eChave, 1, , ObjRs_Parametros)
   If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
      Set ObjRs_Auxiliar = ObjCx_Execucao.Execute(eSQL)
      If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
         Set RecordSet_Auxiliar = ObjRs_Auxiliar
      Else
         Set RecordSet_Auxiliar = Nothing
      End If
   End If
End If

Exit Function
ErrConex:
If Err <> lcIntNoErrors Then
      ErrCode = Err.Number
      ErrDesc = Err.Description
      Set RecordSet_Auxiliar = Nothing
End If
End Function
Private Function ConexaoTexto() As String
Dim Arquivo As String
Dim Provider As String
        Arquivo = "c:\SEGBR\MSDisk.dat"
        Open Arquivo For Input As #1
            Line Input #1, Provider
        Close #1
        ConexaoTexto = Provider
End Function
Public Property Get FecharConexao(ByVal ICont As Long) As Boolean


On Error GoTo ErrFechaCx
 If vArrayCox(ICont).State = 1 Then
        
        vArrayCox(ICont).Close
        Set vArrayCox(ICont) = Nothing

        If vArrayCox(ICont).State = 1 And Err <> 0 Then _
           GoTo ErrFechaCx:
           FecharConexao = True
Else
        FecharConexao = False
End If
      
Exit Property
ErrFechaCx:
FecharConexao = False
End Property
Public Property Get CancelarConexoes(ByVal ICont As Long) As Boolean

On Error GoTo ErrCancelaCxs

Do While ICont <> -1
   If vArrayCox(ICont).State = 1 Then
       vArrayCox(ICont).Close
       Set vArrayCox(ICont) = Nothing
       ICont = ICont - 1
   Else
       ICont = ICont - 1
   End If
Loop
 
CancelarConexoes = True

Exit Property
ErrCancelaCxs:
CancelarConexoes = False
End Property
Public Function RetornarProgressoRSetAssync() As Variant



End Function

Private Sub Rs_FetchComplete(ByVal pError As ADODB.Error, _
                                   adStatus As ADODB.EventStatusEnum, _
                             ByVal pRecordSet As ADODB.Recordset)
vComplete = False
If adStatus = adStatusOK Then
   vComplete = True
Else
   vComplete = False
End If

End Sub
Private Sub Rs_FetchProgress(ByVal Progress As Long, _
                             ByVal MaxProgress As Long, _
                                   adStatus As ADODB.EventStatusEnum, _
                             ByVal pRecordSet As ADODB.Recordset)
      
      vMaxProgress = 0
      vProgress = 0
      vMaxProgress = MaxProgress
      vProgress = Progress
        
End Sub
Private Function _
       Trace _
       (ByVal sProcedure As String, _
        ByVal sSigla_Recurso As String, _
        ByVal sSistema As String, _
        ByVal sAmbiente As String, _
        ByVal sTi As String, _
        ByVal sTf As String)

        Dim eObjCx_Trace As New ADODB.Connection
        Dim eObjRS_Trace As New ADODB.Recordset
        Dim ObjWN As Object
        Dim sql As String
        
        eObjCx_Trace = ConexaoTexto()
        eObjCx_Trace.Open
             
        If eObjCx_Trace.State = 1 Then Else GoTo ErrTrace
             Set eObjRS_Trace = eObjCx_Trace.Execute _
             ("Set NoCount on Exec segab_db.dbo.Retorna_Versao_recurso_sps '" & sSigla_Recurso & "'")
        If eObjRS_Trace.State = 1 Then Else GoTo ErrTrace

       
        Select Case eObjRS_Trace!Tipo
            Case 1 'HostName
                 Set ObjWN = CreateObject("Wscript.Network")
                 If UCase(eObjRS_Trace!Valor) = UCase(ObjWN.computerName) Then
                         sql = "Set NoCount On Exec controle_sistema_db..Inclui_Trace_Procedure_spi '" & _
                                Mid(sProcedure, 1, InStr(sProcedure, "'")) & ",'" & _
                                sSigla_Recurso & "','" & _
                                ObjWN.UserName & "','" & _
                                sSistema & "','" & _
                                sAmbiente & "','" & _
                                ObjWN.computerName & "','" & _
                                sTi & "','" & _
                                sTf & "'"
                                eObjCx_Trace.Execute sql, , adAsyncExecute
                 End If
            Case 2 'Login Rede
                 Set ObjWN = CreateObject("Wscript.Network")
                 If UCase(eObjRS_Trace!Valor) = UCase(ObjWN.UserName) Then
                         sql = "Set NoCount On Exec controle_sistema_db..Inclui_Trace_Procedure_spi '" & _
                                Mid(sProcedure, 1, InStr(sProcedure, "'")) & ",'" & _
                                sSigla_Recurso & "','" & _
                                ObjWN.UserName & "','" & _
                                sSistema & "','" & _
                                sAmbiente & "','" & _
                                ObjWN.computerName & "','" & _
                                sTi & "','" & _
                                sTf & "'"
                                eObjCx_Trace.Execute sql, , adAsyncExecute
                                
                 End If
            Case 3 'Todos
                         sql = "Set NoCount On Exec controle_sistema_db..Inclui_Trace_Procedure_spi '" & _
                                Mid(sProcedure, 1, InStr(sProcedure, "'")) & ",'" & _
                                sSigla_Recurso & "','" & _
                                ObjWN.UserName & "','" & _
                                sSistema & "','" & _
                                sAmbiente & "','" & _
                                ObjWN.computerName & "','" & _
                                sTi & "','" & _
                                sTf & "'"
                                eObjCx_Trace.Execute sql, , adAsyncExecute
        End Select

        Set eObjCx_Trace = Nothing
        Set eObjRS_Trace = Nothing
        Exit Function

ErrTrace:
Set eObjCx_Trace = Nothing
Set eObjRS_Trace = Nothing
 

End Function




