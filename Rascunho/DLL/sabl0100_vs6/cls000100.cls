VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 1  'NoTransaction
END
Attribute VB_Name = "cls000100"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3CBC8E670037"
Option Explicit
Dim eTpIni As String
Dim eTpFim As String
Private ErrCode As Variant
Private ErrDesc As String
Private Const SYNCHRONIZE = &H100000
Private Const INFINITE = -1&
Private Declare Function GetWindowsDirectory& Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long)
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Private Function ValidaAmbiente(ByVal eAmbiente As String) As Boolean
On Error GoTo ErrValidaAmbiente:

  Select Case eAmbiente
  Case "PRODUCAO", "PRODUCAO_ABS"
     'eAmbiente = "PRODUCAO"
     ValidaAmbiente = True
  Case "QUALIDADE"
     eAmbiente = "QUALIDADE"
     ValidaAmbiente = False
  Case "DESENVOLVIMENTO"
     eAmbiente = "DESENVOLVIMENTO"
     ValidaAmbiente = False
  End Select
  
Exit Function
ErrValidaAmbiente:

End Function
Private Function ValidaVersao(ByVal Sigla_Recurso As String, _
                              ByVal eChave As Long, _
                              ByVal eDescricaoArquivo As String) As Boolean
'On Error GoTo ErrValidaVersao:
'Dim obj0016 As New cls000016
'Dim RetSenha As String
'Dim ReSSenha As String
'Dim eSQL As String
'Dim RsRte As New ADODB.Recordset
'
'eSQL = "Set NoCount on Exec segab_db.dbo.Retorna_Versao_recurso_sps '" & Sigla_Recurso & "'"
'Set RsRte = RecordSet_Auxiliar("SEGAB", "PRODUCAO", eSQL, eChave)
'ReSSenha = LTrim(RsRte.Fields("Senha"))
'
'If Err = 0 And RsRte.State = 1 Then
'   Dim ObjPass As New cls000016
'   RetSenha = ObjPass.AbrePassword(ReSSenha, eChave)
'   Set ObjPass = Nothing
'   If CBool(eDescricaoArquivo = RetSenha) = True Then
      ValidaVersao = True
'   Else
'      ValidaVersao = False
'   End If
'Else
'   GoTo ErrValidaVersao:
'End If
'Exit Function
'ErrValidaVersao:
End Function
Private Function ObterProvider(ByVal eSistema As String, _
                               ByVal eAmbiente As String, _
                               ByVal eConexao As ADODB.Connection, _
                               Optional ByVal eLockType As LockTypeEnum, _
                               Optional ByVal eCursorType As CursorTypeEnum = adLockReadOnly, _
                               Optional ByVal eTimeOut As Long = 30000) _
                               As ADODB.Recordset
    Dim ObjRs As Object
    Dim lLogContinue As Boolean
    Dim StatusCode As Integer
    Dim StatusString As String
    Dim sql As String
    Const lcIntNoErrors = 0
    Set ObjRs = CreateObject("ADODB.Recordset")

    If Err = lcIntNoErrors Then
        lLogContinue = True
    Else
        StatusCode = Err
        StatusString = Err.Description
    End If
    On Error GoTo 0
    sql = "Set NoCount on Exec Controle_Sistema_db.dbo.obter_parametro_conexao2_sps " & _
          "'" & eSistema & "'," & _
          "'" & eAmbiente & "'"
    If lLogContinue = True Then
       On Error Resume Next
          
          ObjRs.CursorLocation = adUseClient
          Set ObjRs.ActiveConnection = eConexao
          Set ObjRs = eConexao.Execute(sql)
       
       If ObjRs Is Nothing Then
            StatusCode = Err
            StatusString = Err.Description
       Else
            If Err = lcIntNoErrors And ObjRs.State = 1 Then
                 'Sucesso
               StatusCode = 0
               Set ObterProvider = CreateObject("ADODB.Recordset")
               Set ObterProvider = ObjRs
               Set ObjRs = Nothing
               Set eConexao = Nothing
            Else
               StatusCode = Err
               StatusString = Err.Description
             '  Set ObjCx_Cx = Nothing
              ' ObterConexao = False
            End If
        End If
    End If
                             
End Function
Private Function ObterConexao(ByVal eSistema As String, _
                              ByVal eAmbiente As String, _
                              ByVal eChave As String, _
                              Optional ByVal eTipo As Integer = 0, _
                              Optional ByVal eCursorLocation As CursorLocationEnum = adUseClient, _
                              Optional ByVal eRecorSet As ADODB.Recordset, _
                              Optional ByVal eLockType As LockTypeEnum = adLockReadOnly, _
                              Optional ByVal eCursorType As CursorTypeEnum = adLockReadOnly, _
                              Optional ByVal eTimeOut As Long = 10000) _
                              As Variant

On Error GoTo AbrirConexaoErr
Const lcIntNoErrors = 0
Dim ObjCx_Cx As Object
Dim ObjRs_Cx As Object
Dim lLogContinue As Boolean
Dim sql As String
Dim Provider As String
Dim StatusString As String
Dim StatusCode As Long
Dim Obj016 As New cls000016
On Error GoTo AbrirConexaoErr

    Set ObjCx_Cx = CreateObject("ADODB.Connection")
        If Err = lcIntNoErrors Then
        lLogContinue = True
    Else
        StatusCode = Err
        StatusString = Err.Description
    End If
    On Error GoTo 0

    If lLogContinue = True Then
       On Error Resume Next
     If eTipo = 0 Then
        If UCase(eAmbiente) = "PRODUCAO" Or UCase(eAmbiente) = "PRODUCAO_ABS" Then
            ObjCx_Cx.ConnectionString = ConexaoTexto()
        Else
            'implementado dessa forma para n�o haver necessidade de troca ou inclus�o de arquivos nas maquinas dos usu�rios.
            ObjCx_Cx.ConnectionString = ObterConexaoFixo("SEGBR", "QUALIDADE", "SISBB")
        End If
       
     Else
         Dim ObjPass As New cls000016
         If UCase(eAmbiente) = "PRODUCAO" Or UCase(eAmbiente) = "PRODUCAO_ABS" Then
              
            If Not eRecorSet Is Nothing Then
                 
               'bmoraes - verificando se o password � cifrado
               If eRecorSet!cifrado = "S" Then
                   ObjCx_Cx.ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;" & _
                                         "User ID=" & eRecorSet!usuario & _
                                         ";pwd=" & ObjPass.AbrePassword(eRecorSet!senha, eChave) & _
                                         ";Initial Catalog=" & eRecorSet!banco & _
                                         ";Data Source=" & eRecorSet!servidor
               Else
                   ObjCx_Cx.ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;" & _
                                         "User ID=" & eRecorSet!usuario & _
                                         ";pwd=" & eRecorSet!senha & _
                                         ";Initial Catalog=" & eRecorSet!banco & _
                                         ";Data Source=" & eRecorSet!servidor
               End If
               Set ObjPass = Nothing
               'bmoraes fim
            Else
            
                ObjCx_Cx.ConnectionString = ObterConexaoFixo(eSistema, eAmbiente, eChave)
            
            End If
        
         Else
             If Not eRecorSet Is Nothing Then
                ObjCx_Cx.ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;" & _
                                      "User ID=" & eRecorSet!usuario & _
                                      ";pwd=" & eRecorSet!senha & _
                                      ";Initial Catalog=" & eRecorSet!banco & _
                                      ";Data Source=" & eRecorSet!servidor
             Else
             
                ObjCx_Cx.ConnectionString = ObterConexaoFixo(eSistema, eAmbiente, eChave)
             
             End If
         End If
     End If
       'ObjCx_Cx.CursorLocation = eCursorLocation
       ObjCx_Cx.ConnectionTimeout = eTimeOut
       ObjCx_Cx.CommandTimeout = 0
       ObjCx_Cx.Open
       If ObjCx_Cx Is Nothing Or ObjCx_Cx.State = 0 Then
            StatusCode = Err
            StatusString = Err.Description
            Set ObjCx_Cx = Nothing
            ObterConexao = False
       Else
           If eTipo = 0 Then
              Set ObterConexao = CreateObject("ADODB.Recordset")
              'ObterConexao.CursorLocation = adUseClient
              Set ObterConexao = ObterProvider(eSistema, eAmbiente, ObjCx_Cx, adLockReadOnly, , eTimeOut)
           Else
              Set ObterConexao = Nothing
              Set ObterConexao = CreateObject("ADODB.Connection")
              ObterConexao.CursorLocation = adUseClient
              Set ObterConexao = ObjCx_Cx
              Set ObjCx_Cx = Nothing
           End If
              If Err <> lcIntNoErrors And ObterConexao.State = 0 Then
                 StatusCode = Err
                 StatusString = Err.Description
                 Set ObjCx_Cx = Nothing
                 ObterConexao = False
              End If
        End If
    On Error GoTo 0
            Else
               StatusCode = Err
               StatusString = Err.Description
               Set ObjCx_Cx = Nothing
               ObterConexao = False
            End If
    Set ObjCx_Cx = Nothing
Exit Function
AbrirConexaoErr:
    ErrCode = Err.Number
    ErrDesc = Err.Description
    
    Call Err.Raise(ErrCode, , "SABL0100.cls000100.ObterConexao - " & ErrDesc)
      
End Function



Public Function _
ExecutarSQL(ByVal eSistema As String, _
              ByVal eAmbiente As String, _
              ByVal eSigla_Recurso As String, _
              ByVal eDescricaoArquivo As String, _
              ByVal eSQL As String, _
              ByVal eChave As Long, _
              Optional ByVal eRetRecordSet As Boolean = True, _
              Optional ByVal eExecASincrona As Boolean = False, _
              Optional ByVal eTimeOut As Long = 30000, _
              Optional ByVal eLockType As LockTypeEnum = adLockReadOnly, _
              Optional ByVal eCursorLocation As CursorLocationEnum = adUseClient, _
              Optional ByVal eCursorType As CursorTypeEnum = adLockReadOnly) As Variant

On Error GoTo ErrConex:

      
      Const lcIntNoErrors = 0
      Dim ObjRs_Ret As Recordset
      Dim ObjRs_Parametros As Variant
      Dim Continuar As Boolean
      Dim ObjCx_Execucao As Variant
      Dim StatusString As String
      Dim oSql As String
      Dim eTpFim As String

Select Case ValidaAmbiente(eAmbiente)
Case True
   If ValidaVersao(eSigla_Recurso, eChave, eDescricaoArquivo) = True Then
      If eRetRecordSet = True Then
         eExecASincrona = False
      End If
      Set ObjCx_Execucao = CreateObject("ADODB.Connection")
      If (eSistema <> "SEGBR" And eSistema <> "DIG" And eSistema <> "SCI" And eSistema <> "SMQ") Then
            Set ObjRs_Parametros = ObterConexao(eSistema, eAmbiente, eChave, 0)
            If Err = lcIntNoErrors And ObjRs_Parametros.State = 1 Then
                Continuar = True
            Else
                Continuar = False
            End If
      Else
            Continuar = True
      End If
      If Err = lcIntNoErrors And Continuar Then
         'Set ObjCx_Execucao = ObterConexao(eSistema, eAmbiente, eChave, 1, , ObjRs_Parametros)
         If (eSistema <> "SEGBR" And eSistema <> "DIG" And eSistema <> "SCI" And eSistema <> "SMQ") Then
            Set ObjCx_Execucao = ObterConexao(eSistema, eAmbiente, eChave, 1, , ObjRs_Parametros)
         Else
            Set ObjCx_Execucao = ObterConexao(eSistema, eAmbiente, eChave, 1)
         End If
         
         If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
            Set ObjRs_Parametros = Nothing
            If eRetRecordSet = True Then
               Set ObjRs_Ret = Nothing
               Set ObjRs_Ret = CreateObject("ADODB.Recordset")
               Set ObjRs_Ret = New ADODB.Recordset
               ObjRs_Ret.CursorType = eCursorType
               ObjRs_Ret.LockType = eLockType
               ObjRs_Ret.CursorLocation = eCursorLocation
               
               If Not eExecASincrona Then
                  'eTpIni = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  Call ObjRs_Ret.Open(eSQL, ObjCx_Execucao)
                  'eTpFim = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  'Comentado para fins de otimiza��o
                  'Trace eSQL, eSigla_Recurso, eSistema, eAmbiente, eTpIni, eTpFim
               Else
                   Set ObjRs_Ret = ObjCx_Execucao.Execute(eSQL, , adAsyncExecute)
               End If
               
               If Err = lcIntNoErrors And ObjRs_Ret.State = 1 Then
                  Set ExecutarSQL = CreateObject("ADODB.Recordset")
                  Set ExecutarSQL = New ADODB.Recordset
                  Set ExecutarSQL = ObjRs_Ret
               Else
                  Set ObjRs_Ret = Nothing
                  Set ObjCx_Execucao = Nothing
               End If
            Else
               If eExecASincrona = True Then
                  ObjCx_Execucao.Execute (eSQL), , adAsyncExecute
               Else
                  'eTpIni = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  ObjCx_Execucao.Execute (eSQL), , adCmdText
                  'eTpFim = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  'Comentado para fins de otimiza��o
                  'Trace eSQL, eSigla_Recurso, eSistema, eAmbiente, eTpIni, eTpFim
               End If
               If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
                  ExecutarSQL = True
               Else
                  Set ObjCx_Execucao = Nothing
                  ExecutarSQL = False
               End If
            End If
       End If
      Else
        Set ObjRs_Parametros = Nothing
        Set ObjCx_Execucao = Nothing
      End If
   Else
     'versao incorreta
   End If
Case False
      Set ObjCx_Execucao = CreateObject("ADODB.Connection")
      If (eSistema <> "SEGBR" And eSistema <> "DIG" And eSistema <> "SCI" And eSistema <> "SMQ") Then
            Set ObjRs_Parametros = ObterConexao(eSistema, eAmbiente, eChave, 0)
            If Err = lcIntNoErrors And ObjRs_Parametros.State = 1 Then
                Continuar = True
            Else
                Continuar = False
            End If
      Else
            Continuar = True
      End If
      If Err = lcIntNoErrors And Continuar Then
         If (eSistema <> "SEGBR" And eSistema <> "DIG" And eSistema <> "SCI" And eSistema <> "SMQ") Then
            Set ObjCx_Execucao = ObterConexao(eSistema, eAmbiente, eChave, 1, , ObjRs_Parametros)
         Else
            Set ObjCx_Execucao = ObterConexao(eSistema, eAmbiente, eChave, 1)
         End If
         If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
            Set ObjRs_Parametros = Nothing
               Dim ObjRs_Trace As Object
               Set ObjRs_Trace = Nothing
               Set ObjRs_Trace = CreateObject("ADODB.Recordset")
               Set ObjRs_Trace = New ADODB.Recordset
            If eRetRecordSet = True Then
               Set ObjRs_Ret = Nothing
               Set ObjRs_Ret = CreateObject("ADODB.Recordset")
               Set ObjRs_Ret = New ADODB.Recordset
               Dim ObjCx_Trace As New ADODB.Connection
               ObjRs_Ret.CursorLocation = adUseClient
                eTpIni = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                Set ObjRs_Ret = ObjCx_Execucao.Execute(eSQL)
                eTpFim = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                'Comentado para fins de otimiza��o
                'Trace eSQL, eSigla_Recurso, eSistema, eAmbiente, eTpIni, eTpFim
               If Err = lcIntNoErrors And ObjRs_Ret.State = 1 Then
                  Set ExecutarSQL = CreateObject("ADODB.Recordset")
                  Set ExecutarSQL = New ADODB.Recordset
                  Set ExecutarSQL = ObjRs_Ret
               Else
                  Set ObjRs_Ret = Nothing
                  Set ObjCx_Execucao = Nothing
               End If
            Else
               If eExecASincrona = True Then
                  ObjCx_Execucao.Execute (eSQL), , adAsyncExecute
               Else
                  eTpIni = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  Set ObjRs_Trace = ObjCx_Execucao.Execute(eSQL)
                  
                  eTpFim = Format(Date, "yyyymmdd") & " " & Format(Time, "hh:mm:ss")
                  'Comentado para fins de otimiza��o
                  'Trace eSQL, eSigla_Recurso, eSistema, eAmbiente, eTpIni, eTpFim
               End If
               If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
                  ExecutarSQL = True
               Else
                  Set ObjCx_Execucao = Nothing
                  ExecutarSQL = False
               End If
            End If
       End If
     End If
End Select
Exit Function
ErrConex:

    ErrCode = Err.Number
    ErrDesc = Err.Description
    
    Call Err.Raise(ErrCode, , "SABL0100.cls000100.ExecutarSQL - " & ErrDesc)
    
End Function

Public Property Get RedenineCorAmbiente(ByVal eAmbiente As String) As Long
If vAmbiente = "2" Or "QUALIDADE" Then
   RedenineCorAmbiente = "-2147483633"
Else
   RedenineCorAmbiente = "12648447"
End If
End Property
Private Function _
RecordSet_Auxiliar(ByVal eSistema As String, _
                   ByVal eAmbiente As String, _
                   ByVal eSQL As String, _
                   ByVal eChave As String, _
                   Optional ByVal eTimeOut As Integer = 30000) As ADODB.Recordset

Const lcIntNoErrors = 0
Dim ObjRs_Parametros As Variant
Dim StatusString As String
Dim ObjCx_Execucao As Object

Dim ObjRs_Auxiliar As New ADODB.Recordset
Set ObjCx_Execucao = CreateObject("ADODB.Connection")
Set ObjRs_Parametros = ObterConexao(eSistema, eAmbiente, eChave, 0)
 
If Err = lcIntNoErrors And ObjRs_Parametros.State = 1 Then
   Set ObjCx_Execucao = ObterConexao(eSistema, eAmbiente, eChave, 1, , ObjRs_Parametros)
   If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
      Set ObjRs_Auxiliar = ObjCx_Execucao.Execute(eSQL)
      If Err = lcIntNoErrors And ObjCx_Execucao.State = 1 Then
         Set RecordSet_Auxiliar = ObjRs_Auxiliar
      Else
         Set RecordSet_Auxiliar = Nothing
      End If
   End If
End If

Exit Function
ErrConex:
If Err <> lcIntNoErrors Then
      ErrCode = Err.Number
      ErrDesc = Err.Description
      Set RecordSet_Auxiliar = Nothing
End If
End Function
Private Function ConexaoTexto() As String
Dim Arquivo As String
Dim Provider As String
        Arquivo = "c:\SEGBR\MSDisk.dat"
        Open Arquivo For Input As #1
            Line Input #1, Provider
        Close #1
        ConexaoTexto = Provider
End Function
Private Function WindowsDir() As String

  WindowsDir = Space(256)
  WindowsDir = Left$(WindowsDir, GetWindowsDirectory(WindowsDir, 256&))
 
End Function

Public Function _
ExecutaBCP(ByVal eSistema As String, _
           ByVal eAmbiente As String, _
           ByVal eSigla_Recurso As String, _
           ByVal eDescricaoArquivo As String, _
           ByVal eStringBCP As String, _
           ByVal eChave As Long, _
           Optional ByVal eExecASincrona As Boolean = False, _
           Optional ByVal eBathSize As Long = 10000) As Boolean


On Error GoTo ErrConex:
   ExecutaBCP = False

   If ValidaVersao(eSigla_Recurso, eChave, eDescricaoArquivo) = True Then
      Dim ObjCx_Execucao As New ADODB.Connection
      Dim ObjRsBCP As New ADODB.Recordset
      Dim ObjRs_Parametros As New ADODB.Recordset
      Set ObjRs_Parametros = ObterConexao(eSistema, eAmbiente, eChave, 0)
      If Err = 0 And ObjRs_Parametros.State = 1 Then
         Dim ObjPass As New cls000016
         Dim vBCP_ID As Integer
         Dim vHandle_id As Integer
         vBCP_ID = 0
         If eAmbiente = "PRODUCAO" Or eAmbiente = "PRODUCAO_ABS" Then
            
            'bmoraes 04.02.2011 - verificando se o password � cifrado
            If ObjRs_Parametros(4) = "S" Then
                vBCP_ID = Shell(eStringBCP & "-U" & ObjRs_Parametros(2) & " -P" & ObjPass.AbrePassword(ObjRs_Parametros(3), eChave) & " -S" & ObjRs_Parametros(0) & " -b" & eBathSize, vbHide)
            Else
                vBCP_ID = Shell(eStringBCP & "-U" & ObjRs_Parametros(2) & " -P" & ObjRs_Parametros(3) & " -S" & ObjRs_Parametros(0) & " -b" & eBathSize, vbHide)
            End If
            'bmoraes fim

         Else
             vBCP_ID = Shell(eStringBCP & "-U" & ObjRs_Parametros(2) & " -P" & ObjRs_Parametros(3) & " -S" & ObjRs_Parametros(0) & " -b" & eBathSize, vbHide)
         End If
         If eExecASincrona = True Then
             DoEvents
             vHandle_id = OpenProcess(SYNCHRONIZE, 0, vBCP_ID)
             If vHandle_id <> 0 Then
                WaitForSingleObject vHandle_id, INFINITE
                CloseHandle vHandle_id
             End If
         End If
      Else
         ExecutaBCP = False
      End If
   Else
      ExecutaBCP = False
   End If

ExecutaBCP = True

Exit Function

ErrConex:

    If Err <> 0 Then
      ExecutaBCP = False
    End If

End Function
Private Function _
       Trace _
       (ByVal sProcedure As String, _
        ByVal sSigla_Recurso As String, _
        ByVal sSistema As String, _
        ByVal sAmbiente As String, _
        ByVal sTi As String, _
        ByVal sTf As String)

        Dim eObjCx_Trace As New ADODB.Connection
        Dim eObjRS_Trace As New ADODB.Recordset
        Dim ObjWN As Object
        
        eObjCx_Trace = ConexaoTexto()
        eObjCx_Trace.Open
             
        If eObjCx_Trace.State = 1 Then Else GoTo ErrTrace
             Set eObjRS_Trace = eObjCx_Trace.Execute _
             ("Set NoCount on Exec segab_db.dbo.Retorna_Versao_recurso_sps '" & sSigla_Recurso & "'")
        If eObjRS_Trace.State = 1 Then Else GoTo ErrTrace
        Dim sql As String
       
        Select Case eObjRS_Trace!Tipo
            Case 1 'HostName
                 Set ObjWN = CreateObject("Wscript.Network")
                 If UCase(eObjRS_Trace!Valor) = UCase(ObjWN.computerName) Then
                         sql = "Set NoCount On Exec controle_sistema_db..Inclui_Trace_Procedure_spi '" & _
                                Mid(sProcedure, 1, InStr(sProcedure, "'")) & ",'" & _
                                sSigla_Recurso & "','" & _
                                ObjWN.UserName & "','" & _
                                sSistema & "','" & _
                                sAmbiente & "','" & _
                                ObjWN.computerName & "','" & _
                                sTi & "','" & _
                                sTf & "'"
                                eObjCx_Trace.Execute sql, , adAsyncExecute
                 End If
            Case 2 'Login Rede
                 Set ObjWN = CreateObject("Wscript.Network")
                 If UCase(eObjRS_Trace!Valor) = UCase(ObjWN.UserName) Then
                         sql = "Set NoCount On Exec controle_sistema_db..Inclui_Trace_Procedure_spi '" & _
                                Mid(sProcedure, 1, InStr(sProcedure, "'")) & ",'" & _
                                sSigla_Recurso & "','" & _
                                ObjWN.UserName & "','" & _
                                sSistema & "','" & _
                                sAmbiente & "','" & _
                                ObjWN.computerName & "','" & _
                                sTi & "','" & _
                                sTf & "'"
                                eObjCx_Trace.Execute sql, , adAsyncExecute
                                
                 End If
            Case 3 'Todos
                         sql = "Set NoCount On Exec controle_sistema_db..Inclui_Trace_Procedure_spi '" & _
                                Mid(sProcedure, 1, InStr(sProcedure, "'")) & ",'" & _
                                sSigla_Recurso & "','" & _
                                ObjWN.UserName & "','" & _
                                sSistema & "','" & _
                                sAmbiente & "','" & _
                                ObjWN.computerName & "','" & _
                                sTi & "','" & _
                                sTf & "'"
                                eObjCx_Trace.Execute sql, , adAsyncExecute
        End Select

        Set eObjCx_Trace = Nothing
        Set eObjRS_Trace = Nothing
        Exit Function

ErrTrace:
Set eObjCx_Trace = Nothing
Set eObjRS_Trace = Nothing
 

End Function


