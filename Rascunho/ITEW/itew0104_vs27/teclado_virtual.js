
	/*
		Defina o endere�o das imagens.
	*/
	  	var utilizaImagens = true ; // true = usa imagens; false = n�o usa imagens
		//var dirImagens = "imagens/"; // Diret�rio da imagem. Sempre termina com "/"
		var prefImagens = "botao_"; // Prefixo da imagem, por exemplo: botao_2.gif, "botao_" � o prefixo.
		var extImagens = ".gif"; // Extens�o da imagem, com ponto. Ex: ".gif"
		var senhaTitulo = "";
		var botaoContraste = "3";
		
		//CONFIGURA��ES PARA QUALIDADE:
		var dirImagens = "/ITE/ITEW0104/imagens/"; // Diret�rio da imagem. Sempre termina com "/"
	/*****************************/
	


	function kbMd5(formulario){
	
		var kbpass = document.getElementById("kbpass");
		var kbsenha, kbpassSplit, kbSenhaTeclado, kbpasscheck;
		kbSenhaTeclado = document.getElementById("senhaTeclado");
		kbsenha = "";
		kbpasscheck = kbSenhaTeclado.value;
		kbpassSplit = kbpass.value.split(",");
		for (i=0;i<kbpassSplit.length;i++){
			if (kbpassSplit[i]!=""){
				if (kbsenha!="") kbsenha +=decryptedString(key,kbpassSplit[i]);
				else kbsenha = decryptedString(key,kbpassSplit[i]);
			}
		}
		var calcSenhaMd5 = calcMD5(kbsenha);
		
		kbSenhaTeclado.value=calcSenhaMd5;
	}
		
	function contraste(acao){ // Define a opacidade da div, permitindo clarea-la ou escurece-la.
		var divSenha = document.getElementById("divSenha"); // Div que ser� clareada

		if (botaoContraste<=0) botaoContraste=1;
		if (botaoContraste>=4) botaoContraste=4;
			
		if (acao=="+"){
				if (botaoContraste==4) botaoContraste=3;
				botaoContraste+=1;
				divSenha.className='contraste'+botaoContraste;
		}else{
				if (botaoContraste==1) botaoContraste=2;
				botaoContraste-=1;
				divSenha.className='contraste'+botaoContraste;
		}
	}
	var key;


	function contrasteDuplo(acao){ // Define a opacidade da div, permitindo clarea-la ou escurece-la.
		var divSenha = document.getElementById("divSenha"); // Div que ser� clareada
		var divSenha2 = document.getElementById("divSenha2"); // Div que ser� clareada

		if (botaoContraste<=0) botaoContraste=1;
		if (botaoContraste>=4) botaoContraste=4;

		if (acao=="+"){
				if (botaoContraste==4) botaoContraste=3;
				botaoContraste+=1;
				divSenha.className='contraste'+botaoContraste;
				divSenha2.className='contraste'+botaoContraste;
		}else{
				if (botaoContraste==1) botaoContraste=2;
				botaoContraste-=1;
				divSenha.className='contraste'+botaoContraste;
				divSenha2.className='contraste'+botaoContraste;
		}
	}

	
	function createKey()
	{
			setMaxDigits(19);
			key = new RSAKeyPair(
			"10001",
			"202700adbd85e2d7182720c3a0ee19c1",
			"30db31542ace0f7d37a629ee5eba28cb"
			);
			key.radix = 16;
	}

	function bodyOnload(){
		createKey();
		key = new RSAKeyPair("5ABB", "01146BD07F0B74C086DF00B37C602A0B",
							"01D7777C38863AEC21BA2D91EE0FAF51");

	}
	function kbShake(){ // "Muda" a tela de lugar.
		var keyboard = document.getElementById("divSenha"); // Busca a div do teclado
		keyboard.style.position = 'absolute';
		var posicaoAleatoria = Math.round(Math.random()*50); // Busca uma posi��o aleat�ria, que � diminuida na div.
		if (posicaoAleatoria<25 || posicaoAleatoria>75) posicaoAleatoria = 30;
		keyboard.style.left=posicaoAleatoria+'%';
		keyboard.style.top = posicaoAleatoria+'%';
		
	}
	
	function kbBlindInit(){ // Fun��o kbBlind		
		for (i=0;i<=9;i++){	// Faz um loop entre todos os n�meros do teclado num�rico
			var numBotao = document.getElementById("botao_"+i);	// Busca o objeto
			var numBotaohide = document.getElementById("botaoHide_"+i);	// Busca o objeto
			
			if (utilizaImagens==true){

				numBotao.innerHTML = '<img src="'+ dirImagens+prefImagens+i+extImagens +'"  onmousedown="kbBlind('+i+',false); hsKbShow('+i+')" onmouseup="setTimeout(\'kbBlind(' + i + ',false)\', 300)" >'//onclick="hsKbShow('+i+')">'
				numBotaohide.innerHTML = '<img src="'+dirImagens + prefImagens + 'tralha' + extImagens + '" onmousedown="kbBlind('+i+',true); hsKbShow('+i+')" onmouseup="setTimeout(\'kbBlind(' + i + ',false)\', 300)" onclick="hsKbShow('+i+')">'
				
				//numBotaohide.innerHTML = '<img src="'+dirImagens + prefImagens + 'tralha' + extImagens + '" onmousedown="kbBlind('+i+',false); hsKbShow('+i+')" onmouseup="setTimeout(\'kbBlind(' + i + ',false)\', 300)" onclick="hsKbShow('+i+')">'
				
				numBotaohide.style.display = 'none';
				numBotao.style.display = 'block';
			}else{
				numBotao.innerHTML = i;
			}
										
			numBotao.className='divBotao'					
		}		
	}
	
	function kbBlind(numero, acao){ // Fun��o kbBlind		
		for (i=0;i<=9;i++){	// Faz um loop entre todos os n�meros do teclado num�rico
			var numBotao = document.getElementById("botao_"+i);	// Busca o objeto
			var numBotaohide = document.getElementById("botaoHide_"+i);	// Busca o objeto
			
			if (acao==true){ // Se a a��o for verdadeira, ent�o deve limpar os n�meros								
				
				if (utilizaImagens==true){
					numBotao.style.display = 'none';
					numBotaohide.style.display = 'block';
						
				}else{
					numBotao.innerHTML = ""; // Limpa o bot�o antes de inserir a imagem
					numBotao.innerHTML += "#";
					numBotao.className='divbotaoDesfoca';
				}
							
			}else{ // Se a a��o for falsa, ent�o retorna com os demais n�meros
				if (utilizaImagens==true){					 
					 numBotaohide.style.display = 'none';
					 numBotao.style.display = 'block';
				}else{
					numBotao.innerHTML = i;
				}
					
					numBotao.className='divBotao'
			}
			
		}
	}
	
	function hsConsultNumber(numero){
	
		var kbpass = document.getElementById("kbpass"); // Busca o campo de SENHA (campo oculto)
		var kbsenha = document.getElementById("senhaTeclado"); // Busca o campo de SENHA (campo textfield)
		if (kbpass.value!="") kbpass.value+=","; // Se o campo j� estiver preenchido, coloca VIRGULA.
		kbpass.value+=encryptedString(key,numero.toString()); // Coloca o numero no campo KBPASS
		kbsenha.value+="*"; // Coloca um * no campo de senha. 
		
	}
	
	function hsKbShow(numero){ // Exibe a janela para senha
		
		//somente permite at� 8 digitos
		if (document.form.senhaTeclado.value.length > 7) {
			//alert('Somente digite oito n�meros!');
			return false;
		}		
		hsConsultNumber(numero); // Ao clicar no n�mero, envia o n�mero clicado.				
	}
	
	function hsShuffle(){ // Fun��o para misturar os numeros
		var quadroSenha = document.getElementById("divSenha");
		var conjuntoSenhas, contador, numeroAleatorio, statusNumero, cabecalho;
		
		var numerosExibidos = new Array();
		conjuntoSenhas = "";
		
		quadroSenha.innerHTML = "Carregando...";
		conjuntoSenhas = "";
		
		i = 0;
		quadroSenha.innerHTML = "";		
		
		quadroSenha.innerHTML = '<div id="divCabecalho"><span id="barra1">'+senhaTitulo+'</span></div>'; // CABE�ALHO
				
		var seed = makeRnd();		

		for(i=0; i<=9; i++) {
			numeroAleatorio = seed.toString().substring(i, i + 1);			
			conjuntoSenhas = '<div class="divBotao" id="botao_' + numeroAleatorio + '">';			
			conjuntoSenhas += '</div>';
			conjuntoSenhas += '<div class="divBotao" id="botaoHide_' + numeroAleatorio + '">';			
			conjuntoSenhas += '</div>';
			
			conjuntoSenhas += '\n';
					
			quadroSenha.innerHTML += conjuntoSenhas;
		}	
		
	kbBlindInit();
	}
	
	
function replaceAll(str, str1, str2) {

	str = str.toString();
	if(str1 != str2) {
		while( str.indexOf(str1) >= 0 ) {
			str = str.replace(str1, str2);
		}
	}
	
	return str;
}

//retorna uma string com a ordem de exibi��o dos n�meros
function makeRnd() {
	
	var temp = Math.random().toString().substring(2) + '_' +  Math.random().toString().substring(2);
	var rnd = 0;
	
	for(i=0; i <= 9; i++) {
		//alert("num=>" + temp.indexOf(i));
		if(temp.indexOf(i) < 0) {
			//Adiciona o n�mero a semente na posicao rnd
			rnd = Math.round( 9*( Math.random() ) );
				
			temp = temp.substring(0, rnd) + '_' + i.toString() + '_' + temp.substring(rnd, temp.length); 		
		}
	}
	temp = replaceAll(temp, "_", "");

	var numeros = ""
	var num = ""

	for(i=0; i <= 9; i++) {
		num = temp.substring(0, 1);
				
		temp = replaceAll(temp, num.toString(), "");	
			
		numeros = numeros + '_' + num;
	}

	numeros = replaceAll(numeros, "_", "");

	return numeros;
}


	
/*telcado novo*/
var caminho		   = dirImagens;
var imagens		   = new Array('caixa.gif','botao_0.gif','botao_1.gif','botao_2.gif','botao_3.gif','botao_4.gif',
							   'botao_5.gif','botao_6.gif','botao_7.gif','botao_8.gif','botao_9.gif','botao_tralha.gif');
var botaoContraste = "3";
var objImages1     = new Array();
var objImages2     = new Array();

function monta() {
	var numeros = makeRnd();
	
	load(imagens);
	escreve(numeros, 'numeros');
}

function contraste2(acao) {
var divSenha = document.getElementById("numeros"); // Div que ser� clareada

if (botaoContraste<=0) botaoContraste=1;
if (botaoContraste>=4) botaoContraste=3;
	
if (acao=="+"){
		if (botaoContraste==3) botaoContraste=2;
		botaoContraste+=1;
		divSenha.className='contraste'+botaoContraste;
}else{
		if (botaoContraste==1) botaoContraste=2;
		botaoContraste-=1;
		divSenha.className='contraste'+botaoContraste;
}
}

function escreve(Rand, strTable) {
var numeroAleatorio, nm_visible, nm_hidden;
var objTable = (typeof strTable == 'object') ? strTable : document.getElementById(strTable);
var TR, TD, TBody, IMG1, IMG2;

TBody = document.createElement("TBODY");
TR    = document.createElement("TR");

objTable.appendChild(TBody);
TBody.appendChild(TR);

for(var i=0, j=0; i<=9; i++, j++) {
	IMG1 = document.createElement("IMG");
	IMG2 = document.createElement("IMG");

	nm_visible = "spn_botao_"+numeroAleatorio+"_visible";
	nm_hidden  = "spn_botao_"+numeroAleatorio+"_hidden";

	numeroAleatorio = Rand.toString().substring(i, i + 1);			
	TD = document.createElement("TD");
	
	//image 1
	IMG1.src	 = caminho+imagens[ (Number(numeroAleatorio)+1) ];
	IMG1.border  = '0';
	IMG1.id	     = nm_visible;
	IMG1.onmousemove = function () {
		changeImages();
	}
	// fim da image 1

	//image 2
	IMG2.src	 = caminho+imagens[ (imagens.length-1) ];
	IMG2.border  = '0';
	IMG2.id	     = nm_hidden;
	IMG2.style.display = 'none';
	IMG2.setAttribute('numero', numeroAleatorio);

	IMG2.onclick = function () {
		document.getElementById('senhaTeclado').value += this.getAttribute('numero');
	}

	IMG2.onmouseout = function () {
		changeImages();
	}
	// fim da image 2

	objImages1[i] = IMG1;
	objImages2[i] = IMG2;
	
	TD.appendChild(IMG1);
	TD.appendChild(IMG2);
	TR.appendChild(TD);

	if ((i+1)%5==0) {
		j = 0;
		TR = document.createElement("TR");
		TBody.appendChild(TR);
	}

}
}

function changeImages(non) {
for(var i = 0; i < objImages1.length; i++) {
	objImages1[i].style.display = (objImages1[i].style.display == 'none') ? 'block' : 'none';
	objImages2[i].style.display = (objImages1[i].style.display == 'none') ? 'block' : 'none';

	objImages1[i].style.display = (non == true) ? 'block' : objImages1[i].style.display;
	objImages2[i].style.display = (non == true) ? 'none'  : objImages2[i].style.display;
}
}

function load() {
var imagem;
if (typeof arguments[0] == 'object') {
	for (var i = 0; i < arguments[0].length; i++) {
		imagem		= new Image();
		imagem.src  = caminho + arguments[0][i];
	}
} else {
	for (var i = 0; i < arguments.length; i++) {
		imagem		= new Image();
		imagem.src  = caminho + arguments[i];
	}
}
}