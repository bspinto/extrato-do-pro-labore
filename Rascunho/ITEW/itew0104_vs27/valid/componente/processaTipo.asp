<%@ Language=VBScript %>
<!-- #include file="../../serv/componente/hierarquia.asp" -->
<!-- #include file="../../serv/componente/registro.asp" -->
<html>
	<head>
		<link rel="stylesheet" href="../../serv/componente/comp_estilos.css" type="text/css">
	</head>
	<body>
<%
		eTipo = Trim(Request("tipo_id"))
		If eTipo <> "" Then
			eTipo = Int(eTipo)
		End If

if request("flagProcessa") = "E" then
	exTp = componente.ExcluirTipoComponente(eTipo)
	Select Case exTp
	Case 1
		strerror = "<li>Este registro n�o pode ser excluido!</li>"
%>
	<!--#include virtual = "/funcao/msg_erro.asp"-->
<%
	Case 2
		strerror = "<li>Erro na exclus�o do Registro!</li>"
%>
	<!--#include virtual = "/funcao/msg_erro.asp"-->
<%
	Case 0
%>

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr class="backgroundTitulos" height="22">
					<td class="texto" width="130"><b>Hierarquia:&nbsp;&nbsp;</b></td>
					<td class="texto"><% = hierarquia(1,request("tipoNivelHierarquia")) %></td>
				</tr>
				<tr height="22">
					<td class="texto" colspan="2" align="center">Registro excluido com sucesso!</td>
				</tr>
				<tr height="40" valign="middle">
					<td class="texto" colspan="2" align = "center">					
						<a href="javascript:history.go(-2);"><img src="/img/comp_btn_voltar.gif" border="0"></a>
					</td>
				</tr>
			</table>

<%
	End Select
Else
		Set rsTp = componente.ConsultaTipoComponente(eTipo)
		If NOT rsTp.EOF Then
%>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<form name="form" method="post" action="processaTipo.asp">
			<input type="hidden" name="flagProcessa" value="E">
			<input type="hidden" name="tipo_id" value="<%=eTipo%>">
			<input type="hidden" name="tipoNivelHierarquia" value="<%=request("tipoNivelHierarquia")%>">
				<tr class="backgroundTitulos" height="22">
					<td class="texto" width="130"><b>Hierarquia:&nbsp;&nbsp;</b></td>
					<td class="texto"><% = hierarquia(1,request("tipoNivelHierarquia")) %></td>
				</tr>
				<tr height="22">
					<td class="texto" width="130"><b>Subtipo:&nbsp;&nbsp;</b></td>
					<td class="texto"><% = verSubTp(rsTp("nome_sub_tipo_componente")) %></td>
				</tr>
				<tr height="22">
					<td class="texto" width="130"><b>Nome:&nbsp;&nbsp;</b></td>
					<td class="texto"><% = verNull(rsTp("nome_tipo_componente")) %></td>
				</tr>
				<tr height="22">
					<td class="texto" width="130"><b>Descri��o:&nbsp;&nbsp;</b></td>
					<td class="texto"><% = verNull(rsTp("descricao")) %></td>
				</tr>
				<tr height="22" class="backgroundTitulos">
					<td class="texto" colspan="2" align = "center"><b>Excluir o Tipo de Componente?</b></td>
				</tr>
				<tr height="40" valign="middle">
					<td class="texto" colspan="2" align = "center">
						<a href="javascript:history.back();"><img src="/img/comp_btn_voltar.gif" border="0"></a>
						<a href="javascript:document.form.submit();"><img src="/img/comp_btn_excluir.gif" border="0"></a>			
					</td>
				</tr>
			</form>
			</table>
<% 
		Else
			strerror = "<li>Nenhum Registro Encontrado!</li>"
%>
			<!--#include virtual = "/funcao/msg_erro.asp"-->
<%
		End if
End If
%>
	</body>
</html>
