<!--#include virtual = "/padrao.asp"-->
<%
strerror = ""

'dados da averba��o definitiva encontrada
sql = "exec w_averb_definitiva_sps " & averb_def & ","
sql = sql & apolice & "," & seguradora_id & "," & cod_susep & ","
sql = sql & subgrupo
set rs = conex_segbr.execute(sql)
if rs("origem")="SEGUROS" then
	'devera pegar o registro que esta na internet 
	rs.movenext
end if
pais_id_origem = rs("pais_id_origem")
pais_id_destino = rs("pais_id_destino")
navio_id = rs("navio_id")
'ler o nome do pais de origem
sql = "exec w_nome_pais_sps " & pais_id_origem
set rs1 = conex_segbr.execute(sql)
'ler o nome do pais de destino
sql = "exec w_nome_pais_sps " & pais_id_destino
set rs2 = conex_segbr.execute(sql)
'tratar ind_transp
if rs("ind_transp") = "1" then
   meio_transp = "Mar�timo"
elseif rs("ind_transp") = "2" then
   meio_transp = "Rodovi�rio"
elseif rs("ind_transp") = "3" then
   meio_transp = "A�reo"
elseif rs("ind_transp") = "4" then
   meio_transp = "Ferrovi�rio"
end if
bytMeioTransporte = rs("ind_transp") '(*)
'ler nome do navio (se for mar�timo)
if rs("navio_id") <> "" then
   lngIdMeioTransporte = rs("navio_id") '(*)
   sql = "exec w_nome_navio_sps " & navio_id
   set rs3 = conex_segbr.execute(sql)
   nome_navio = rs3("nome")
   lngSociedadeClassificadoraNavio = rs3("sociedade_classificadora") '(*)
   lngTipoPropulsaoNavio = rs3("tipo_propulsao") '(*)
   lngTonelagemNavio	 = rs3("tonelagem") '(*)
   lngTipoCascoNavio	 = rs3("tipo_casco") '(*)
   lngAnoConstrucao		 = rs3("ano_construcao") '(*)
   set rs3 = nothing
else
   lngIdMeioTransporte = 0 '(*)
   lngSociedadeClassificadoraNavio = 0 '(*)
   lngTipoPropulsaoNavio = 0 '(*)
   lngTonelagemNavio = 0 '(*)
   lngTipoCascoNavio = 0 '(*)
   lngAnoConstrucao = 0 '(*)
end if
'tratar ind_embarque
if rs("ind_embarque") = "T" then
   embarque = "Total"
else
   embarque = "Parcial"
end if
'tratar ind_transbordo
if rs("ind_transbordo") = "S" then
   transbordo = "Sim"
   strTransbordo = "S" '(*)
else
   transbordo = "N�o"
   strTransbordo = "N" '(*)
end if
'tratar ind_cobertura
if rs("ind_cobertura") = "P" then
   cobertura_final = "o Porto"
   bytCoberturaAte = "1" '(*)
else
   cobertura_final = "o destino final"
   bytCoberturaAte = "2" '(*)
end if
'tratar ind_vl_declarado
if rs("ind_vl_declarado") = "S" then
   conhecimento = "com o valor declarado"
   strAWBDeclarado = "S" '(*)
else
   conhecimento = "sem o valor declarado"
   strAWBDeclarado = "N" '(*)
end if
%>
<HTML>
<HEAD><TITLE>seguro transporte</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=code">
<META NAME="GENERATOR" CONTENT="MSHTML 5.00.2919.6307">
</HEAD>
<BODY BGCOLOR="#FFFFFF" LINK="#0000FF" VLINK="#800080" TEXT="#000000" TOPMARGIN="0" LEFTMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
<!--#include virtual = "/funcao/menu_transporte.asp"-->
<table class="tabelaform" cellspacing="0" cellpadding="0" align="center" style="width: 775px">
<tr><td><!--#include file="default1.asp"--></td></tr>
</table>
<form method="post" action="exclui.asp" name="form">
<input type="hidden" name="averb_def" value="<%=averb_def%>">
<!--#include virtual = "/internet/serv/ouroimp/hidden_geral.asp"-->
<table class="escolha" cellspacing="1" style="width:700px" border="0" align="center">
<tr>
  <td class=td_titulo colspan="2">Dados da Averba��o Definitiva</td>
</tr>
<tr><td>&nbsp;</td></tr>
	<TR>
		<td class="td_label" width="35%">Ap�lice</td>
		<TD class="td_dado"><%=apolice%></TD>
	</TR>
	<TR>
		<td class="td_label">N� Averba��o Definitiva</td>
		<TD class="td_dado"><%=averb_def%></TD>
	</TR>
	<TR>
		<td class="td_label" width="70">Segurado</td>
		<TD class="td_dado"><%=nome_fantasia%></TD>
	</TR>
	<%if subgrupo <> "0" then
		sql = "exec w_nome_subgrupo_sps " & apolice & ",22," & subgrupo
		set rs_nome = conex_segbr.execute(sql)%>
		<tr><td class="td_label">Nome do Subgrupo</td>
			<td class="td_dado"><%=rs_nome("nome")%></td>
		</tr>
		<%set rs_nome = nothing
	end if%>
   	<TR>
		<td class="td_label" colspan="5" Style="Text-Align : Center">
        <b>Caracter�sticas do Transporte</b></td>
	</TR>
</table>
<table class="escolha" cellspacing="1" style="width:700px" border="0" align="center">
  <TR>
    <TD align="left" class="td_label" width="119">Meio de Transporte</TD>
    <TD class="td_dado" width=381><%=meio_transp%></TD>
  </TR>
<%if meio_transp = "Mar�timo" then%>  
  <TR>
    <TD align="left" class="td_label" width="119">Nome do Navio</TD>
    <TD class="td_dado" width="381"><%=nome_navio%>
	</TD>
  </TR>
<%elseif meio_transp = "A�reo" then%>
  <TR>
    <TD align="left" class="td_label" width="119">Nome da Transportadora</TD>
    <TD class="td_dado" width="381"><%=rs("nome_empresa")%></TD>
  </TR>
  <tr>
    <TD align="left" class="td_label" width="119">Nr V�o/Pref. da Aeronave</TD>
    <TD class="td_dado" width="381"><%=rs("nr_voo")%>
	</TD>
  </TR>
<%elseif meio_transp = "Ferrovi�rio" then%>
  <TR>
    <TD align="left" class="td_label" width="119">Nome da Transportadora</TD>
    <TD class="td_dado" width="381"><%=rs("nome_empresa")%></TD>
  </TR>
  <TR>
    <TD align="left" class="td_label" width="119">Prefixo do Trem</TD>
    <TD class="td_dado" width="381"><%=rs("prefixo_trem")%></TD>
  </TR>
<%elseif meio_transp = "Rodovi�rio" then%>
  <TR>
    <TD align="left" class="td_label" width="119">Nome da Transportadora</TD>
    <TD class="td_dado" width="381"><%=rs("nome_empresa")%></TD>
  </TR>
  <TR>
    <TD align="left" class="td_label" width="119">Placa do Caminh�o</TD>
    <TD class="td_dado" width="381"><%=rs("placa_caminhao")%></TD>
  </TR>
<%end if%>
	<TR>
		<td class="td_label" align="left" width="119">Embarque</td>
		<td class="td_dado" width="381"><%=embarque%></td>   
	</TR>
  <TR>
    <TD class="td_label" width="184">Ocorreu Transbordo/Intermodal?</TD>
    <TD align="left" width="191" class="td_dado"><%=transbordo%>
    </TD></TR>
<%if transbordo = "Sim" then%>
  <TR>
    <TD class="td_label" width="184">Local de Transbordo</TD>
    <TD align="left" width="191" class="td_dado"><%=rs("local_transbordo")%></TD></TR>
<%end if%>
  <TR>
    <TD class="td_label" width="184">Cobertura at�</TD>
    <TD align="left" width="191" class="td_dado"><%=cobertura_final%>
	</TD>
  </TR>
<%if meio_transp = "A�reo" then%>
  <TR>
    <TD class="td_label" width="184">
		<div id="ValorDeclaradoLbl">O conhecimento do embarque � com ou sem valor declarado?
		</div>
	</TD>
    <TD align="left" width="191" class="td_dado">
		<div id="ValorDeclarado"><%=conhecimento%></FONT>
		</div>
	</TD>
  </TR>
<%end if%>
</table>
<table class="escolha" cellspacing="1" style="width:700px" border="0" align="center">
  <TR>
    <TD align="middle" class="td_label" colSpan="6" style="text-align:center"><B>Dados da Viagem</B>
    </TD>
  </TR>
  <TR>
	<td class="td_label" align="left" style="WIDTH: 300px">Data do Embarque</td>
	<td class="td_dado" style="WIDTH: 300px"><%=rs("dt_embarque")%></td>   
	<td class="td_label" align="left" style="WIDTH: 300px">Local</td>
	<td class="td_dado" style="WIDTH: 300px"><%=rs("local_embarque")%></td>
	<td class="td_label" align="left" style="WIDTH: 300px">Pa�s</td>
	<td class="td_dado" style="WIDTH: 300px"><%=rs1("nome")%></td>   
  </TR>
  <TR>
	<td class="td_label" align="left" style="WIDTH: 300px">Data do Desembarque</td>
	<td class="td_dado" style="WIDTH: 300px"><%=rs("dt_desembarque")%></td>   
	<td class="td_label" align="left" style="WIDTH: 300px">Local</td>
	<td class="td_dado" style="WIDTH: 300px"><%=rs("local_desembarque")%></td>
	<td class="td_label" align="left" style="WIDTH: 300px">Pa�s</td>
	<td class="td_dado" style="WIDTH: 300px"><%=rs2("nome")%></td>      
  </TR>
</TABLE>
<table class="escolha" cellspacing="1" style="width:700px" border="0" align="center">
  <TR>
    <TD align="left" class="td_label">Embalagem</TD></TR>
  <TR>
    <TD align="left" class="td_dado">
    <%'buscando a descri��o da embalagem para uma definitiva
	  sql = "exec w_embalagem_sps  " & averb_def & ","
	  sql = sql & seguradora_id & "," & cod_susep & "," & apolice & "," & subgrupo
	  set rs_emb = conex_segbr.execute(sql)
	  embalagem = trim(rs_emb("descr_embalagem")) & ""
	  if embalagem = "" then
	     Response.Write "-"
	  else
	     Response.Write embalagem
	  end if
	  set rs_emb = nothing%>
    </TD></TR>
</TABLE>
<%
set rs = nothing
set rs1 = nothing
set rs2 = nothing

'ler o id da mercadoria
sql = "exec w_ind_merc_sps " & averb_def & ","
sql = sql & apolice & "," & seguradora_id & "," & cod_susep & "," & subgrupo
Set rs1 = conex_segbr.execute(sql)
if rs1.eof then
   strerror = strerror & "Nenhuma mercadoria encontrada para a averba��o definitiva: " & averb_def
end if
if strerror <> "" then%>
<table class="escolha" cellspacing="1" border="0" align="center">
<tr>
	<td class="td_dado" style="text-align:center"><%=strerror%></td>
</tr>
</TABLE>
<%else

'APRESENTA��O DAS MERCADORIAS
do while not rs1.eof
   ind_mercadoria_id = rs1("ind_mercadoria_id")
   averb_provisoria_id = rs1("averb_provisoria_id")
   'apresenta a mercadoria
   sql = "exec w_ind_mercadoria_sps " & ind_mercadoria_id & ","
   sql = sql & apolice & "," & cod_susep & "," & seguradora_id & ","
   sql = sql & subgrupo & "," & averb_provisoria_id & "," & averb_def
   set rs2 = conex_segbr.execute(sql)
   ouro_grupo_id = rs2("ouro_grupo_id")
   lngGrupo = rs2("ouro_grupo_id") '(*)
   ouro_item_id = rs2("ouro_item_id")
   lngItem = rs2("ouro_item_id") '(*)
   ouro_subitem_id = rs2("ouro_subitem_id")
   lngSubItem = rs2("ouro_subitem_id") '(*)
   moeda_id_fob = rs2("moeda_id_fob")	
   moeda_id_frete = rs2("moeda_id_frete")
   moeda_id_despesa = rs2("moeda_id_despesa")
   moeda_id_lucro = rs2("moeda_id_lucro")
   
   sql = "exec w_ler_mercadoria_sps " & ind_mercadoria_id
   set rs_merc = conex_segbr.execute(sql)
   descricao_mercadoria = rs_merc("descr_mercadoria") & ""
   if descricao_mercadoria = "" then
	  descricao_mercadoria = "-"
   end if
   set rs_merc = nothing
   
   'ler ind_garantia e ind_fortuna da averba��o provis�ria
   sql = "exec w_garantia_fortuna_sps " & averb_provisoria_id & ","
   sql = sql & seguradora_id & "," & cod_susep & "," & apolice & "," & subgrupo
   set rs3 = conex_segbr.execute(sql)
   if not rs3.eof then
	'tratar ind_garantia
	   ind_garantia = cInt(rs3("ind_garantia"))
   else
 	   ind_garantia = 0
   end if
   'tratar ind_garantia
   ind_garantia = cInt(rs3("ind_garantia"))
   if ind_garantia = "1" then
      garantia = "Ampla"
      bytCobertura = 1 '(*)
      strGuerra = "N" '(*)
      strGreve = "N" '(*)
   elseif ind_garantia = "2" then
      garantia = "Restrita"
      bytCobertura = 2 '(*)
      strGuerra = "N" '(*)
      strGreve = "N" '(*)
   elseif ind_garantia = "5" then
      garantia = "Ampla e Guerras"
      bytCobertura = 1 '(*)
      strGuerra = "S" '(*)
      strGreve = "N" '(*)
   elseif ind_garantia = "6" then
      garantia = "Ampla e Greves"
      bytCobertura = 1 '(*)
      strGuerra = "N" '(*)
      strGreve = "S" '(*)
   elseif ind_garantia = "7" then
      garantia = "Ampla, Greves e Guerra"
      bytCobertura = 1 '(*)
      strGuerra = "S" '(*)
      strGreve = "S" '(*)
   elseif ind_garantia = "8" then
      garantia = "Restrita e Guerras"
      bytCobertura = 2 '(*)
      strGuerra = "S" '(*)
      strGreve = "N" '(*)
   elseif ind_garantia = "9" then
      garantia = "Restrita e Greves"
      bytCobertura = 2 '(*)
      strGuerra = "N" '(*)
      strGreve = "S" '(*)
   elseif ind_garantia = "10" then
      garantia = "Restrita, Greves e Guerra"
      bytCobertura = 2 '(*)
      strGuerra = "S" '(*)
      strGreve = "S" '(*)
   end if
    'tratar ind_fortuna
   if rs3("ind_fortuna") = "S" then
      fortuna = "Sim"
      strFortunaMar = "S" '(*)
   else
      fortuna = "N�o"
      strFortunaMar = "N" '(*)
   end if
   set rs = nothing
   set rs3 = nothing
%>
<br>
<table class="escolha" cellspacing="1" style="width:700px" border="0" align="center">
  <TR>
    <TD class="td_label" colspan="2" style="text-align:center">
      <B>Mercadorias</B>
    </TD>
  </TR>
<%if tp_transporte = "I" then%>
   <TR>
      <td align="left" class="td_label" width=30%>N� Averba��o Provis�ria</td>
      <td class="td_dado"><%=averb_provisoria_id%></td>
<%end if%>
   <TR>
      <TD align="left" class="td_label" width=30%>Garantias</TD>
      <TD class="td_dado"><%=garantia%></TD>
   </TR>
   <%if ind_garantia = "2" or ind_garantia = "8" or ind_garantia = "9" or ind_garantia = "10" then%>
		<TR>
		   <TD align="left" class="td_label">Fortuna do mar?</TD>
		   <TD class="td_dado"><%=fortuna%></TD>
		</TR>
   <%end if%>
</table>
<table class="escolha" cellspacing="1" style="width:700px" border="0" align="center">
  <TR>
    <TD align="left" class="td_label" colspan="5">Mercadoria Segurada</TD>
  </TR>
  <TR>
    <TD align="left" class="td_dado" colspan="5"><%=descricao_mercadoria%></TD>
  </TR>
<%if ind_garantia = "1" or ind_garantia = "5" or ind_garantia = "6" or ind_garantia = "7" then%>
<TR>
    <TD align="middle" class="td_label" width="40%">Mercadoria</TD>
    <TD align="middle" class="td_label" width="30%" colspan="3">Item</TD>
    <TD align="middle" class="td_label" width="30%">Subitem</TD>
</tr>
<TR>
    <TD align="middle" class="td_dado">
	<%'ler o nome do grupo
    sql = "exec w_nome_grupo_sps " & ouro_grupo_id
	set rs3 = conex_segbr.execute(sql)
	Response.Write(rs3("nome"))
	set rs3 = nothing%>
	</TD>
    <TD align="middle" class="td_dado" colspan="3">
	<%
    'ler o nome do item
    sql = "exec w_nome_item_sps " & ouro_grupo_id & "," & ouro_item_id
	set rs3 = conex_segbr.execute(sql)
	Response.Write(rs3("nome"))
	set rs3 = nothing%>
    </TD>
    <TD align="middle" class="td_dado">
	<%'ler o nome do subitem
    sql = "exec w_nome_subitem_sps " & ouro_grupo_id & ","
	sql = sql & ouro_item_id & "," & ouro_subitem_id
	set rs3 = conex_segbr.execute(sql)
	Response.Write(rs3("nome"))
	set rs3 = nothing%>
    </TD>
</tr>
<%end if%>
<TR>
    <TD class="td_label" align="left" width="30%">Marca</TD>
    <TD colspan="4" class="td_dado" align="left"><%=rs2("marca")%></TD>
</TR>
<TR>
    <TD align="left" class="td_label">Peso</TD>
    <TD align="left" class="td_dado" colspan="4"><%=rs2("peso")%>&nbsp;
    <%if rs2("medida_peso") = 1 then
         Response.Write "Quilo(s)"
    elseif rs2("medida_peso") = 2 then
         Response.Write "Tonelada(s)"
    elseif rs2("medida_peso") = 3 then
         Response.Write "Litro(s)"
    end if%>
    </TD>
<TR>
	<TD class="Td_label" align="left">Quantidade de volumes</TD>
	<TD class="td_dado" align="left" colspan="4"><%=rs2("quantidade")%></TD>
</TR>
<tr>
    <TD align="left" class="td_label">Taxa de convers�o</TD>
    <TD align="middle" class="td_dado" colspan="4" Style="text-align : left"><%=cstr(FormatNumber(Cdbl(rs2("tx_conversao_orig")),4))%></td>
</tr>
<TR>
    <TD align="middle" class="td_label">Verbas Seguradas</TD>
    <TD align="middle" class="td_label" colspan="3">Moeda Origem</TD>
    <TD align="middle" class="td_label" >Valor</font></TD>
</tr>
<TR>
    <TD align="left" class="td_label">&nbsp;&nbsp;Custo(IS) Embarcado</TD>
    <TD align="middle" class="td_dado" colspan="3">
    <%'ler o nome da moeda
    sql = "exec w_nome_moeda_sps " & moeda_id_fob
    set rs3 = conex_segbr.execute(sql)
    Response.Write(rs3("nome"))
    set rs3 = nothing%>
	</TD>
    <TD align="middle" class="td_dado" Style="text-align : right">
    <%=cstr(FormatNumber(Cdbl(rs2("vl_fob")),2))%>
    </TD>
</TR>
<TR>
    <TD align="left" class="td_label">&nbsp;&nbsp;Frete</TD>
    <TD align="middle" class="td_dado" colspan="3">
    <%'ler o nome da moeda do frete
    sql = "exec w_nome_moeda_sps " & moeda_id_frete
    set rs3 = conex_segbr.execute(sql)
    Response.Write(rs3("nome"))
    set rs3 = nothing%>
	</TD>
    <TD align="middle" class="td_dado" Style="text-align : right">
    <%=cstr(FormatNumber(Cdbl(rs2("vl_frete")),2))%>
    </TD>
</TR>
<TR>
    <TD align="left" class="td_label">&nbsp;&nbsp;Despesas Diversas</TD>
    <TD align="middle" class="td_dado" colspan="3">
    <%'ler o nome da moeda da despesa
    sql = "exec w_nome_moeda_sps " & moeda_id_despesa
    set rs3 = conex_segbr.execute(sql)
    Response.Write(rs3("nome"))
    set rs3 = nothing%>
    </TD>
    <TD align="middle" class="td_dado" Style="text-align : right">
    <%=cstr(FormatNumber(Cdbl(rs2("vl_despesa")),2))%>
	</TD>
</TR>
<TR>
    <TD align="left" class="td_label">&nbsp;&nbsp;Lucros Esperados</TD>
    <TD align="middle" class="td_dado" colspan="3">
    <%'ler o nome da moeda lucro
    sql = "exec w_nome_moeda_sps " & moeda_id_lucro
    set rs3 = conex_segbr.execute(sql)
    Response.Write(rs3("nome"))
    set rs3 = nothing%>
    </TD>
    <TD align="middle" class="td_dado" Style="text-align : right">
    <%=cstr(FormatNumber(Cdbl(rs2("vl_lucro")),2))%>
    </TD>
</TR>
<TR>
    <TD align="middle" class="td_label" colspan="5" style="text-align:center"><b>Impostos Total</b></TD>
</TR>  
<TR>
    <TD align="middle" class="td_label">Descri��o</TD>
    <TD align="middle" class="td_label" colspan="3">Valor </TD>
    <TD align="middle" class="td_label">Perc.</TD>
</TR>
<TR>
    <TD align="left" class="td_label">&nbsp;&nbsp;II</TD>
    <TD align="middle" class="td_dado" colspan="3" Style="text-align : right">
    <%if rs2("vl_ii_valor") = "0" or rs2("vl_ii_valor") = "" or isnull(rs2("vl_ii_valor"))then	%>
		<%=cstr(FormatNumber(Cdbl(rs2("vl_ii_perc")),2))%></TD>
        <%if (rs2("vl_ii_valor") = "0" or isnull(rs2("vl_ii_valor"))) and rs2("vl_ii_perc") <> "0" then%>
              <TD align="middle" class="td_dado">%</TD>
        <%else%>
			  <TD align="middle" class="td_dado">&nbsp;</TD>
        <%end if
	else%>
		<%=cstr(FormatNumber(Cdbl(rs2("vl_ii_valor")),2))%></TD>
		<TD align="middle" class="td_dado">&nbsp;</TD>
	<%end if%>
</TR>
<TR>
    <TD align="left" class="td_label">&nbsp;&nbsp;IPI</TD>
    <TD align="middle" class="td_dado" colspan="3" style="text-align : right">
    <%if rs2("vl_ipi_valor") = "0" or rs2("vl_ipi_valor") = "" or isnull(rs2("vl_ipi_valor")) then%>
		<%=cstr(FormatNumber(Cdbl(rs2("vl_ipi_perc")),2))%></TD>
		<%if (rs2("vl_ipi_valor") = "0" or isnull(rs2("vl_ipi_valor"))) and rs2("vl_ipi_perc") <> "0" then%>
              <TD align="middle" class="td_dado">%</TD>
        <%else%>
			  <TD align="middle" class="td_dado">&nbsp;</TD>
		<%end if
	else%>
		<%=cstr(FormatNumber(Cdbl(rs2("vl_ipi_valor")),2))%></TD>
		<TD align="middle" class="td_dado">&nbsp;</TD>
	<%end if%>
</TR>
<TR>
    <TD align="left" class="td_label">&nbsp;&nbsp;ICMS</TD>
    <TD align="middle" class="td_dado" colspan="3" style="text-align : right">
    <%if rs2("vl_icms_valor") = "0" or rs2("vl_icms_valor") = "" or isnull(rs2("vl_icms_valor"))then%>
		<%=cstr(FormatNumber(Cdbl(rs2("vl_icms_perc")),2))%></TD>
		<%if (rs2("vl_icms_valor") = "0" or isnull(rs2("vl_icms_valor")))and rs2("vl_icms_perc") <> "0" then%>
              <TD align="middle" class="td_dado">%</TD>
        <%else%>
			  <TD align="middle" class="td_dado">&nbsp;</TD>
		<%end if
	else%>
		<%=cstr(FormatNumber(Cdbl(rs2("vl_icms_valor")),2))%></TD>
		<TD align="middle" class="td_dado">&nbsp;</TD>
	<%end if%>
</TR>
<TR>
    <TD align="left" class="td_label">Total dos Impostos</TD>
    <TD align="middle" class="td_dado" colspan="4">
    <%=cstr(FormatNumber(Cdbl(rs2("vl_total_imposto")),2))%></TD>
</TR>
<TR>
    <TD align="left" class="td_label">A mercadoria ser� embarcada em container?</TD>
    <TD align="left" class="td_dado" colspan="4">
    <%if rs2("ind_container") = "S" then
		Response.Write "Sim"
		strContainer = "S" '(*)
	else
		Response.Write "N�o"
		strContainer = "N" '(*)
	end if%>
    </TD>
</tr>
<TR>
    <TD align="left" class="td_label">O container ser� aberto antes do destino final?</TD>
    <TD align="left" class="td_dado" colspan="4">
    <%if rs2("ind_container_aberto") = "S" then
		Response.Write "Sim"
		strDesova = "S" '(*)
	else
		Response.Write "N�o"
		strDesova = "N" '(*)
	end if%>
    </TD>
</tr>
<TR>
    <TD align="left" class="td_label">A mercadoria ser� embarcada a granel?</TD>
    <TD align="left" class="td_dado" colspan="4">
    <%if rs2("ind_granel") = "S" then
		Response.Write "Sim"
		strProdutoGranel = "S" '(*)
	else
		Response.Write "N�o"
		strProdutoGranel = "N" '(*)
	end if%>
    </TD>
</tr>
<TR>
    <TD align="left" class="td_label">� produto qu�mico?</TD>
    <TD align="left" class="td_dado" colspan="4">
    <%if rs2("ind_prod_quimico") = "S" then
		Response.Write "Sim"
		strProdutoQuimico = "S" '(*)
	else
		Response.Write "N�o"
		strProdutoQuimico = "N" '(*)
	end if%>
    </TD>
</tr>
<TR>
    <TD align="left" class="td_label">Cobertura para paralisa��o de equipamento de refrigera��o?</TD>
    <TD align="left" class="td_dado" colspan="4">
    <%if rs2("ind_eqpto_refrigera") = "S" then
		Response.Write "Sim"
		strCamaraFrigParou = "S" '(*)
    else
		Response.Write "N�o"
		strCamaraFrigParou = "N" '(*)
    end if%>
	</TD>
</TR>
<TR>
    <TD align="left" class="td_label">Pr�mio da mercadoria</TD>
    <TD align="left" class="td_dado" colspan="4">
    <%=rs2("vl_premio_item")%></TD>
</TR>
</TABLE>
<%set rs2 = nothing
rs1.movenext
loop
set rs1 = nothing
end if 'if strerror <> ""%>
<br>
<hr style="width:775px">
<br>
<table class="escolha" align="center">
<tr>
<td class="td_dado" style="text-align:center">Confirma exclus�o dos dados? </td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
<td align="center">
<input name="sim" type="submit" value=" Sim ">&nbsp;&nbsp;
<input name="nao" type="button" value=" N�o " onclick="javascript: history.back()">
</td>
</tr>
</table>
</form>
<br>
</BODY>
</HTML>
