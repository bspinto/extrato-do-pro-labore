<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>BB Seguros - Companhia de Seguros Alian�a do Brasil - �rea Exclusiva</title>
		<!-- meta tags -->
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="content-language" content="pt-br" />
	<meta name="robots" content="index, follow" />
	<meta name="author" content="Dynamic Tecnologia" />
	<meta name="keywords" content="seguro, bb, vida, resid�ncia, morte, residencial, roubo, risco, indeniza��o, patrim�nio, acidente, invalidez, garantia, c�ncer, mulher, doen�a, funeral, seguran�a, tranq�ilidade, assist�ncia, casa, empresa, prestamista, inc�ndio, imprevisto, agr�cola, agroneg�cio, agricultura, safra, rural, colheita, condom�nio, m�quina, transporte, responsabilidade, meio+ambiente, carbono, neutraliza��o, social, v�lei, cultura, cons�rcio" />
	<meta name="description" content="A BB Seguros - Companhia de Seguros Alian�a do Brasil possui uma diversificada carteira de produtos, composta por mais de 40 tipos de seguros que cobrem riscos pessoais e patrimoniais. Suas solu��es satisfazem as necessidades de pessoas f�sicas e jur�dicas, em todos os segmentos, inclusive o agroneg�cio. Ao investir em a��es culturais, esportivas e socioambientais, a empresa refor�a seu comprometimento com a sociedade." />
		<!-- link tags - css -->
	<link href="css/padrao.css" type="text/css" rel="stylesheet" />
	<link href="nav.css" type="text/css" rel="stylesheet" />
	<link href="css/conteudo.css" type="text/css" rel="stylesheet" />
	<link href="css/templates/azul.css" type="text/css" rel="stylesheet" />
		<!-- link tags - icon -->
	<link href="/alianca/home/favicon.ico" type="image/x-icon" rel="shortcut icon" />
		<!-- scripts -->
	<script src="/alianca/home/js/padrao.js" type="text/javascript"></script>
	<script src="/alianca/home/js/nav.js" type="text/javascript"></script>
</head>
<body>
<div id="raiz">
<!-- topo -->
  <div id="topo">
    <div id="logotipo"><a href="/alianca/home/" title="Home - Alian&ccedil;a do Brasil"><img src="/alianca/home/img/logotipo.gif" alt="Home - Alian&ccedil;a do Brasil" /></a></div>
    <div id="topo_apoio">
	  <div class="topo_conteudo">
	    <span class="data"><%= UCase( Left(Formatdatetime(now, 1), 1)) + Right(Formatdatetime(now, 1), Len(Formatdatetime(now, 1))-1)%></span>
		<form name="busca" action="/alianca/home/busca.cfm" method="post" onsubmit="return(validaBusca());" ID="Form1">
		  <input type="text" name="str_busca" id="str_busca" value="Procure aqui..." class="busca" maxlength="80" onfocus="this.value = '';" />
		  <input type="image" name="bt_buscar" value="Buscar" src="/alianca/home/img/geral/bt_buscar.gif" ID="Image1"/>
		</form>
		
		<form name="acesso_rapido" action="index.cfm" method="post" ID="Form2">
		  <select name="str_acesso_rapido" id="str_acesso_rapido">
		    <option value="">Encontre o que voc� precisa...</option>
			<option value="/alianca/home/conteudo/quemsomos/" title="Quem Somos" class="select_item">Quem Somos</option>
			<option value="/alianca/home/conteudo/quemsomos/aempresa/" title="A Empresa" class="select_item">&nbsp;&nbsp;A Empresa</option>
			<option value="/alianca/home/conteudo/quemsomos/aempresa/codigodecondutaetica/" title="C�digo de Conduta �tica" class="select_item">&nbsp;&nbsp;&nbsp;&nbsp;C�digo de Conduta �tica</option>
			<option value="/alianca/home/conteudo/quemsomos/aempresa/codigodecondutaetica/selodequalidadeetica.cfm" title="Selo de Qualidade �tica" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Selo de Qualidade �tica</option><option value="/alianca/home/conteudo/quemsomos/aempresa/premiosereconhecimentos/" title="Pr�mios e reconhecimentos" class="select_item">&nbsp;&nbsp;&nbsp;&nbsp;Pr�mios e reconhecimentos</option>
			<option value="/alianca/home/conteudo/quemsomos/aempresa/premiosereconhecimentos/2007.cfm" title="2007" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;2007</option><option value="/alianca/home/conteudo/quemsomos/aempresa/premiosereconhecimentos/2006.cfm" title="2006" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;2006</option><option value="/alianca/home/conteudo/quemsomos/aempresa/premiosereconhecimentos/2005.cfm" title="2005" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;2005</option><option value="/alianca/home/conteudo/quemsomos/aempresa/premiosereconhecimentos/2004.cfm" title="2004" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;2004</option><option value="/alianca/home/conteudo/quemsomos/aempresa/premiosereconhecimentos/2003.cfm" title="2003" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;2003</option><option value="/alianca/home/conteudo/quemsomos/responsabilidadesocial/" title="Responsabilidade Social" class="select_item">&nbsp;&nbsp;Responsabilidade Social</option>
			<option value="/alianca/home/conteudo/quemsomos/responsabilidadesocial/apoiocultural.cfm" title="Apoio Cultural" >&nbsp;&nbsp;&nbsp;-&nbsp;Apoio Cultural</option><option value="/alianca/home/conteudo/quemsomos/responsabilidadesocial/apoioesportivo.cfm" title="Apoio Esportivo" >&nbsp;&nbsp;&nbsp;-&nbsp;Apoio Esportivo</option><option value="/alianca/home/conteudo/quemsomos/responsabilidadesocial/apoiosocioambiental.cfm" title="Apoio Socioambiental" >&nbsp;&nbsp;&nbsp;-&nbsp;Apoio Socioambiental</option><option value="/alianca/home/conteudo/quemsomos/responsabilidadesocial/balancosocialibase.cfm" title="Balan�o Social" >&nbsp;&nbsp;&nbsp;-&nbsp;Balan�o Social</option><option value="/alianca/home/conteudo/quemsomos/responsabilidadesocial/valoradicionado.cfm" title="Valor Adicionado" >&nbsp;&nbsp;&nbsp;-&nbsp;Valor Adicionado</option><option value="/alianca/home/conteudo/quemsomos/responsabilidadesocial/entidadesparceiras.cfm" title="Entidades parceiras" >&nbsp;&nbsp;&nbsp;-&nbsp;Entidades parceiras</option><option value="/alianca/home/conteudo/quemsomos/forumdetendenciasaliancadobrasil/" title="F�rum de Tend�ncias Alian�a do Brasil" class="select_item">&nbsp;&nbsp;F�rum de Tend�ncias Alian�a do Brasil</option>
			<option value="/alianca/home/conteudo/quemsomos/forumdetendenciasaliancadobrasil/2006/" title="2006" class="select_item">&nbsp;&nbsp;&nbsp;&nbsp;2006</option>
			<option value="/alianca/home/conteudo/quemsomos/forumdetendenciasaliancadobrasil/2006/paineldecomercio.cfm" title="Painel de Com�rcio" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Painel de Com�rcio</option><option value="/alianca/home/conteudo/quemsomos/forumdetendenciasaliancadobrasil/2006/paineldeautopecas.cfm" title="Painel de Autope�as" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Painel de Autope�as</option><option value="/alianca/home/conteudo/quemsomos/forumdetendenciasaliancadobrasil/2006/paineldeagronegocios.cfm" title="Painel de Agroneg�cios" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Painel de Agroneg�cios</option><option value="/alianca/home/conteudo/quemsomos/forumdetendenciasaliancadobrasil/2006/paineldesiderurgiaemineracao.cfm" title="Painel de Siderurgia e Minera��o" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Painel de Siderurgia e Minera��o</option><option value="/alianca/home/conteudo/quemsomos/forumdetendenciasaliancadobrasil/2006/paineldepetroleoeenergia.cfm" title="Painel de Petr�leo e Energia" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Painel de Petr�leo e Energia</option><option value="/alianca/home/conteudo/quemsomos/forumdetendenciasaliancadobrasil/2006/paineldosetorpetroquimico.cfm" title="Painel do Setor Petroqu�mico" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Painel do Setor Petroqu�mico</option><option value="/alianca/home/conteudo/quemsomos/forumdetendenciasaliancadobrasil/2006/paineldossetoresquimicoefarmaceutico.cfm" title="Painel dos Setores Qu�mico e Farmac�utico" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;Painel dos Setores Qu�mico e Farmac�utico</option><option value="/alianca/home/conteudo/quemsomos/trabalheconosco.cfm" title="Cadastre seu curr�culo"  class="select_item">-&nbsp;Cadastre seu curr�culo</option><option value="/alianca/home/conteudo/paravoce/" title="Para Voc�" class="select_item">Para Voc�</option>
			<option value="/alianca/home/conteudo/paravoce/pessoas/" title="Pessoas" class="select_item">&nbsp;&nbsp;Pessoas</option>
			<option value="/alianca/home/conteudo/paravoce/pessoas/seguroourovida.cfm" title="Seguro Ouro Vida" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Ouro Vida</option><option value="/alianca/home/conteudo/paravoce/pessoas/bbsegurovidamulher.cfm" title="BB Seguro Vida Mulher" >&nbsp;&nbsp;&nbsp;-&nbsp;BB Seguro Vida Mulher</option><option value="/alianca/home/conteudo/paravoce/pessoas/bbsegurovida.cfm" title="BB Seguro Vida" >&nbsp;&nbsp;&nbsp;-&nbsp;BB Seguro Vida</option><option value="/alianca/home/conteudo/paravoce/pessoas/seguroourovidagarantia.cfm" title="Seguro Ouro Vida Garantia" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Ouro Vida Garantia</option><option value="/alianca/home/conteudo/paravoce/pessoas/seguroourovidaprodutorrural.cfm" title="Seguro Ouro Vida Produtor Rural" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Ouro Vida Produtor Rural</option><option value="/alianca/home/conteudo/paravoce/pessoas/bbsegurovidaagriculturafamiliar.cfm" title="BB Seguro Vida Agricultura Familiar" >&nbsp;&nbsp;&nbsp;-&nbsp;BB Seguro Vida Agricultura Familiar</option><option value="/alianca/home/conteudo/paravoce/pessoas/bbseguroprestamistaconsorcio.cfm" title="BB Seguro Prestamista Cons�rcio" >&nbsp;&nbsp;&nbsp;-&nbsp;BB Seguro Prestamista Cons�rcio</option><option value="/alianca/home/conteudo/paravoce/pessoas/bbreferenciadodiestilosegurodeapc.cfm" title="BB Referenciado DI Estilo - Seguro de APC" >&nbsp;&nbsp;&nbsp;-&nbsp;BB Referenciado DI Estilo - Seguro de APC</option><option value="/alianca/home/conteudo/paravoce/patrimoniais/" title="Patrimoniais" class="select_item">&nbsp;&nbsp;Patrimoniais</option>
			<option value="/alianca/home/conteudo/paravoce/patrimoniais/seguroouroresidencial.cfm" title="Seguro Ouro Residencial" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Ouro Residencial</option><option value="/alianca/home/conteudo/paravoce/patrimoniais/seguroouromaquinas.cfm" title="Seguro Ouro M�quinas" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Ouro M�quinas</option><option value="/alianca/home/conteudo/paravoce/transporte/" title="Transporte" class="select_item">&nbsp;&nbsp;Transporte</option>
			<option value="/alianca/home/conteudo/paravoce/transporte/bbsegurotransporteinternacional.cfm" title="BB Seguro Transporte Internacional" >&nbsp;&nbsp;&nbsp;-&nbsp;BB Seguro Transporte Internacional</option><option value="/alianca/home/conteudo/paravoce/rurais/" title="Rurais" class="select_item">&nbsp;&nbsp;Rurais</option>
			<option value="/alianca/home/conteudo/paravoce/rurais/bbseguroagricola.cfm" title="BB Seguro Agr�cola" >&nbsp;&nbsp;&nbsp;-&nbsp;BB Seguro Agr�cola</option><option value="/alianca/home/conteudo/paravoce/rurais/seguroourovidaprodutorrural.cfm" title="Seguro Ouro Vida Produtor Rural" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Ouro Vida Produtor Rural</option><option value="/alianca/home/conteudo/paravoce/rurais/bbsegurovidaagriculturafamiliar.cfm" title="BB Seguro Vida Agricultura Familiar" >&nbsp;&nbsp;&nbsp;-&nbsp;BB Seguro Vida Agricultura Familiar</option><option value="/alianca/home/conteudo/paravoce/rurais/seguroouroimplementosagricolas.cfm" title="Seguro Ouro Implementos Agr�colas" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Ouro Implementos Agr�colas</option><option value="/alianca/home/conteudo/paravoce/rurais/seguropenhorrural.cfm" title="Seguro Penhor Rural" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Penhor Rural</option><option value="/alianca/home/conteudo/paraseusnegocios/" title="Para Seus Neg�cios" class="select_item">Para Seus Neg�cios</option>
			<option value="/alianca/home/conteudo/paraseusnegocios/patrimoniais/" title="Patrimoniais" class="select_item">&nbsp;&nbsp;Patrimoniais</option>
			<option value="/alianca/home/conteudo/paraseusnegocios/patrimoniais/seguroouroempresarial.cfm" title="Seguro Ouro Empresarial" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Ouro Empresarial</option><option value="/alianca/home/conteudo/paraseusnegocios/patrimoniais/seguroouromaquinas.cfm" title="Seguro Ouro M�quinas" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Ouro M�quinas</option><option value="/alianca/home/conteudo/paraseusnegocios/patrimoniais/segurocondominiopersonalizado.cfm" title="Seguro Condom�nio Personalizado" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Condom�nio Personalizado</option><option value="/alianca/home/conteudo/paraseusnegocios/transporte/" title="Transporte" class="select_item">&nbsp;&nbsp;Transporte</option>
			<option value="/alianca/home/conteudo/paraseusnegocios/transporte/bbsegurotransporteinternacional.cfm" title="BB Seguro Transporte Internacional" >&nbsp;&nbsp;&nbsp;-&nbsp;BB Seguro Transporte Internacional</option><option value="/alianca/home/conteudo/paraseusnegocios/garantia/" title="Garantia" class="select_item">&nbsp;&nbsp;Garantia</option>
			<option value="/alianca/home/conteudo/paraseusnegocios/garantia/segurogarantia.cfm" title="Seguro Garantia" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Garantia</option><option value="/alianca/home/conteudo/paraseusnegocios/rurais/" title="Rurais" class="select_item">&nbsp;&nbsp;Rurais</option>
			<option value="/alianca/home/conteudo/paraseusnegocios/rurais/bbseguroagricola.cfm" title="BB Seguro Agr�cola" >&nbsp;&nbsp;&nbsp;-&nbsp;BB Seguro Agr�cola</option><option value="/alianca/home/conteudo/paraseusnegocios/rurais/seguroouroimplementosagricolas.cfm" title="Seguro Ouro Implementos Agr�colas" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Ouro Implementos Agr�colas</option><option value="/alianca/home/conteudo/paraseusnegocios/rurais/seguropenhorrural.cfm" title="Seguro Penhor Rural" >&nbsp;&nbsp;&nbsp;-&nbsp;Seguro Penhor Rural</option><option value="/alianca/home/conteudo/informese/" title="Informe-se" class="select_item">Informe-se</option>
			<option value="/alianca/home/conteudo/informese/guiadeorientacaoedefesadosegurado.cfm" title="Guia de Orienta��o e Defesa do Segurado"  class="select_item">-&nbsp;Guia de Orienta��o e Defesa do Segurado</option><option value="/alianca/home/conteudo/informese/linksuteis.cfm" title="Links �teis"  class="select_item">-&nbsp;Links �teis</option><option value="/alianca/home/conteudo/atendimento/" title="Atendimento" class="select_item">Atendimento</option><option value="/alianca/home/conteudo/areaexclusiva/" title="�rea Exclusiva" class="select_item">�rea Exclusiva</option>
		  </select>
		  <a href="javascript:selectLink(document.getElementById('str_acesso_rapido').value,false);void(0);"><img src="/alianca/home/img/geral/bt_ir.gif" alt="" /></a>
		</form>
		<a href="javascript:tamanhoFonte(true);" title="Aumentar Fonte"><img src="/alianca/home/img/geral/bt_a_mais.gif" alt="Aumentar Fonte" /></a>
		<a href="javascript:tamanhoFonte(false);" title="Diminuir Fonte"><img src="/alianca/home/img/geral/bt_a_menos.gif" alt="Diminuir Fonte" /></a>
	  </div>
	  <div class="topo_nav">
	    <ul id="topo_menu">
		<li><a href="/alianca/home/conteudo/quemsomos/">Quem Somos</a></li><li><a href="/alianca/home/conteudo/paravoce/">Para Voc�</a></li><li><a href="/alianca/home/conteudo/paraseusnegocios/">Para Seus Neg�cios</a></li><li><a href="/alianca/home/conteudo/informese/">Informe-se</a></li><li><a href="/alianca/home/conteudo/atendimento/">Atendimento</a></li><li><a href="/alianca/home/conteudo/areaexclusiva/">�rea Exclusiva</a></li>
		</ul>
	  </div>
    </div>
</div>
<!-- /topo -->

<!--div id="faux">
  <div id="nav">
  	<div class="bloco"> 
	 <div class="bloco_menu"><h2>�rea Exclusiva</h2></div>
	  <ul id="menu">
	  <li><a href="/alianca/home/conteudo/areaexclusiva/index.cfm">�rea Exclusiva</a></li>
	  </ul>
    </div>
  </div-->

  <div id="conteudo" style="width: 99%">
		<!--div class="barra_apoio"><a href="javascript:popImprimir('/alianca/home/imprimir.cfm?id=4E5EB2DC-1143-CFB3-D57F07FF4C57DE9D&cont=�rea Exclusiva','Imprimir',625,500);" title="Vers&atilde;o para Impress&atilde;o"><img src="/alianca/home/img/geral/bt_imprimir.gif" alt="Vers&atilde;o para Impress&atilde;o" />Imprimir</a></div-->

	<div id="bread_crumb">
	  <ul id="bread_crumb_menu">
	  <li>Home</li> 
	  <li>- <h1>�rea Exclusiva</h1></li>
	  </ul>
	</div>

	<div id="conteudo_item">
		<script>
			document.writeln( '<iframe width="100%" height="' + eval( screen.availHeight - 270 ) + '" scrolling="Auto" frameborder="no" src="trabalho.asp" name="IFrameExecute"></iframe>' )
		</script>

		<script type="text/javascript" language="javascript">
			mostraAba(0);
	    </script>
    </div>


	<!--div class="barra_apoio"><a href="#" title="Voltar ao topo da p&aacute;gina"><img src="/alianca/home/img/geral/bt_voltar_topo.gif" alt="Voltar ao topo da p&aacute;gina" />Voltar ao topo</a></div-->
  </div>
<!--/div-->


<!-- rodape -->
  <div id="rodape">
	<a href="http://www.seguros.com.br/rd.asp?tp=14&id=3" target="_self">Vote neste site</a>&nbsp;-&nbsp;<a href="/alianca/home/rodape/politicadeprivacidadeeseguranca.cfm">Pol�tica de Privacidade e Seguran�a</a>- <a href="/alianca/home/conteudo/quemsomos/trabalheconosco.cfm" target="_self">Cadastre seu curr�culo</a>&nbsp;-&nbsp;<a href="/alianca/home/rodape/mapadosite.cfm">Mapa do Site</a>
  </div>
<!-- /rodape -->
</div><!--discover:coldfusion--></body>
</html> 