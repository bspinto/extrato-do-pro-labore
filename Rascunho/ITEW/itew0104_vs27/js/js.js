﻿function LimparInput(nome) {
	var campo = document.getElementById(nome);
    campo.value = campo.value.replace('<', '');
    campo.value = campo.value.replace('>', '');
    campo.value = campo.value.replace('--', '');
    campo.value = campo.value.replace('1=1', '');
    campo.value = campo.value.replace("')", "");
    campo.value = campo.value.replace("\\", "");
    campo.value = campo.value.replace("'='", "");
    campo.value = campo.value.replace("<script>", "");
    campo.value = campo.value.replace("&", "");
}
function Tamanho(string){
	if(string!=""){
		var arraychar = string.split("");
		var tamanho = 1;
		for(i=1;i<10000;i++){
			if(arraychar[i]){
				tamanho += 1;
			}else{
				i = 200000;
			}
		}
	}
return tamanho;
}
function ValidaRegexAlph(nome) {
    var retorno = "";
	var campo = document.getElementById(nome);;
    //campo.value = campo.value.trim();
    for (var i = 0; i < Tamanho(campo.value); i++) {
        if (/[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ0-9 ]+$/.test(campo.value.charAt(i))) {
            retorno += campo.value.charAt(i);
        } 
    }
    campo.value = retorno;
}
function ValidaRegexNum(nome) {
    var retorno = "";
	var campo = document.getElementById(nome);
    for (var i = 0; i < Tamanho(campo.value); i++) {
        if (/[0-9]+$/.test(campo.charAt(i))) {
            retorno += campo.value.charAt(i);
        } 
    }
    campo.value = retorno;
}
function ValidaRegexDt(nome) {
    var retorno = "";
	var campo = document.getElementById(nome);
    for (var i = 0; i < Tamanho(campo.value); i++) {
        if (/[0-9{2}/0-9{2}/0-9{4}]/.test(campo.value.charAt(i))) {
            retorno += campo.value.charAt(i);
        } 
    }
    campo.value = retorno;
}
function ValidaRegexPhone(nome) {
    var retorno = "";
	var campo = document.getElementById(nome);
    for (var i = 0; i < Tamanho(campo.value); i++) {
        if (/[(0-9{3})0-9{4}0-9{4}]/.test(campo.value.charAt(i))) {
            retorno += campo.value.charAt(i);
        } 
    }
    campo.value = retorno;
}
function ValidaRegexCPF(nome) {
    var retorno = "";
	var campo = document.getElementById(nome);
    //campo.value = campo.value.trim();
    for (var i = 0; i < Tamanho(campo.value); i++) {
        if (/[0-9.\- ]+$/.test(campo.value.charAt(i))) {
            retorno += campo.value.charAt(i);
        } 
    }
    campo.value = retorno;
}