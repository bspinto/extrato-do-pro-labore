// Verificando para IE5 - Mac
var bug_dom_ie5_mac = (navigator.userAgent.indexOf('MSIE 5') != -1 && navigator.userAgent.indexOf('Mac') != -1);

// Verificando se ha compatibilidade com o DOM (Document Object Model) - WC3
var W3CDOM = (!bug_dom_ie5_mac && document.getElementsByTagName && document.createElement);

// Valida Busca
// [begin]
function validaBusca() {
	if (!W3CDOM) {return false};
	
	if (document.getElementById("str_busca").value == "") {
		alert("Por favor, preencha o campo de busca.");
		document.getElementById("str_busca").focus();
		return false;
	}
}
// [end]

// Acessibilidade para o tamanho das fontes
// [begin]
function tamanhoFonte($tipo) {
	if (!W3CDOM) {return false};
	
	var id_div = "conteudo"
	var valor_atual = document.getElementById(id_div).className.split("_");
	var count = Number(valor_atual[1]);
	if (!count) count = 2;
	if ($tipo) {
		if (count <= 3) {
			count++;
			document.getElementById(id_div).className = "texto_"+count;
		}
	} else {
		if (count > 1) {
			count--;
			document.getElementById(id_div).className = "texto_"+count;
		}
	}
	return count;
}
function retornaFonte() {
	if (!W3CDOM) {return false};
	
	var id_div = "conteudo"
	var valor_atual = document.getElementById(id_div).className.split("_");
	var count = Number(valor_atual[1]);
	if (!count) count = 2;
	return "texto_" + count;
}
// [end]

// Menu de Acesso Rapido
// [begin]
function selectLink($link,$janela) {
	if (!W3CDOM) {return false};
	
	if ($link != "") {
		if ($janela) {
			window.open($link);
		} else {
			window.location = $link;
		}
	} else {
		return false;	
	}
}
// [end]

// Janela PopUp de Versao para Impressao
// [begin]
function popImprimir($url,$nome,$largura,$altura) {
	window.open($url,$nome,'width='+$largura+',height='+$altura+',scrollbars=yes,toolbar=no,location=no,status=no,menubar=no,resizable=no,left=20,top=20');
}
// [end]
