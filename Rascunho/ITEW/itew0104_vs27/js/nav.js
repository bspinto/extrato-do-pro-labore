// Menu DropDown - Fix IE Bug
// [begin]
function ieDropDown() {
	if (!W3CDOM) {return false};

	if (document.getElementById("menu") != null)
	{
	
		var navItems = document.getElementById("menu").getElementsByTagName("li");
		for (var i=0; i<navItems.length; i++) {
			if (navItems[i].className == "menu_sub") {
				navItems[i].onmouseover = function() {
					this.className += " over";
				}
				navItems[i].onmouseout = function() {
					this.className = "menu_sub";
				}
			}
		}
	}
}
window.onload = ieDropDown;
// [end]

// Mostra Aba
// [begin]
function mostraAba($aba) {
    
	if (W3CDOM && document.getElementById("cont_aba_menu")) {
	    
		var elemento_aba = document.getElementById("cont_aba_menu");
		var elemento_cont = document.getElementById("cont_aba");
		var qtde_aba = elemento_aba.getElementsByTagName("li");
		var qtde_cont = elemento_cont.getElementsByTagName("div");
		for (var i=0; i<qtde_aba.length; i++) {
			if ($aba != i) {
				qtde_aba[i].className = "";
				qtde_cont[i].className = "cont_aba_off";
			} else {
				qtde_aba[i].className = "on";
				qtde_cont[i].className = "cont_aba_on";
			}
		}
	}
}
// [end]

// Mostra Item da Aba
// [begin]
function mostraItem($item) {
	if (!W3CDOM) {return false};
	
	if (document.getElementById("item_"+$item).className == "cont_aba_off") {
		document.getElementById("item_"+$item).className = "cont_aba_on";
	} else {
		document.getElementById("item_"+$item).className = "cont_aba_off";
	}
}
// [end]
