WHILE @@TRANCOUNT > 0 ROLLBACK
WHILE @@TRANCOUNT = 0 BEGIN TRAN

--rpt_ext_pgto_pro_labore_spi 14742908, '20000701', '20000731', 'CC',1,'20191106',4067313,'nova tendencia','p'

 
--CREATE PROCEDURE rpt_ext_pgto_pro_labore_spi  
DECLARE
@cliente_id           int = 14742908,  
@dt_ini_acerto        smalldatetime = '20000701',  
@dt_fim_acerto        smalldatetime = '20000731',  
@cod_origem           varchar(2) = 'CC',  
@voucher_id           int = 1,  
@dt_recebimento      smalldatetime = '20191106',  
@acerto_id            int = 4067313,  
@usuario       varchar(20) = 'nova tendencia',  
@situacao             char(1) = 'p' 
  
----AS  
  
----/****************************************************************************/  
----/* Procedure para geracao do relatorio Extrato de Pagamento a Corretores    */   
----/* data: 17/03/2000           */  
----/* Area de dboolvimento - Junior          */  
----/****************************************************************************/  
  
DECLARE  
@movimentacao_id        int            ,  
@dt_movimentacao  smalldatetime  ,  
@tp_operacao   char(1)  ,  
@val_movimentacao  numeric(15,2)  ,  
@val_ir   numeric(9,2)  ,  
@perc_comissao          numeric(9,6)    ,  
@val_paridade_moeda     numeric(9,6)    ,  
@proposta_bb            numeric(9)      ,  
@proposta_id            numeric(9)      ,  
@endosso_id             int  ,  
@num_parcela            int  ,  
@num_cobranca           int  ,  
@dt_baixa               smalldatetime ,  
@dt_agendamento         smalldatetime ,  
@val_iof                numeric(15,2)   ,  
@val_cobranca           numeric(15,2) ,  
@val_comissao_bb numeric(15,2) ,  
@tp_lancto              char(1)  ,  
@custo_apolice  numeric(15,2) ,  
@tp_emissao  char(1)  ,  
@premio_liquido  numeric(15,2) ,  
@val_juros_mora  numeric(9,2) ,  
@moeda_id               numeric(3,0)    ,  
@apolice_id             numeric(9,0)    ,  
@sucursal_seguradora_id numeric(5,0)    ,  
@ramo_id                tinyint         ,  
@produto_id             int             ,  
@nome                   varchar(60)     ,  
@tipo_proposta          char(1)  
  
  
----DELETE FROM rpt_ext_pgto_corretor_tb WHERE usuario = @usuario  
  
  
----DECLARE CursorTmp INSENSITIVE CURSOR FOR   
SELECT  m.movimentacao_id,  
 @dt_recebimento,  
 m.dt_movimentacao,  
 mcpj.tp_operacao,  
  
 mcpj.val_movimentacao,  
        convert(numeric(9,2),0), --VAL_IR 
        @cod_origem,  
        @voucher_id,  
        mcpj.perc_pro_labore  
  
FROM ps_acerto_tb    a,  
 ps_acerto_pagamento_tb   ap,  
 ps_mov_estipulante_tb   mcpj,  
        ps_movimentacao_tb       m   
  
        WHERE   ap.acerto_id =  @acerto_id  
    and  a.acerto_id    = @acerto_id  
    and mcpj.acerto_id    = a.acerto_id  
 and m.movimentacao_id = mcpj.movimentacao_id  
  
OPEN CursorTmp  
  
FETCH NEXT FROM CursorTmp INTO  
      @movimentacao_id,  
      @dt_recebimento,  
      @dt_movimentacao,  
      @tp_operacao,  
      @val_movimentacao,  
      @val_ir,  
      @cod_origem,  
      @voucher_id,  
      @perc_comissao  
  
WHILE (@@FETCH_STATUS=0)  
  
BEGIN  
  
SELECT @custo_apolice = 0  
  
SELECT @proposta_id    = ac.proposta_id ,  
       @endosso_id     = ac.endosso_id ,  
       @num_parcela    = 1,  
       @dt_baixa       = @dt_recebimento,  
       @dt_agendamento = @dt_movimentacao,  
  
       --LSA - 04/10/2006  
       --FLOW 162140 - Obter o valor de IOF gerado na baixa da fatura  
       --@val_iof        = isnull(ac.val_iof,0),     
       @val_iof        = isnull(ac.val_iof_bb,ac.val_iof),     
         
       @custo_apolice  = isnull(ac.custo_apolice,0),  
       --Rog�rio (Stefanini) - 14/02/2006 - Altera��o do campo utilizado como base no relat�rio  
       @val_cobranca   = ac.val_pago,  
       --@val_cobranca   = ac.val_bruto,  
       @val_paridade_moeda   = 1.00,  
       @val_juros_mora = 0,  
       @tp_lancto      = '2',  
       @apolice_id = ac.apolice_id,  
       @ramo_id = ac.ramo_id,  
       @sucursal_seguradora_id = ac.sucursal_seguradora_id  
FROM   ps_mov_endosso_financeiro_tb mac,  
       fatura_tb   ac  
WHERE  mac.movimentacao_id = @movimentacao_id  
       AND ac.proposta_id      = mac.proposta_id  
       AND ac.endosso_id       = mac.endosso_id  
  
-- Se n�o encontrar na fatura_tb, procurar na agendamento_cobranca_tb.  
IF @@ROWCOUNT = 0  
BEGIN  
   SELECT @proposta_id = ac.proposta_id ,  
   @endosso_id  = ac.num_endosso ,  
        @num_parcela = isnull(ac.num_parcela_endosso, ac.num_cobranca),  
          @num_cobranca = ac.num_cobranca,  
          @dt_baixa = ac.dt_baixa,  
          @dt_agendamento = ac.dt_agendamento,  
          @val_iof     = isnull(ac.val_iof_bb,val_iof),  
          @val_cobranca  = ac.val_pago,  
          @val_comissao_bb = isnull(ac.val_comissao_bb,0),  
          @val_juros_mora = isnull(ac.val_juros_mora,0),  
          @tp_lancto   = '1',  
          @dt_recebimento = ac.dt_recebimento,  
          @apolice_id = ac.apolice_id,  
          @ramo_id = ac.ramo_id,  
          @sucursal_seguradora_id = ac.sucursal_seguradora_id  
   FROM   ps_mov_agenda_cobranca_tb mac,  
          agendamento_cobranca_tb   ac  
  WHERE   mac.movimentacao_id = @movimentacao_id  
          AND ac.proposta_id      = mac.proposta_id  
          AND ac.num_cobranca     = mac.num_cobranca  
          AND   ac.situacao = 'a'  
  
  
   IF @@ROWCOUNT = 0  
      SELECT @proposta_id    = ac.proposta_id ,  
          @endosso_id     = ac.endosso_id ,  
          @num_parcela    = 1,  
          @dt_baixa       = @dt_recebimento,  
          @dt_agendamento = @dt_movimentacao,  
          @val_iof        = isnull(ac.val_iof,0),  
          @custo_apolice  = isnull(custo_apolice,0),  
          @val_cobranca   = ac.val_financeiro,  
          @val_paridade_moeda   = isnull(ac.val_paridade_moeda,1),  
          @val_juros_mora = 0,  
          @tp_lancto      = '2',  
          @apolice_id = 0,  
          @ramo_id = 0,  
          @sucursal_seguradora_id = 0  
      FROM    ps_mov_endosso_financeiro_tb mac,  
          endosso_financeiro_tb   ac  
      WHERE   mac.movimentacao_id = @movimentacao_id  
     AND ac.proposta_id      = mac.proposta_id  
     AND ac.endosso_id       = mac.endosso_id  
END  
  
----/*        */  
----/* A partir desta data cobra_se comissao sobre o juros */   
----/*        */  
----IF @val_juros_mora > 0 and @dt_baixa > '20011007'  
----   SELECT @val_juros_mora = 0  
  
  
----/*       */  
----/*  CUSTO APOLICE     */   
----/*       */  
  
----SELECT @proposta_bb = 0  
  
----IF (@endosso_id is null) OR (@endosso_id = 0)  
----   BEGIN  
----     SELECT @tipo_proposta = 'F',  
----            @proposta_bb = isnull(proposta_bb,0),  
----            @custo_apolice = isnull(custo_apolice,0),  
----            @moeda_id      = isnull(premio_moeda_id,0)  
----     FROM   proposta_fechada_tb   
----     WHERE  proposta_id = @proposta_id  
       
----     IF @@ROWCOUNT = 0  
----        SELECT @tipo_proposta = 'A',  
----               @proposta_bb = isnull(proposta_bb,0),  
----               @custo_apolice = isnull(custo_certificado,0),  
----               @moeda_id      = isnull(premio_moeda_id,0)  
----        FROM   proposta_adesao_tb   
----        WHERE  proposta_id = @proposta_id  
  
----     IF @num_parcela > 1  
----        SELECT @custo_apolice = 0  
----   END  
----ELSE  
----   BEGIN  
----     SELECT @tipo_proposta = 'F',  
----            @proposta_bb = isnull(proposta_bb,0),  
----            @custo_apolice = isnull(a.custo_apolice,0),  
----            @moeda_id      = isnull(a.premio_moeda_id,790)   
----     FROM   endosso_financeiro_tb a,  
----            proposta_fechada_tb b  
----     WHERE  a.proposta_id  = @proposta_id and  
----            endosso_id   = @endosso_id and  
----            a.proposta_id = b.proposta_id  
       
----     IF @@ROWCOUNT = 0        
----        SELECT @tipo_proposta = 'A',  
----               @proposta_bb = isnull(proposta_bb,0),  
----               @custo_apolice = isnull(a.custo_apolice,0),  
----               @moeda_id      = isnull(a.premio_moeda_id,790)   
----        FROM   endosso_financeiro_tb a,  
----               proposta_adesao_tb b  
----        WHERE  a.proposta_id  = @proposta_id and  
----               endosso_id   = @endosso_id and  
----               a.proposta_id = b.proposta_id  
  
----     IF @num_parcela > 1  
----        SELECT @custo_apolice = 0  
----   END  
----/*       */  
----/*  MOEDA        */   
----/*       */  
  
  
----IF @moeda_id <> 790  
----   IF (@endosso_id is null) OR (@endosso_id = 0) or (@tp_lancto = '1')  
----       BEGIN  
----         SELECT @val_cobranca = isnull(@val_cobranca,0) * b.val_paridade_moeda  
----         FROM   agendamento_cobranca_me_tb a,  
----                paridade_tb b  
----         WHERE  proposta_id = @proposta_id and  
----             num_cobranca  = @num_cobranca and  
----             b.origem_moeda_id = @moeda_id and  
----                b.destino_moeda_id = 790 and  
----             b.dt_conversao = isnull(a.dt_conversao,@dt_baixa)  
  
----      SELECT @custo_apolice = 0  
----      SELECT @val_iof = 0  
----       END  
----   ELSE  
----       SELECT @val_cobranca = isnull(@val_cobranca,0) * isnull(@val_paridade_moeda,1)  
  
  
----/*       */  
----/*  APOLICE     */   
----/*       */  
  
----IF @tipo_proposta = 'F'  
----   SELECT @tp_emissao = UPPER(tp_emissao),  
----          @apolice_id = apolice_id,  
----          @sucursal_seguradora_id = sucursal_seguradora_id,  
----          @ramo_id                = ramo_id    
----   FROM   apolice_tb   
----   WHERE  proposta_id = @proposta_id  
----ELSE  
----   SELECT @tp_emissao = UPPER(tp_emissao),  
----          @apolice_id = certificado_id,  
----          @sucursal_seguradora_id = 0,  
----          @ramo_id                = ramo_id    
----   FROM   certificado_re_tb   
----   WHERE  proposta_id = @proposta_id  
  
  
  
----/*       */  
----/*  PRODUTO E PROPONENTE   */   
  
----/*       */  
  
----SELECT @produto_id = produto_id,  
  
----       @nome       = nome  
----FROM   proposta_tb ,  
----       cliente_tb  
----WHERE  proposta_id = @proposta_id and  
  
----       prop_cliente_id = cliente_id  
  
  
----/*       */  
----/*  PROPONENTE DE SUB GRUPO   */   
----/*       */  
  
----IF (@ramo_id = 93 OR @ramo_id = 82 OR @ramo_id = 81 OR @ramo_id = 77) AND  
----   (@endosso_id is not null)  
----   SELECT @nome = isnull(nome,@nome)  
----   FROM   fatura_tb a,  
----          sub_grupo_apolice_tb b  
----   WHERE  a.proposta_id = @proposta_id and  
  
----          a.endosso_id  = @endosso_id and  
----          b.apolice_id = @apolice_id and  
----          b.ramo_id    = @ramo_id    and  
----          b.sucursal_seguradora_id = @sucursal_seguradora_id and  
----          b.sub_grupo_id = a.sub_grupo_id   
  
  
  
----IF @tp_operacao = 'd' and @tp_lancto = '2'  
----   BEGIN   
----      SELECT @tp_lancto = '5'   
  
----      --SELECT @val_iof = 0  
----      --SELECT @custo_apolice = 0  
----      SELECT @val_juros_mora = 0  
----      SELECT @val_movimentacao = @val_movimentacao * -1  
----      SELECT @val_ir = @val_ir * -1  
----   END  
  
  
  
  
  
----/*       */  
----/*  PREMIO LIQUIDO    */   
----/*       */  
  
----IF @tp_emissao = 'A'  
----   BEGIN   
----     SELECT @val_cobranca = @val_cobranca + @val_comissao_bb  
----     SELECT @premio_liquido = @val_cobranca   
  
----   END  
----ELSE  
----   BEGIN  
----      SELECT @premio_liquido = @val_cobranca - @val_iof - @custo_apolice - @val_juros_mora  
----      SELECT @val_cobranca = @val_cobranca - @val_iof  
----   END  
  
  
      
----INSERT  rpt_ext_pgto_corretor_tb  
---- (  
---- corretor_id,  
---- sucursal_corretor_id,  
---- tp_corretor,  
---- dt_ini_acerto,  
---- dt_fim_acerto,  
---- movimentacao_id,  
---- dt_acerto,  
---- dt_movimentacao,  
---- tp_operacao,  
  
---- val_movimentacao,  
---- tp_lancto,  
---- proposta_id,  
---- num_cobranca,  
---- endosso_id,  
  
---- proposta_bb,  
---- val_cobranca,  
---- val_iof,  
---- val_ir,  
---- custo_apolice,  
---- premio_liquido,  
  
---- perc_corretagem,  
  
---- dt_inclusao,  
---- usuario,  
---- hostname,  
----        cod_origem,  
----        voucher_id,  
----        dt_pgto_corretor,  
----        apolice_id,  
----        ramo_id,  
----        sucursal_seguradora_id,  
----        produto_id,  
----        nome  
---- )  
---- VALUES (convert(numeric(9,0),@cliente_id),  
----  '0000',  
  
----  'PL',  
----  @dt_ini_acerto,  
----  @dt_fim_acerto,  
----  @movimentacao_id,  
----  @dt_baixa,  
----  @dt_agendamento,  
----  @tp_operacao,  
----  @val_movimentacao,  
----  @tp_lancto,   
----  @proposta_id,  
----  @num_parcela,  
----  @endosso_id,  
----  @proposta_bb,  
----  @val_cobranca,  
----  @val_iof,  
  
----  isnull(@val_ir,0),  
  
----  isnull(@custo_apolice,0),  
  
----                @premio_liquido,  
----  isnull(@perc_comissao,0.0),  
----  getdate(),  
----  @usuario,  
  
----  host_name(),  
----                @cod_origem,  
----                @voucher_id,  
----                @dt_recebimento,  
----                @apolice_id,  
----         @ramo_id,  
----         @sucursal_seguradora_id,  
----         @produto_id,  
----         @nome)  
  
----FETCH NEXT FROM CursorTmp INTO  
  
----      @movimentacao_id,  
----      @dt_recebimento,  
----      @dt_movimentacao,  
----      @tp_operacao,  
----      @val_movimentacao,  
----      @val_ir,  
  
----      @cod_origem,  
  
----      @voucher_id,  
----      @perc_comissao  
  
----END  
  
CLOSE CursorTmp  
DEALLOCATE CursorTmp  
  
  
----RETURN  
  
  